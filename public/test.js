var UserAction = function () {
    var ins = {};

    // private properies
    var user_id = 1;
    var user_name = "ABC XYZ";

    // private method
    var doSomething = function()
    {

    };

    // public method
    ins.changeUser = function () {
        // use private method
        doSomething();
    };

    return ins;
}();