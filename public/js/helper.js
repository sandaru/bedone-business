/**
 * Parse JSON from String
 * @param string
 * @returns {any}
 */
function parseJson(string, showLog)
{
    var data = null;

    try {
        data = JSON.parse(string);
    }
    catch (e)
    {
        if (_.isUndefined(showLog) === false)
            console.log(e);
        data = null;
    }

    return data;
}

var isHasAjax = false;
$(document).bind("ajaxSend",  function() {
    $('.loader').show();
    isHasAjax = true;
}).bind("ajaxStop",  function() {
    $('.loader').hide();
}).bind("ajaxError", function() {
    $('.loader').hide();
});

setTimeout(function(){
    if(!isHasAjax) {
        $('.loader').hide();
    }
}, 300);

setTimeout(function(){
    if(isHasAjax) {
        console.log('OUT OF TIME AJAX')
        $('.loader').hide();
    }
}, 10000);

window.runAfter = function(callback, time) {
    setTimeout(callback, time);
};

window.ajaxUploadSetting = {
    enctype: 'multipart/form-data',
    contentType: false,
    processData: false
};

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$.fn.disabled = function() {
    $(this).attr('disabled', 'disabled');
    return this;
};

$.fn.enabled = function () {
    $(this).removeAttr('disabled');
    return this;
};

$.fn.readonly = function () {
    $(this).attr('readonly', 'readonly');
    return this;
};

$.fn.writable = function () {
    $(this).removeAttr('readonly');
    return this;
};

$.fn.customSelect2 = function (dataSource) {
    return this.select2({
        data: dataSource,
        templateResult: function (data) {
            return data.html;
        },
        escapeMarkup: function(m) {
            return m;
        }
    });
};

$.fn.unsetValueSelect2 = function () {
    return this.val('').trigger('change');
};

$.fn.scrollToBottom = function () {
    return this.scrollTop(this.height() + 99999);
};