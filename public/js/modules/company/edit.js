var EditCompanyAction = function () {
    var ins = {};

    var chooseFile = function () {
        $("#fileFeatureImage").click();
    };

    var checkFile = function(event) {
        var file = event.target.files[0];

        if (file.type.indexOf('image') < 0)
        {
            BeDoneToaster.error(langText.file_not_allowed);
            return;
        }

        $("#imgFeature").attr('src', URL.createObjectURL(file));
        $("#fileFeatureImage").attr('name', "FeatureImage");
    };

    ins.init = function () {
        $("#btnCover").click(function () {
            chooseFile();
        });

        $("#fileFeatureImage").on('change', function (event) {
            checkFile(event);
        });
    };

    return ins;
}();

$(document).ready(function () {
    EditCompanyAction.init();
});