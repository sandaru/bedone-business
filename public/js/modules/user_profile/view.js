$(document).ready(function () {
    $("#partial").css('visibility', 'visible');

    // process data
    _.each(companyModules, function (item) {
        if (item.state.selected) {
            item.li_attr = {
                class: 'tree-green',
            }
        } else {
            item.li_attr = {
                class: 'tree-red',
            }
        }
    });

    // finalization, create js tree
    $('#treeNodeBody').jstree({
        core: {
            data: companyModules
        },
    }).on('select_node.jstree', function (event, selected) {
        var prepareData = {
            id: selected.node.id,
            text: selected.node.text,
            functions: selected.node.data.functions,
        };
        VueViewUserProfile.setNode(prepareData);
    });
});

var VueViewUserProfile = new Vue({
    el: "#partial",
    data: {
        selectedNode: null,
        selectedNodeOld: null,
        name: "",
        description: ""
    },
    methods: {
        setNode: function (data) {
            this.selectedNode = _.cloneDeep(data);
            this.selectedNodeOld = data;
        },
        closeFunction: function () {
            this.selectedNode = _.cloneDeep(this.selectedNodeOld);
            this.selectedNode = null;
        },
        applyFunction: function () {
            var self = this;
            _.each(this.selectedNodeOld.functions, function (item, index) {
                item.Checked = self.selectedNode.functions[index].Checked;
            });
            this.selectedNode = this.selectedNodeOld = null;
        },
    },
});
