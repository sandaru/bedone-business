// process data
var nodeData = _.map(companyModules, function (item) {
    var children = _.map(item.Module.Children, function (child) {
        return {
            id: child.ID,
            text: child.Name,
            children: [],
            data: {
                functions: processFunctions(child.Functions),
            },
            state: {
                selected: true
            }
        }
    });

    return {
        id: item.ModuleID,
        text: item.Module.Name,
        children: children,
        data: {
            functions: processFunctions(item.Module.Functions),
        },
        state: {
            selected: true
        }
    }
});

$(document).ready(function () {
    // finalization, create js tree
    $('#treeNodeBody').jstree({
        core: {
            data: nodeData
        },
        plugins: [ "checkbox" ]
    }).on('select_node.jstree', function (event, selected) {
        var prepareData = {
            id: selected.node.id,
            text: selected.node.text,
            functions: selected.node.data.functions,
        };
        VueAddUserProfile.setNode(prepareData);
    });
});

var VueAddUserProfile = new Vue({
    el: "#partial",
    data: {
        listNodes: nodeData,
        selectedNode: null,
        selectedNodeOld: null,
        name: "",
        description: ""
    },
    methods: {
        setNode: function (data) {
            this.selectedNode = _.cloneDeep(data);
            this.selectedNodeOld = data;
        },
        closeFunction: function () {
            this.selectedNode = _.cloneDeep(this.selectedNodeOld);
            this.selectedNode = null;
        },
        applyFunction: function () {
            var self = this;
            _.each(this.selectedNodeOld.functions, function (item, index) {
                item.Checked = self.selectedNode.functions[index].Checked;
            });
            this.selectedNode = this.selectedNodeOld = null;
        },
        save: function () {
            var totalData = getProfileData();

            var postData = {
                Name: this.name,
                Description: this.description,
                Data: totalData
            };

            // AJAX
            BeDoneAjax.post(base_api + "userprofile/add", postData)
                .done(function (data) {
                     if (data != null) {
                         if (data.error) {
                             BeDoneToaster.error(data.error);
                         } else {
                             BeDoneToaster.success(data.success);
                             setTimeout(function () {
                                 window.location = returnURL;
                             }, 2000);
                         }
                     } else {
                        BeDoneToaster.error(langText.action_failed);
                     }
                });
        }
    }
});

function processFunctions(arr) {
    return _.map(arr, function (item) {
        return {
            ID: item.ID,
            Name: item.Name,
            Checked: true,
        }
    });
}

/**
 * Data PRE-Processing
 */
function getProfileData()
{
    var data = $('#treeNodeBody').jstree(true).get_json('#', {flat:true});
    var dataTree = $('#treeNodeBody').jstree(true).get_json('#', {flat:false});

    return _.map(data, function (item) {
        var inTree = _.find(dataTree, {id: item.id});
        var isChildrenSelected = false;
        if (typeof inTree !== "undefined") {
            _.each(inTree.children, function (child) {
                if (child.state.selected === true) {
                    isChildrenSelected = true;
                }
            });
        }

        return {
            ID: parseInt(item.id),
            Functions: item.data.functions,
            Checked: isChildrenSelected === true ? true : item.state.selected
        }
    });
}