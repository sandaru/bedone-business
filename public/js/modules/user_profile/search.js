var VueSearchUserProfile = new Vue({
    el: "#partial",
    data: {

    },
    methods: {
        deleteProfile: function (e) {
            var target = e.target;
            var href = $(target).parent("a").attr('data-href');

            swal({
                title: langText.are_you_sure,
                text: langText.wont_revert,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: langText.Yes
            }).then((result) => {
                if (result.value) {
                    window.location = href;
                }
            });
        },
        viewUser: function (ID) {
            // create table
            $('#tableUser').DataTable().ajax.url(base_api + "userprofile/get_user/" + ID).load();
            $("#userModal").modal('show');
        }
    },
});

$(document).ready(function() {
    $('#tableUser').DataTable( {
        //"ajax": "",
        "columns": [
            { "data": "Name" },
            { "data": "Email" },
        ]
    } );
} );