var NewsCategoryMaintenanceAction = function () {
    var ins = {};

    ins.confirmDelete = function (item_id) {
        swal({
            title: langText.are_you_sure,
            text: langText.wont_revert,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: langText.YesDelete
        }).then((result) => {
            if (result.value) {
                window.location = base + "NewsMaintenance/Delete/" + item_id;
            }
        });
    };

    return ins;
}();