var VueViewNews = new Vue({
    el: "#partial",
    data: {
        limit: 10,
        offset: 0,
        total: 0,
        page: 1,
        totalPage: 0,
        list_comment: [],
        comment: "",
        nowEditing: -1,
        nowEditComment: "",
    },
    methods: {
        get_profile_image: function(url)
        {
            if (url === null || url === "")
                return base + "img/default-avatar.png";
            else
                return base + url;
        },
        get_profile_url: function(id)
        {
            return base + "UserProfile/" + id;
        },
        post_comment: function () {
            if (functionAccess.AddComment === false)
                return;

            var data = {
                Comment: this.comment
            };

            var self = this;
            BeDoneAjax.post(base_api + "news/comment/add/" + NEWS_ID, data)
                .done(function (data) {
                    if (data != null)
                    {
                        if (data.success)
                        {
                            BeDoneToaster.success(data.success);
                            // add a fake below
                            var fakeComment = _.clone(userInfo);
                            fakeComment.CreatedDate = new Date(Date.now()).toLocaleString();
                            fakeComment.Comment = self.comment;
                            self.list_comment.push(fakeComment);

                            // clear field
                            self.comment = "";
                        }
                        else
                            BeDoneToaster.error(data.error);
                    }
                    else {
                        BeDoneToaster.error(langText.action_failed);
                    }
                });
        },
        page_change: function (isUp) {
            if (isUp === true)
                this.page++;
            else
                this.page--;

            // calculate offset
            this.offset = (this.page - 1) * this.limit;

            // begin ajax
            var self = this;
            BeDoneAjax.get(base_api + "news/get_comment/" + NEWS_ID + "?offset=" + this.offset + "&limit=" + this.limit)
                .done(function (data) {
                    if (data != null)
                    {
                        self.list_comment = data.Data;
                        self.total = data.Total;
                        self.totalPage = Math.ceil(data.Total / self.limit);
                    }
                    else {
                        BeDoneToaster.error(langText.failed_retrieve);
                    }
                });
        },

        // comment action
        removeComment: function (index) {
            if (functionAccess.DeleteComment === false)
                return;

            var item = this.list_comment[index];
            // swal ask
            swal({
                title: langText.are_you_sure,
                text: langText.wont_revert,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: langText.YesDelete
            }).then((result) => {
                if (result.value) {
                    this.actionRemove(item.ID, index);
                }
            });
        },
        actionRemove: function (ID, index) {
            var self = this;
            BeDoneAjax.post(base_api + "news/comment/del/" + ID)
                .done(function (data) {
                    if (data != null)
                    {
                        if (data.error)
                            BeDoneToaster.error(data.error);
                        else
                        {
                            BeDoneToaster.success(data.success);
                            self.list_comment.splice(index, 1);
                        }
                    }
                    else {
                        BeDoneToaster.error(langText.action_failed);
                    }
                });
        },
        
        // edit
        setEditState: function (index) {
            if (functionAccess.EditComment === false)
                return;

            this.nowEditing = index;
            this.nowEditComment = this.list_comment[index].Comment;
        },
        clearEditState: function () {
            this.nowEditing = -1;
            this.nowEditComment = "";
        },
        doEdit: function (ID) {
            if (functionAccess.EditComment === false)
                return;

            var self = this;
            BeDoneAjax.post(base_api + "news/comment/edit/" + ID, {Comment: this.nowEditComment})
                .done(function (data) {
                    if (data != null)
                    {
                        if (data.error)
                            BeDoneToaster.error(data.error);
                        else
                        {
                            BeDoneToaster.success(data.success);
                            self.list_comment[self.nowEditing].Comment = self.nowEditComment;
                            self.clearEditState();
                        }
                    }
                    else {
                        BeDoneToaster.error(langText.action_failed);
                    }
                });
        }
    },
    mounted: function () {
        if (functionAccess.ViewComment === false)
            return;

        var self = this;
        BeDoneAjax.get(base_api + "news/get_comment/" + NEWS_ID + "?offset=" + this.offset + "&limit=" + this.limit)
            .done(function (data) {
                if (data != null)
                {
                    self.list_comment = data.Data;
                    self.total = data.Total;
                    self.totalPage = Math.ceil(data.Total / self.limit);
                }
                else {
                    BeDoneToaster.error(langText.failed_retrieve);
                }
            });
    }
});

$(document).ready(function () {
    $("#partial").css('visibility', 'visible');
});