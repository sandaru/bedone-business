var VueNewsFeed = new Vue({
    'el': '#partial',
    data: {
        functionAccess: null,
        list_category: [],
        list_news: [],
        limit: 10,
        offset: 0,
        total: 0,
        isOver: false,
        selectedCate: null,
        news: null,
        selectedFiles: [],
    },
    methods: {
        createNews: function()
        {
            return {
                ID: "",
                Title: "",
                Description: "",
                Content: "",
                CategoryID: "",
                FeatureImage: [],
            };
        },
        load_category: function (cate) {
            this.selectedCate = cate;
            this.offset = 0;
            this.total = 0;
            this.load_news();
        },
        getUrl: function () {
            return base + "api/news/get_data?limit=" + this.limit + "&offset=" + this.offset + (this.selectedCate != null ? "&CategoryID=" + this.selectedCate.ID : "");
        },
        load_news: function () {
            var self = this;
            this.isOver = false;

            BeDoneAjax.get(this.getUrl())
                .done(function (data) {
                    if (data != null && data.Data)
                    {
                        self.list_news = data.Data;
                        //self.total = data.Total;
                    }
                    else
                    {
                        BeDoneToaster.error(langText.failed_retrieve);
                    }
                });
        },
        load_more_news: function()
        {
            var self = this;
            this.offset += this.limit;
            BeDoneAjax.get(this.getUrl())
                .done(function (data) {
                    if (data != null && data.Data)
                    {
                        if (data.Data.length > 0)
                        {
                            _.each(data.Data, function (item) {
                                self.list_news.push(item);
                            });
                        }
                        else
                        {
                            self.isOver = true;
                        }
                    }
                    else
                    {
                        BeDoneToaster.error(langText.failed_retrieve);
                    }
                });
        },
        getFeatureImage: function (path) {
            if (path === "")
                return base + "img/default_feature.png";
            else
                return base + path;
        },
        getNewsPage: function (ID) {
            return base + "News/" + ID;
        },

        // add
        addNews: function () {
            if (functionAccess.Add === false)
                return;

            // reset data form
            this.news = this.createNews();
            this.selectedFiles = [];
            $("#newsModal").modal('show');
        },

        // edit
        editItem: function(index)
        {
            this.news = _.clone(this.list_news[index]);
            this.selectedFiles = [];

            var self = this;
            _.each(this.news.FeatureImage, function (item, i) {
                self.selectedFiles.push(item);
            });

            // find category
            var category = _.findWhere(this.list_category, {Name: this.news.CategoryName});
            if (_.isUndefined(category) === false)
            {
                this.news.CategoryID = category.ID;
            }

            $("#newsModal").modal('show');
        },

        // Img functionals
        selectImage: function()
        {
            $("#fileUpload").click();
        },
        selectedImage: function(event)
        {
            if (this.selectedFiles.length > 4)
                return;

            var file = event.target.files[0];

            if (file.type.indexOf("image") < 0)
            {
                BeDoneToaster.error(langText.file_not_allowed);
                return;
            }

            this.selectedFiles.push(file);
        },
        removeImage: function(index)
        {
            this.selectedFiles.splice(index, 1);
        },
        showImage: function(item) {
            if(item instanceof File)
                return URL.createObjectURL(item);
            else
                return item;
        },

        /**
         * Add or update news
         */
        addOrUpdate: function () {
            // prepare data
            var formData = new FormData();

            // add normal data
            formData.append("Title", this.news.Title);
            formData.append("Description", this.news.Description);
            formData.append("Content", this.news.Content);
            formData.append("CategoryID", this.news.CategoryID);

            // add upload images
            _.each(this.selectedFiles, function (file, index) {
                if (file instanceof File)
                    formData.append("UploadImages[]", file);
                else
                    formData.append("FeatureImage[]", file);
            });

            // check if add
            if (this.news.ID === "")
                this.addingRequest(formData); // add
            else
                this.editingRequest(this.news.ID, formData);
        },

        addingRequest: function(formData)
        {
            var settings = {
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false
            };

            var self = this;
            BeDoneAjax.post(base_api + "news/add", formData, settings)
                .done(function (data) {
                    //var data = parseJson(result);

                    if (data != null)
                    {
                        if (_.isUndefined(data.error) === false)
                            BeDoneToaster.error(data.error);
                        else
                        {
                            BeDoneToaster.success(data.success);

                            // close modal
                            $("#newsModal").modal('hide');
                        }
                    }
                    else {
                        BeDoneToaster.error(langText.action_failed);
                    }
                });
        },
        editingRequest: function(ID, formData)
        {
            // get old featureimage if exists

            var settings = {
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false
            };

            var self = this;
            BeDoneAjax.post(base_api + "news/edit/" + ID, formData, settings)
                .done(function (data) {
                    ///var data = parseJson(result);

                    if (data != null)
                    {
                        if (_.isUndefined(data.error) === false)
                            BeDoneToaster.error(data.error);
                        else
                        {
                            BeDoneToaster.success(data.success);

                            // close modal
                            $("#newsModal").modal('hide');
                        }
                    }
                    else {
                        BeDoneToaster.error(langText.action_failed);
                    }
                });
        },

        /**
         * Delete news
         * @param ID
         * @param index
         */
        confirmDelete: function (ID, index) {
            if (functionAccess.Delete === false)
                return;

            swal({
                title: langText.are_you_sure,
                text: langText.wont_revert,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: langText.YesDelete
            }).then((result) => {
                if (result.value) {
                    this.delete(ID, index);
                }
            });
        },
        delete: function (ID, index) {
            var self = this;
            BeDoneAjax.get(base + "api/news/delete/" + ID)
                .done(function (data) {
                    if (data != null)
                    {
                        if (data.error)
                            BeDoneToaster.error(data.error);
                        else {
                            BeDoneToaster.success(data.success);
                            self.list_news.splice(index, 1);
                        }
                    }
                    else
                    {
                        BeDoneToaster.error(langText.delete_failed);
                    }
                });
        },
    },
    created: function()
    {
        this.news = this.createNews();
    },
    mounted: function () {
        this.list_category = category_list;
        this.functionAccess = functionAccess;
        this.load_news();
        //$("#slCategory").select2();
    },
});

$(document).ready(function () {
    $("#partial").css('visibility', 'visible');
});