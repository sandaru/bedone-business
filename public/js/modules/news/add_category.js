var NewsCategoryAddAction = function () {
    var ins = {};

    ins.init = function () {
        $("#slIcon").change(function (event) {
            $("#iconCategory").attr('class', 'fa fa-2x ' + event.target.value);
        });
    };

    return ins;
}();

$(document).ready(function () {
    NewsCategoryAddAction.init();
});