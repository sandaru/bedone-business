var VueEditBookingResourceAction = new Vue({
    el: "#partial",
    data: {
        selectedFiles: [],
        ID: "",
        Name: "",
        Description: "",
        Address: "",
    },
    methods: {
        // Img functionals
        selectImage: function()
        {
            $("#fileUpload").click();
        },
        selectedImage: function(event)
        {
            if (this.selectedFiles.length > 4)
                return;

            var file = event.target.files[0];

            if (file.type.indexOf("image") < 0)
            {
                BeDoneToaster.error(globalLang.file_not_allowed);
                return;
            }

            this.selectedFiles.push(file);
        },
        removeImage: function(index)
        {
            this.selectedFiles.splice(index, 1);
        },
        showImage: function(item) {
            if(item instanceof File)
                return URL.createObjectURL(item);
            else
                return item;
        },
        save: function () {
            // start to post
            var postData = new FormData();
            postData.append("Name", this.Name);
            postData.append("Description", this.Description);
            postData.append("Address", this.Address);

            _.each(this.selectedFiles, function (file) {
                if (file instanceof File) {
                    postData.append("Gallery[]", file);
                } else {
                    postData.append("OldGallery[]", file);
                }
            });

            var settings = {
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false
            };

            // request
            BeDoneAjax.post(base_api + "booking_resource/edit/" + this.ID, postData, settings)
                .done(function (data) {
                    if (data !== null) {
                        if (data.error) {
                            BeDoneToaster.error(data.error);
                        } else {
                            BeDoneToaster.success(data.success, function () {
                                window.location = back_url;
                            });
                        }
                    } else {
                        BeDoneToaster.error(globalLang.action_failed);
                    }
                });
        }
    },
    mounted: function () {
        this.ID = resourceInfo.ID;
        this.Name = resourceInfo.Name;
        this.Address = resourceInfo.Address;
        this.Description = resourceInfo.Description;
        this.selectedFiles = resourceInfo.Gallery;

        $("#partial").css('visibility', 'visible');
    }
});