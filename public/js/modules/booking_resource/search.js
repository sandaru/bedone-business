$(document).ready(function () {
    $(".deleteAction").click(function () {
        var url = $(this).attr('data-href');
        BeDoneConfirm.confirm("", function () {
            window.location = url;
        });
    });
});