var VueUserGroup = new Vue({
    el: "#partial",
    data: {
        list_groups: [],
        keyword: "",
    },
    methods: {
        getAllGroup: function () {
            var self = this;
            BeDoneAjax.get(base_api + "group/all")
                .done(function (data) {
                     if (data !== null) {
                        self.list_groups = data;
                     } else {
                        BeDoneToaster.error(globalLang.action_failed);
                     }
                });
        }
    },
    mounted: function () {
        $("#partial").css('visibility', 'visible');
        this.getAllGroup();
    }
});