var VueAllStaff = new Vue({
    el: "#partial",
    data: {
        list_staff: [],
        keyword: "",
    },
    methods: {
        get_profile_image: function(url)
        {
            if (url === null || url === "")
                return base + "img/default-avatar.png";
            else
                return base + url;
        },
        get_profile_url: function(id)
        {
            return base + "UserProfile/" + id;
        },
    },
    mounted: function () {
        var self = this;
        BeDoneAjax.get(base_api + "staff/all")
            .done(function (data) {
                if (data != null)
                {
                    self.list_staff = data;
                }
                else {
                    BeDoneToaster.error(langText.action_failed);
                }
            });
    }
});

$(document).ready(function () {
    $("#partial").css('visibility', 'visible');
});