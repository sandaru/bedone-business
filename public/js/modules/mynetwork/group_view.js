var GroupViewAction = function () {
    var ins = {};

    ins.init = function () {
        $(".removeMember").click(function () {
            removeMember($(this));
        });
        $(".change-img").click(function () {
            $("#fileGroupImage").val(null).click();
        });
        $("#fileGroupImage").on('change', function (e) {
            var file = e.target.files[0];
            if (file.type.indexOf("image") < 0)
            {
                BeDoneToaster.error(langText.file_not_allowed);
                $("#fileGroupImage").val(null).removeAttr('name');
            }

            $("#fileGroupImage").attr('name', 'GroupImage');
            $(".change-img").attr('src', URL.createObjectURL(file));
        });

        if (typeof option_list !== "undefined") {
            $("#slAddPeople").customSelect2(option_list);
        }
    };

    var removeMember = function (selector) {
        var href = selector.attr('data-href');
        BeDoneConfirm.confirm("", function () {
            window.location = href;
        });
    };

    ins.edit_group = function () {
        if (typeof default_data === "undefined") {
            return;
        }

        // set default data lel
        $("input[name='Name']").val(default_data.Name);
        $("textarea[name='Description']").val(default_data.Description);
        $(".change-img").attr('src', default_data.GroupImage);
        $("#fileGroupImage").removeAttr('name');

        $("#editGroupModal").modal('show');
    };

    ins.add_more_people = function() {
        $("#slAddPeople").unsetValueSelect2();
        $("#addPeopleModal").modal('show');
    };

    ins.remove_group = function () {
        BeDoneConfirm.confirm(null, function () {
            window.location = base + "Groups/Delete/" + group_id;
        });
    };

    ins.leave_group = function () {
        BeDoneConfirm.confirm(null, function () {
            window.location = base + "Groups/Leave/" + group_id;
        });
    };

    return ins;
}();

$(document).ready(function () {
    GroupViewAction.init();
});