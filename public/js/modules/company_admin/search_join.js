$(document).ready(function () {
    $("#joinBody").on('click', '.decline-user', function (e) {
        if (!functionAccess.Decline)
            return;

        var target = e.target;
        var url = $(target).parent("a").attr('data-href');

        swal({
            title: langText.sure_to_reject,
            text: langText.wont_revert,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: langText.Yes
        }).then((result) => {
            if (result.value) {
                window.location = url;
            }
        });
    });
});