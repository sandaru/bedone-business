$(document).ready(function () {
    $("table").on('click', 'tr td .removeUser', function (e) {
        var target = e.target;
        var href = $(target).parent('a').attr('data-href');

        swal({
            title: langText.are_you_sure,
            text: langText.wont_revert,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: langText.Yes
        }).then((result) => {
            if (result.value) {
                window.location = href;
            }
        });
    });
});