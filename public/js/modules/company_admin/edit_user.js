var EditUserMaintenance = function()
{
    var ins = {};

    ins.init = function () {
        $("#slcProfile").select2();

        $("#imgProfile").click(function () {
            $("#fileProfile").click();
        });

        $("#imgFeature").click(function () {
            $("#fileFeature").click();
        });

        $("#fileProfile").on('change', function (e) {
            processImage(e.target, "Profile");
        });

        $("#fileFeature").on('change', function (e) {
            processImage(e.target, "Feature");
        });
    };

    var checkImageExtension = function (fileObj) {
        if (fileObj.type.indexOf('image') < 0)
        {
            BeDoneToaster.error(globalLang.file_not_allowed);
            return false;
        }

        return true;
    };

    var processImage = function (target, type) {
        // check file extension
        var file = target.files[0];
        if (checkImageExtension(file) === false)
            return;

        // ok change image
        $("#img" + type).attr('src', URL.createObjectURL(file));
        $("#file" + type).attr('name', type + "Image");
    };

    return ins;
}();

$(document).ready(function () {
    EditUserMaintenance.init();
});