var UserProfileAction = function () {
    var ins = {};

    ins.viewBenefit = function () {
        swal({
            title: $("#benefitText").html(),
            type: 'info',
            html: document.getElementById('benefitTable').outerHTML,
            showCloseButton: true,
        });
    };

    ins.init = function () {
        $("#companyList").select2();
    };

    ins.joinCompany = function () {
        var idCompany = $("#companyList").val();

        if (idCompany === "" || idCompany === null)
        {
            BeDoneToaster.error(langText.choose_company_failed);
            return;
        }

        // ok ajax here to join company
        BeDoneAjax.post(base + "api/company/join", {CompanyID: idCompany})
            .done(function (result) {
                var data = parseJson(result);

                if (data !== null)
                {
                    if (_.isUndefined(data.error))
                    {
                        // success
                        BeDoneToaster.success(data.success, function () {
                            $("#joinCompanyModal").modal('hide');
                            location.reload();
                        });
                    }
                    else {
                        BeDoneToaster.error(langText.join_company_failed);
                    }
                }
                else
                {
                    BeDoneToaster.error(langText.join_company_failed);
                }
            });
    };

    ins.createCompany = function () {
        var company = $("#txtCompanyName");

        if (company.val().length <= 0)
        {
            BeDoneToaster.error(langText.input_company_failed);
            return;
        }

        // ok ajax here to create company
        BeDoneAjax.post(base + "api/company/create", {Name: company.val()})
            .done(function (result) {
                var data = parseJson(result);

                if (data !== null)
                {
                    if (_.isUndefined(data.error))
                    {
                        // success
                        BeDoneToaster.success(langText.create_company_success, function () {
                            $("#createCompanyModal").modal('hide');
                            location.reload();
                        });
                    }
                    else {
                        BeDoneToaster.error(langText.create_company_failed);
                    }
                }
                else
                {
                    BeDoneToaster.error(langText.create_company_failed);
                }
            });
    };

    ins.confirmCancel = function () {
        swal({
            title: langText.are_you_sure,
            text: langText.wont_revert,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: langText.YesDelete
        }).then((result) => {
            if (result.value) {
                window.location = base + "Company/CancelRequest";
            }
        });
    };

    return ins;
}();

$(document).ready(function () {
    UserProfileAction.init();
});