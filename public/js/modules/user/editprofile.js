var EditProfileAction = function () {
    var ins = {};

    ins.init = function () {
        $("#selectGender").select2();
        $("#txtDate").datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            format: "YYYY-MM-DD"
        });

        $("#imgProfilePic").on('click', function () {
            $("#fileProfile").click();
        });

        $("#btnChangeCover").on('click', function () {
            $("#fileFeature").click();
        });

        $("#fileProfile").change(function (event) {
            ins.changeProfilePic(event);
        });

        $("#fileFeature").change(function (event) {
            ins.changeCoverPic(event);
        });
    };

    ins.changeProfilePic = function (event) {
        var file = event.target.files[0];

        if (file.type.indexOf('image') < 0)
        {
            BeDoneToaster.error(langText.file_not_allowed);
            return;
        }

        $("#imgProfilePic").attr('src', URL.createObjectURL(file));
        $("#fileProfile").attr('name', "ProfileImage");
    };

    ins.changeCoverPic = function (event) {
        var file = event.target.files[0];

        if (file.type.indexOf('image') < 0)
        {
            BeDoneToaster.error(langText.file_not_allowed);
            return;
        }

        $("#imgCover").attr('src', URL.createObjectURL(file));
        $("#fileFeature").attr('name', "FeatureImage");
    };

    return ins;
}();


$(document).ready(function () {
    EditProfileAction.init();
});