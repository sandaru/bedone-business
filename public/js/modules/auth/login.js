$(document).ready(function () {
    if (typeof error_mess !== "undefined")
    {
        BeDoneToaster.error(error_mess);
    }

    if (typeof valid_error_mess !== "undefined")
    {
        BeDoneToaster.error(valid_error_mess);
    }

    if (typeof success_mess !== "undefined")
    {
        BeDoneToaster.success(success_mess);
    }
});