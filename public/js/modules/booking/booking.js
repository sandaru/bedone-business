var TIME_FORMAT = "HH:mm";
var NORMAL_DATE_FORMAT = "yyyy-mm-dd";
var ICON_CONTROL_DATE = {
    time: "fa fa-clock-o",
    date: "fa fa-calendar",
    up: "fa fa-chevron-up",
    down: "fa fa-chevron-down",
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-screenshot',
    clear: 'fa fa-trash',
    close: 'fa fa-remove'
};
var NOW_DATE = moment(today_date, NORMAL_DATE_FORMAT);

var BookingModule = function() {
    var ins = {};

    var listEvent = [];
    var selectedDate = "";
    var currentEvent = null;

    ins.init = function () {
        $("#partial").css('visibility', 'visible');
        monthCalendar();
        initEvent();
        $("#btnCreate").click(function () {
            createBooking();
        });
        $("#btnSave").click(function () {
            saveBooking();
        });
        $("#btnCancel").click(function () {
            BeDoneConfirm.confirm("", cancelBooking);
        });
        $("#slResource").select2();
        $("#slUsers").select2({
            data: listUsers,
            templateResult: function (data) {
                return data.html;
            },
            escapeMarkup: function(m) {
                return m;
            }
        });
    };

    var initEvent = function () {
        $('#txtEndTime').datetimepicker({
            format: TIME_FORMAT,
            icons: ICON_CONTROL_DATE
        });
        $('#txtStartTime').datetimepicker({
            format: TIME_FORMAT,
            icons: ICON_CONTROL_DATE
        });
        $("#ckbIsAllDay").on('change', function (event) {
            changeDate($(event.target));
        });
    };

    var monthCalendar = function () {
        $('#monthViewCalendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month'
            },
            defaultDate: new Date(),
            editable: true,
            eventStartEditable: false,
            eventLimit: 4, // allow "more" link when too many events
            events: base_api + "booking/month",
            timezone: "local",
            eventClick: function (calEvent, jsEvent, view) {
                viewBooking(calEvent);
            },
            dayClick: function(date, jsEvent, view) {
                // only can create if the day is >= today
                if (date.isBefore(NOW_DATE)) {
                    return;
                }
                selectedDate = date.format();

                clearField();
                // set data
                $("#txtDate").val(selectedDate);

                // show dialog
                $("#bookingModal").modal('show');
            }
        });
    };

    var dayCalendar = function () {
        $("#dayViewCalendar").fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'agendaDay'
            },
            defaultView: "agendaDay",
            defaultDate: new Date(),
            editable: true,
            eventLimit: true, // allow "more" link when too many events
        });
    };

    var createBooking = function () {
        var postData = {
            Title: $("#txtTitle").val(),
            BookingResourceID: $("#slResource").val(),
            Description: $("#txtDescription").val(),
            BookDate: selectedDate,
            StartTime: $("#txtStartTime").val(),
            EndTime: $("#txtEndTime").val(),
            IsAllDay: $("#ckbIsAllDay").is(':checked'),
            IsPrivate: $("#ckbIsPrivate").is(':checked'),
            AttendUsers: $("#slUsers").val()
        };

        BeDoneAjax.post(base_api + "booking/add", postData)
            .done(function (data) {
                if (data !== null) {
                    if (data.error) {
                        BeDoneToaster.error(data.error);
                    } else {
                        BeDoneToaster.success(data.success, function () {
                            location.reload();
                        });
                    }
                } else {
                    BeDoneToaster.error(globalLang.action_failed);
                }
            });
    };

    var clearField = function () {
        $("#txtStartTime").enabled().val('');
        $("#txtEndTime").enabled().val('');
        $("#txtTitle").val("").enabled();
        $("#txtDescription").val("").enabled();
        $("#ckbIsAllDay").bootstrapToggle('off').bootstrapToggle('enable');
        $("#ckbIsPrivate").bootstrapToggle('off').bootstrapToggle('enable');
        $("#slUsers").val([]).trigger('change').enabled();
        $("#slResource").val('').trigger('change').enabled();
        $("#btnCreate").show();
        $("#btnSave").hide();
        $("#btnCancel").hide();
        $(".modal-title").html(langText.CreateNewBooking);
    };

    var changeDate = function ($el) {
        if ($el.is(':checked')) {
            $("#txtStartTime").attr('disabled', 'disabled').val('');
            $("#txtEndTime").attr('disabled', 'disabled').val('');
        } else {
            $("#txtStartTime").removeAttr('disabled');
            $("#txtEndTime").removeAttr('disabled');
        }
    };

    ins.openMyBooking = function (ID) {
        BeDoneAjax.get(base_api + "booking/get/" + ID)
            .done(function (data) {
                if (data !== null) {
                    if (data.error) {
                        BeDoneToaster.error(data.error);
                        return;
                    }

                    // data tranmission
                    var event = data.data;
                    event.start = moment(event.start);
                    event.end = moment(event.end);
                    viewBooking(event);
                } else {
                    BeDoneToaster.error(globalLang.action_failed);
                }
            });
    };

    var viewBooking = function (data) {
        if (functionAccess.ViewPrivate === false && data.IsPrivate === true) {
            return;
        }

        currentEvent = data;
        selectedDate = data.BookDate;
        clearField();

        // set data to these field
        $(".modal-title").html(langText.EditBooking);
        $("#txtTitle").val(data.title);
        $("#txtDescription").val(data.Description);
        $("#txtDate").val(data.BookDate);
        if (data.allDay === true) {
            $("#ckbIsAllDay").bootstrapToggle('on');
        } else {
            $("#ckbIsAllDay").bootstrapToggle('off');
            $("#txtStartTime").val(data.start.format(TIME_FORMAT));
            $("#txtEndTime").val(data.end.format(TIME_FORMAT));
        }
        $("#slResource").val(data.BookingResourceID).trigger('change');
        $("#slUsers").val(data.Users).trigger('change');

        // permission
        if ((functionAccess.Edit !== true && data.AdminUserID !== userID) || moment(today_date).isAfter(data.BookDate) === true) {
            $(".modal-title").html(langText.ViewBooking);
            $("#btnSave").hide();
            $("#btnCancel").hide();
            $("#txtTitle").disabled();
            $("#txtDescription").disabled();
            $("#txtDate").disabled();
            $("#txtStartTime").disabled();
            $("#txtEndTime").disabled();
            $("#ckbIsPrivate").bootstrapToggle('disable');
            $("#ckbIsAllDay").bootstrapToggle('disable');
            $("#slUsers").disabled();
            $("#slResource").disabled();
        } else {
            $("#btnSave").show();
            $("#btnCancel").show();
        }

        // show hide action
        $("#btnCreate").hide();
        $("#bookingModal").modal('show');
    };

    var saveBooking = function () {
        if ((functionAccess.Edit !== true && currentEvent.AdminUserID !== userID) || moment(today_date).isAfter(currentEvent.BookDate) === true) {
            return;
        }

        var postData = {
            Title: $("#txtTitle").val(),
            BookingResourceID: $("#slResource").val(),
            Description: $("#txtDescription").val(),
            BookDate: selectedDate,
            StartTime: $("#txtStartTime").val(),
            EndTime: $("#txtEndTime").val(),
            IsAllDay: $("#ckbIsAllDay").is(':checked'),
            IsPrivate: $("#ckbIsPrivate").is(':checked'),
            AttendUsers: $("#slUsers").val()
        };

        BeDoneAjax.post(base_api + "booking/edit/" + currentEvent.ID, postData)
            .done(function (data) {
                if (data !== null) {
                    if (data.error) {
                        BeDoneToaster.error(data.error);
                    } else {
                        BeDoneToaster.success(data.success, function () {
                            location.reload();
                        });
                    }
                } else {
                    BeDoneToaster.error(globalLang.action_failed);
                }
            });
    };

    var cancelBooking = function () {
        if ((functionAccess.Delete !== true && currentEvent.AdminUserID !== userID) || moment(today_date).isAfter(currentEvent.BookDate) === true) {
            return;
        }

        BeDoneAjax.get(base_api + "booking/cancel/" + currentEvent.ID)
            .done(function (data) {
                if (data !== null) {
                    if (data.error) {
                        BeDoneToaster.error(data.error);
                    } else {
                        BeDoneToaster.success(data.success, function () {
                            location.reload();
                        });
                    }
                } else {
                    BeDoneToaster.error(globalLang.action_failed);
                }
            });
    };

    return ins;
}();


$(document).ready(function () {
    BookingModule.init();
});