var BeDoneAjax = function(){

    function baseAjax(type, url, data, customOptions) {
        var url_request = '';
        if (url.indexOf(base) === 0) {
            url_request = url;
        } else {
            url_request = base + url;
        }

        var ajaxOption = {};

        if(_.isObject(customOptions)) {
            for(var option in customOptions) {
                ajaxOption[option] = customOptions[option];
            }
        }

        ajaxOption.type = type;
        ajaxOption.data = data;
        ajaxOption.url = url_request;

        // console.log(ajaxOption)
        var request = $.ajax(ajaxOption);
        return request;
    }
    var instance = {};
    var postAjax = instance.post = function postAjax(apiUrl, data, customOptions) {
        return baseAjax('POST', apiUrl, data, customOptions);
    };


    instance.get = function getAjax(apiUrl, data, customOptions) {
        return baseAjax('GET', apiUrl, data, customOptions);
    };

    instance.postJSON = function (apiUrl, data, customOptions) {
        var deferred = jQuery.Deferred();
        postAjax(apiUrl, data, customOptions).then(function (response) {
            var json = parseJSON(response);
            if (json === undefined) {
                deferred.reject({msg: "JSON error!"})
                return;
            }
            deferred.resolve(json);

        }, function (error) {
            deferred.reject(error)
        })
        return deferred;
    };

    instance.postJSONResult =  function postAjaxJSONResult(apiUrl, data, customOptions) {
        var errMessage = undefined;
        if(_.isObject(customOptions)) {
            if(_.has(customOptions, 'errorMessage')) {
                errMessage = customOptions.errorMessage;
                delete customOptions.errorMessage;
            }
        }

        var deferred = jQuery.Deferred();
        postAjax(apiUrl, data).then(function (response) {
            var json = parseJSON(response);
            if (json === undefined) {
                deferred.reject({msg: "JSON error!"})
                return;
            }

            if (json.result === 'SUCCESS') {
                deferred.resolve(json);
            } else {
                var showErrorMess = _.isEmpty(errMessage) ? json.msg : errMessage;
                B2BEToast.error(showErrorMess);
                deferred.reject(json);
            }
        }, function (error) {
            console.error(error);
            deferred.reject(error)
        })
        return deferred;
    };


    instance.postCache = function(CacheBundle, key, url, postData){
        var defer = $.Deferred();
        if(CacheBundle.has(key)) {
            defer.resolve(CacheBundle.get(key));
            return defer;
        }
        AjaxService.postJSON(url, postData)
            .done(function(response) {
                CacheBundle.set(key, response);
                defer.resolve(response);
            }).fail(function(err){
            defer.reject(err);
        })
        return defer;
    }

    return instance;
}();