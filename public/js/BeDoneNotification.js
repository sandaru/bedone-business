var BeDoneNotification = function () {
    var ins = {};

    var currentOffset = 0;
    var limit = 10;

    ins.init = function () {
        // watch click notification
        $("#btnNoti").click(function (e) {
            if ($("#numberNotification").length <= 0)
                return;

            // solving
            setSeen();
        });

        $("#btnLoadMore").click(function () {
            loadMoreNotification();
        });
    };

    var loadMoreNotification = function () {
        currentOffset += limit;

        BeDoneAjax.get(base_api + "notification/get_more?limit=" + limit + "&offset=" + currentOffset)
            .done(function (data) {
                if (data === null || data === "") {
                    $("#btnLoadMore").hide();
                } else {
                    $("#btnLoadMore").before(data);
                }
            });
    };

    var setSeen = function () {
        BeDoneAjax.get(base_api + "notification/seen_all")
            .done(function () {
                $("#numberNotification").remove();
            });
    };

    return ins;
}();

$(document).ready(function () {
    BeDoneNotification.init();
});