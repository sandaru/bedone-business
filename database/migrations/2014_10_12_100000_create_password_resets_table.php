<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration
{
    public static $table = "UserResetPassword";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments("ID");
            $table->integer("UserID")->unsigned();
            $table->string("ResetKey");
            $table->dateTime("CreatedDate");

            // mapping
            $table->foreign("UserID")->references('ID')->on(CreateUsersTable::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
