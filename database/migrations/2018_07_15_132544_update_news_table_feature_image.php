<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNewsTableFeatureImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("News", function (Blueprint $tb) {
            $tb->text("FeatureImage")->nullable()->change();
        });

        \Illuminate\Support\Facades\DB::statement("
            ALTER TABLE BookingResource CHANGE `Gallery` `Gallery` TEXT NULL DEFAULT NULL
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("News", function (Blueprint $tb) {
            $tb->string("FeatureImage")->nullable()->change();
        });

        \Illuminate\Support\Facades\DB::statement("
            ALTER TABLE BookingResource CHANGE `Gallery` `Gallery` VARCHAR(255) NULL DEFAULT NULL
        ");
    }
}
