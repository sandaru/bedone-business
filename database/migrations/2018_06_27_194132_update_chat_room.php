<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChatRoom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('ChatRoom', function (Blueprint $tb) {
            $tb->enum('IsArchived', ['yes', 'no'])->default('no');
            $tb->enum('Activate', ['yes', 'no'])->default('yes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ChatRoom', function (Blueprint $tb) {
            $tb->dropColumn('IsArchived');
            $tb->dropColumn('Activate');
        });
    }
}
