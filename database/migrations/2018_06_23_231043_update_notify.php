<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotify extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE UserNotification CHANGE `Type` `Type`
            ENUM('InviteJoinOrg',
                'AcceptJoinOrg',
                'RejectJoinOrg',
                'News',
                'BookingAdded',
                'BookingNotification',
                'BookingCancel') NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE UserNotification CHANGE `Type` `Type`
            ENUM('InviteJoinOrg',
                'AcceptJoinOrg',
                'RejectJoinOrg',
                'News',
                'BookingAdded',
                'BookingNotification') NOT NULL");
    }
}
