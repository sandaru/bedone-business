<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("Group", function (Blueprint $tb) {
            $tb->text('Description')->default(null);
            $tb->string('GroupImage')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("Group", function (Blueprint $tb) {
            $tb->dropColumn('Description');
            $tb->dropColumn('GroupImage');
        });
    }
}
