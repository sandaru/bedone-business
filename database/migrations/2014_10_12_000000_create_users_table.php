<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public static $table = "User";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('CompanyID')->unsigned()->nullable();
            $table->string('Email')->unique();
            $table->string('Password');
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('Introduction')->nullable();
            $table->enum('Gender', ['F', 'M'])->nullable();
            $table->date('BirthDay')->nullable();
            $table->string("JobTitle")->nullable();
            $table->string("HomePhone")->nullable();
            $table->string("WorkPhone")->nullable();
            $table->string("WorkAddress")->nullable();
            $table->enum("Activate", ['yes', 'no'])->default('yes');
            $table->enum("IsSuperAdmin", ['yes', 'no'])->default('no');
            $table->string('FeatureImage')->nullable();
            $table->string('ProfileImage')->nullable();

            // default for tracking
            $table->string("CreatedByID");
            $table->dateTime("CreatedDate");
            $table->string("CreatedByName");
            $table->string("ModifiedByID");
            $table->dateTime("ModifiedDate");
            $table->string("ModifiedByName");

            // foreign key
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
