<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("NewsComment", function (Blueprint $table)
        {
            $table->increments("ID");
            $table->integer("UserID")->unsigned();
            $table->integer("NewsID")->unsigned();
            $table->text('Comment');


            // default for tracking
            $table->string("CreatedByID");
            $table->dateTime("CreatedDate");
            $table->string("CreatedByName");
            $table->string("ModifiedByID");
            $table->dateTime("ModifiedDate");
            $table->string("ModifiedByName");

            $table->foreign("UserID")->references('ID')->on("User");
            $table->foreign("NewsID")->references('ID')->on("News");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("NewsComment");
    }
}
