<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyJoinRequest extends Migration
{
    public static $table = "CompanyJoinRequest";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments("ID");
            $table->integer('UserID')->unsigned();
            $table->enum('Status', ['yes', 'no', 'pending']);

            // default for tracking
            $table->string("CreatedByID");
            $table->dateTime("CreatedDate");
            $table->string("CreatedByName");
            $table->dateTime("PushedDate")->nullable();

            $table->foreign('UserID')->references('ID')->on(CreateUsersTable::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists(self::$table);
    }
}
