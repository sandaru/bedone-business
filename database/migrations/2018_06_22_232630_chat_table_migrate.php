<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChatTableMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("Group", function (Blueprint $tb) {
            $tb->increments("ID");
            $tb->integer("CompanyID")->nullable()->unsigned();
            $tb->integer("UserID")->nullable()->unsigned();
            $tb->string("Name");

            // default for tracking
            $tb->string("CreatedByID");
            $tb->dateTime("CreatedDate");
            $tb->string("CreatedByName");
            $tb->string("ModifiedByID");
            $tb->dateTime("ModifiedDate");
            $tb->string("ModifiedByName");
        });

        Schema::table("Group", function (Blueprint $tb) {
            $tb->foreign('CompanyID')->references('ID')->on("Company");
            $tb->foreign('UserID')->references('ID')->on("User");
        });

        // Group member
        Schema::create("GroupMember", function (Blueprint $tb) {
            $tb->increments("ID");
            $tb->integer("GroupID")->nullable()->unsigned();
            $tb->integer("UserID")->nullable()->unsigned();
        });

        Schema::table("GroupMember", function (Blueprint $tb) {
            $tb->foreign('GroupID')->references('ID')->on("Group");
            $tb->foreign('UserID')->references('ID')->on("User");
        });

        // Chat room
        Schema::create("ChatRoom", function (Blueprint $tb) {
            $tb->increments("ID");
            $tb->integer("UserID")->unsigned();
            $tb->integer("ToUserID")->nullable()->unsigned();
            $tb->integer("GroupID")->nullable()->unsigned();

            $tb->enum("IsGroupChat", ['yes', 'no'])->default('no');
            $tb->datetime('LastActivityDate')->nullable();
            $tb->string('LastActivityMessage')->nullable();
        });

        Schema::table("ChatRoom", function (Blueprint $tb) {
            $tb->foreign('GroupID')->references('ID')->on("Group");
            $tb->foreign('UserID')->references('ID')->on("User");
            $tb->foreign('ToUserID')->references('ID')->on("User");
        });

        // Chat mess
        Schema::create("ChatMessage", function (Blueprint $tb) {
            $tb->increments("ID");
            $tb->integer("ChatRoomID")->unsigned();
            $tb->integer("UserID")->unsigned();

            $tb->enum('Type', ['text', 'sticker', 'image', 'video', 'sound'])->default('text');
            $tb->text("Message")->nullable();
            $tb->string("Source")->nullable();

            $tb->enum('Activate', ['yes', 'no'])->default('yes');

            $tb->string("CreatedByID");
            $tb->dateTime("CreatedDate");
            $tb->string("CreatedByName");
        });

        Schema::table("ChatMessage", function (Blueprint $tb) {
            $tb->foreign('ChatRoomID')->references('ID')->on("ChatRoom");
            $tb->foreign('UserID')->references('ID')->on("User");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("ChatMessage");
        Schema::dropIfExists("ChatRoom");
        Schema::dropIfExists("GroupMember");
        Schema::dropIfExists("Group");
    }
}
