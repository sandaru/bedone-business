<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingResource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('BookingResource', function (Blueprint $tb) {
            $tb->dropColumn('FeatureImage');

            $tb->enum('Activate', ['yes', 'no'])->default('yes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('BookingResource', function (Blueprint $tb) {
            $tb->dropColumn('Activate');

            $tb->string('FeatureImage');
        });
    }
}
