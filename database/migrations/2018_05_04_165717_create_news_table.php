<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    public static $table_news = "News";
    public static $table_newsCate = "NewsCategory";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table_newsCate, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("CompanyID")->unsigned();
            $table->string("Name");
            $table->text("Description");
            $table->string("Icon");

            // default for tracking
            $table->string("CreatedByID");
            $table->dateTime("CreatedDate");
            $table->string("CreatedByName");
            $table->string("ModifiedByID");
            $table->dateTime("ModifiedDate");
            $table->string("ModifiedByName");

            // foreign key
            $table->foreign('CompanyID')->references('ID')->on(CompanyRole::$table);
        });

        Schema::create(self::$table_news, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("CompanyID")->unsigned();
            $table->integer("NewsCategoryID")->unsigned();
            $table->string("Title");
            $table->string("Description");
            $table->longText("Content");
            $table->string("FeatureImage");

            // default for tracking
            $table->string("CreatedByID");
            $table->dateTime("CreatedDate");
            $table->string("CreatedByName");
            $table->string("ModifiedByID");
            $table->dateTime("ModifiedDate");
            $table->string("ModifiedByName");

            // foreign key
            $table->foreign('CompanyID')->references('ID')->on(CompanyRole::$table);
            $table->foreign('NewsCategoryID')->references('ID')->on(self::$table_newsCate);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table_newsCate);
        Schema::dropIfExists(self::$table_news);
    }
}
