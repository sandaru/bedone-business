<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyRole extends Migration
{
    public static $table = "Company";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(self::$table, function (Blueprint $tb) {
            $tb->increments('ID');
            $tb->string("Name");
            $tb->text("Description")->nullable();
            $tb->string("FeatureImage")->nullable();

            // default for tracking
            $tb->string("CreatedByID");
            $tb->dateTime("CreatedDate");
            $tb->string("CreatedByName");
            $tb->string("ModifiedByID");
            $tb->dateTime("ModifiedDate");
            $tb->string("ModifiedByName");
        });

        Schema::table(CreateUsersTable::$table, function (Blueprint $tb) {
            $tb->foreign('CompanyID')->references('ID')->on(CompanyRole::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists(self::$table);
    }
}
