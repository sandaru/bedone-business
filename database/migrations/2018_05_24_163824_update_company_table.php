<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyTable extends Migration
{
    private $upgrate_table = "Company";

    // references table
    private $ref_user_table = "User";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table($this->upgrate_table, function (Blueprint $table) {
            //$table->integer("DefaultUserProfileID")->unsigned()->nullable(); // profile will automatically
            $table->integer("UserAdminID")->unsigned()->nullable(); // this user is the only user have full action and not being change anything

            // foreign
            $table->foreign('UserAdminID')->references('ID')->on($this->ref_user_table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
