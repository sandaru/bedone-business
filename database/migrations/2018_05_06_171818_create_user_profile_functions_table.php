<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileFunctionsTable extends Migration
{
    public static $table = "UserProfileFunction";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("UserProfileModuleID")->unsigned();
            $table->integer("ModuleFunctionID")->unsigned();
            $table->enum("Status", ['on', 'off'])->default('off');

            // foreign
            $table->foreign("UserProfileModuleID")->references('ID')->on(CreateUserProfileModulesTable::$table);
            $table->foreign("ModuleFunctionID")->references('ID')->on(CreateModuleFunctionsTable::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
