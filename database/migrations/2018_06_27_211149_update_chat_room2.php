<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChatRoom2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("ChatRoom", function (Blueprint $tb) {
            $tb->dropForeign(['UserID']);
            $tb->dropColumn('UserID');
        });
        Schema::table("ChatRoom", function (Blueprint $tb) {
            $tb->integer('UserID')->nullable()->unsigned();
            $tb->foreign('UserID')->references('ID')->on("User");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("ChatRoom", function (Blueprint $tb) {
            $tb->dropForeign(['UserID']);
            $tb->dropColumn('UserID');
        });
        Schema::table("ChatRoom", function (Blueprint $tb) {
            $tb->integer('UserID')->unsigned();
            $tb->foreign('UserID')->references('ID')->on("User");
        });
    }
}
