<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileModulesTable extends Migration
{
    public static $table = "UserProfileModule";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("UserProfileID")->unsigned();
            $table->integer("ModuleID")->unsigned();
            $table->enum("Status", ['on', 'off'])->default('off');

            // foreign
            $table->foreign("ModuleID")->references('ID')->on(CreateModulesTable::$table);
            $table->foreign("UserProfileID")->references('ID')->on(CreateUserProfilesTable::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
