<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileAccessesTable extends Migration
{
    public static $table = "UserProfileAccess";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("UserID")->unsigned();
            $table->integer("UserProfileID")->unsigned();
            $table->dateTime('CreatedDate')->nullable();

            $table->foreign("UserID")->references('ID')->on(CreateUsersTable::$table);
            $table->foreign("UserProfileID")->references('ID')->on(CreateUserProfilesTable::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_accesses');
    }
}
