<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChatMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        \Illuminate\Support\Facades\DB::statement("ALTER TABLE `ChatMessage` 
          CHANGE `Type` `Type` ENUM('text', 'sticker', 'image', 'video', 'sound', 'system') DEFAULT 'text';");

        Schema::table("ChatMessage", function (Blueprint $tb) {
            $tb->dropForeign(['UserID']);
            $tb->dropColumn('UserID');
        });
        Schema::table("ChatMessage", function (Blueprint $tb) {
            $tb->integer("UserID")->nullable()->unsigned();
            $tb->foreign('UserID')->references('ID')->on("User");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::statement("ALTER TABLE `ChatMessage` 
          CHANGE `Type` `Type` ENUM('text', 'sticker', 'image', 'video', 'sound') DEFAULT 'text';");
        Schema::table("ChatMessage", function (Blueprint $tb) {
            $tb->dropForeign(['UserID']);
            $tb->dropColumn('UserID');
        });
        Schema::table("ChatMessage", function (Blueprint $tb) {
            $tb->integer("UserID")->unsigned();
            $tb->foreign('UserID')->references('ID')->on("User");
        });
    }
}
