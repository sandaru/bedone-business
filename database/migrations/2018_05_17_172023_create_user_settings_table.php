<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSettingsTable extends Migration
{
    public static $table = "UserSetting";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("UserID")->unsigned();

            $table->enum('Birthday', ['yes', 'no'])->default('yes');
            $table->enum('Gender', ['yes', 'no'])->default('yes');
            $table->enum('Email', ['yes', 'no'])->default('yes');
            $table->enum('WorkPhone', ['yes', 'no'])->default('yes');
            $table->enum('HomePhone', ['yes', 'no'])->default('yes');
            $table->enum('WorkAddress', ['yes', 'no'])->default('yes');

            // tracking modified
            $table->string("ModifiedByID");
            $table->dateTime("ModifiedDate");
            $table->string("ModifiedByName");

            $table->foreign('UserID')->references('ID')->on(CreateUsersTable::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
