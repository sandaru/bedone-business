<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyModulesTable extends Migration
{
    public static $table = "CompanyModule";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("CompanyID")->unsigned();
            $table->integer("ModuleID")->unsigned();

            // default for tracking
            $table->string("CreatedByID");
            $table->dateTime("CreatedDate");
            $table->string("CreatedByName");
            $table->string("ModifiedByID");
            $table->dateTime("ModifiedDate");
            $table->string("ModifiedByName");

            $table->foreign("ModuleID")->references('ID')->on(CreateModulesTable::$table);
            $table->foreign('CompanyID')->references('ID')->on(CompanyRole::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
