<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('Booking', function (Blueprint $tb) {
            $tb->dropColumn('StartTime');
            $tb->dropColumn('EndTime');
        });

        Schema::table('Booking', function (Blueprint $tb) {
            $tb->time('StartTime')->nullable();
            $tb->time('EndTime')->nullable();
            $tb->date('BookDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('Booking', function (Blueprint $tb) {
            $tb->dropColumn('BookDate');
            $tb->dropColumn('StartTime');
            $tb->dropColumn('EndTime');
        });

        Schema::table('Booking', function (Blueprint $tb) {
            $tb->bigInteger('StartTime')->nullable();
            $tb->bigInteger('EndTime')->nullable();
        });
    }
}
