<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTableChatInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("ChatRoomInfo", function (Blueprint $tb) {
             $tb->increments("ID");
             $tb->integer("ChatRoomID")->unsigned();
             $tb->integer("UserID")->unsigned();
             $tb->enum("IsArchived", ['yes', 'no'])->default('no');
             $tb->integer("LastChatMessageID")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("ChatRoomInfo");
    }
}
