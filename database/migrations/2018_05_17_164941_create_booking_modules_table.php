<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingModulesTable extends Migration
{
    public static $table_booking_resource = "BookingResource";
    public static $table_booking = "Booking";
    public static $table_booking_user = "BookingUser";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table_booking_resource, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("CompanyID")->unsigned();
            $table->string("Name");
            $table->text("Description");
            $table->string("FeatureImage");
            $table->string("Address");
            $table->string("Gallery")->nullable();

            // default for tracking
            $table->string("CreatedByID");
            $table->dateTime("CreatedDate");
            $table->string("CreatedByName");
            $table->string("ModifiedByID");
            $table->dateTime("ModifiedDate");
            $table->string("ModifiedByName");

            $table->foreign('CompanyID')->references('ID')->on(CompanyRole::$table);
        });

        Schema::create(self::$table_booking, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("CompanyID")->unsigned();
            $table->integer("BookingResourceID")->unsigned();
            $table->string("Title");
            $table->text("Description");
            $table->bigInteger("StartTime")->nullable();
            $table->bigInteger("EndTime")->nullable();
            $table->enum("IsAllDay", ['yes', 'no'])->default('no');
            $table->enum("IsPrivate", ['yes', 'no'])->default('no');
            $table->integer("AdminUserID")->unsigned();

            // default for tracking
            $table->string("CreatedByID");
            $table->dateTime("CreatedDate");
            $table->string("CreatedByName");
            $table->string("ModifiedByID");
            $table->dateTime("ModifiedDate");
            $table->string("ModifiedByName");

            $table->foreign('CompanyID')->references('ID')->on(CompanyRole::$table);
            $table->foreign('BookingResourceID')->references('ID')->on(self::$table_booking_resource);
            $table->foreign('AdminUserID')->references('ID')->on(CreateUsersTable::$table);
        });

        Schema::create(self::$table_booking_user, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("BookingID")->unsigned();
            $table->integer("UserID")->unsigned();

            $table->foreign('BookingID')->references('ID')->on(self::$table_booking);
            $table->foreign('UserID')->references('ID')->on(CreateUsersTable::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table_booking_user);
        Schema::dropIfExists(self::$table_booking);
        Schema::dropIfExists(self::$table_booking_resource);
    }
}
