<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJoinCompany extends Migration
{
    private $table = "CompanyJoinRequest";
    private $table_foreign = "Company";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $tb) {
            $tb->integer("CompanyID")->nullable()->unsigned();

            $tb->foreign("CompanyID")->references('ID')->on($this->table_foreign);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
