<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNotificationsTable extends Migration
{
    public static $table = "UserNotification";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::$table, function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('UserID')->unsigned();
            $table->integer('CompanyID')->unsigned()->nullable();
            $table->integer('BookingID')->unsigned()->nullable();
            $table->integer('CompanyJoinRequestID')->unsigned()->nullable();

            $table->enum('Type', [
                'InviteJoinOrg',
                'ResponseJoinOrg',
                'BookingAdded',
                'BookingNotification',
            ]);
            $table->enum('IsSeen', ['yes', 'no']);
            $table->integer("CreatedDate");

            $table->foreign('UserID')->references('ID')->on(CreateUsersTable::$table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
