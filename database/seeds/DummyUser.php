<?php

use Illuminate\Database\Seeder;
use App\Models;

class DummyUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a default user
        $totalUsers = Models\User::all()->count();

        if ($totalUsers <= 0)
        {
            // create administrator here
            $userAdmin = new Models\User;
            $userAdmin->Email = "admin@sethphat.com";
            $userAdmin->Password = config('bedone.DEFAULT_LOGIN_PASSWORD');
            $userAdmin->FirstName = "Admin";
            $userAdmin->LastName = "BeDone";
            $userAdmin->IsSuperAdmin = "yes";
            $userAdmin->CreatedByID = $userAdmin->ModifiedByID = 1;
            $userAdmin->CreatedByName = $userAdmin->ModifiedByName = "Admin BeDone";
            $userAdmin->CreatedDate = $userAdmin->ModifiedDate = date(config('bedone.DATETIME_FORMAT'));
            $userAdmin->save();

            // create administrator setting
            $adminSetting = new Models\UserSetting;
            $adminSetting->UserID = $userAdmin->ID;
            $adminSetting->ModifiedByID = 1;
            $adminSetting->ModifiedDate = date(config('bedone.DATETIME_FORMAT'));
            $adminSetting->ModifiedByName = "Admin BeDone";
            $adminSetting->save();


            // create a blank account here
            $userNormal = new Models\User;
            $userNormal->Email = "user@sethphat.com";
            $userNormal->Password =  config('bedone.DEFAULT_LOGIN_PASSWORD');
            $userNormal->FirstName = "Test";
            $userNormal->LastName = "Account";
            $userNormal->IsSuperAdmin = "no";
            $userNormal->CreatedByID = $userNormal->ModifiedByID = 1;
            $userNormal->CreatedByName = $userNormal->ModifiedByName = "Admin BeDone";
            $userNormal->CreatedDate = $userNormal->ModifiedDate = date(config('bedone.DATETIME_FORMAT'));
            $userNormal->save();

            // create blank acc setting
            $normalSetting = new Models\UserSetting;
            $normalSetting->UserID = $userNormal->ID;
            $normalSetting->ModifiedByID = 1;
            $normalSetting->ModifiedDate = date(config('bedone.DATETIME_FORMAT'));
            $normalSetting->ModifiedByName = "Admin BeDone";
            $normalSetting->save();
        }
    }
}
