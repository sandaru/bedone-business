<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 5/22/2018
 * Time: 10:36 PM
 */

/**
 * Encrypt id
 * @param $str
 * @return string
 */
function encode_id($str)
{
    return encrypt($str);
}

/**
 * Decrypt ID
 * @param $str
 * @return string
 */
function decode_id($str)
{
    $id = "";
    try
    {
        $id = decrypt($str);
    }
    catch(\Illuminate\Contracts\Encryption\DecryptException $e) {
        $id = "failed";
    }

    return $id;
}

/**
 * Encrypt for password only
 * @param $str
 * @return string
 */
function password_encrypt($str)
{
    $arrStr = str_split($str);

    $pass = "";
    foreach ($arrStr as $char)
        $pass .= sha1($char);

    return sha1(md5($pass . sha1($pass)));
}

/**
 * Get user avatar
 */
function profile_picture($user_profile_pic)
{
    if (empty($user_profile_pic))
    {
        // wrong
        return asset('img/default-avatar.png');
    }
    else {
        // generate
        return asset($user_profile_pic);
    }
}

/**
 * Get Group avatar
 */
function group_picture($user_profile_pic)
{
    if (empty($user_profile_pic))
    {
        // wrong
        return asset('img/groups.png');
    }
    else {
        // generate
        return asset($user_profile_pic);
    }
}

/**
 * Get user feature image
 */
function feature_image($user_profile_pic)
{
    if (empty($user_profile_pic))
    {
        // wrong
        return asset('img/default_feature.png');
    }
    else {
        // generate
        return asset($user_profile_pic);
    }
}

/**
 * SQL DateTime Now
 */
function sql_date()
{
    return date(config('bedone.DATETIME_FORMAT'));
}

/**
 * Explode ,
 * @param $string
 */
function getTagValue($string)
{
    $exp = explode(",", $string);
    return array_map('trim', $exp);
}

/**
 * String to boolean
 * @param $var
 */
function stringToBoolean($var)
{
    if ($var === "true")
        return true;
    else
        return false;
}

/**
 * Set logged data for SQL
 * @param $obj
 * @param $userObj
 * @param bool $created
 */
function setObjLoggedInfo(&$obj, $userObj, $created = false)
{
    if ($created) {
        $obj->CreatedDate = now();
        $obj->CreatedByName = $userObj->full_name;
        $obj->CreatedByID = $userObj->ID;
    }

    $obj->ModifiedDate = now();
    $obj->ModifiedByName = $userObj->full_name;
    $obj->ModifiedByID = $userObj->ID;
}

/**
 * Parse date from this format to that format
 * @param $date
 * @param $format
 * @return string date|datetime|time
 */
function toDate($date, $format) {
    return \Carbon\Carbon::parse($date)->format($format);
}

/**
 * Parse time
 * @param string $hour
 * @param string $minute
 * @param int $second
 * @return \Carbon\Carbon
 */
function carbonTime($hour = '', $minute = '', $second = 0){
    return \Carbon\Carbon::createFromTime(empty($hour) ? date('H') : $hour,
                                            empty($minute) ? date('i') : $minute, $second);
}

/**
 * Unix time
 * @param $str
 * @return int
 */
function sqlDateToUnixTime($str) {
    return \Carbon\Carbon::createFromFormat(config('bedone.DATETIME_FORMAT'), $str)->timestamp;
}

/**
 * Check if is today
 * @param $date
 * @return bool
 */
function isTodayDate($date) {
    return $date === date(config('bedone.DATE_FORMAT'));
}