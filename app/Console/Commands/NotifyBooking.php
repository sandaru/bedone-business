<?php

namespace App\Console\Commands;

use App\Models\Booking;
use App\Models\UserNotification;
use Illuminate\Console\Command;

class NotifyBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alert booking if nearly...';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // TODO: Get all booking with the time now + 5 = booking_start_time
        $listBooking = Booking::GetNearlyBooking(config('bedone.NOTIFY_BEFORE_MINUTE'));

        $this->info('Booking ready for notify: ' . $listBooking->count());

        // TODO: Alert all user
        foreach ($listBooking as $booking) {
            $this->info('=> Booking Title: ' . $booking->Title . '[Running]...');
            foreach ($booking->BookingUser as $user) {
                // notify now
                UserNotification::sendBookingAlert($booking->ID, $user->UserID);
                $this->info('+ [SENDED] For UserID: ' . $user->UserID);
            }
            $this->info('[Ending...]');
        }

        $this->info('Ending notify - Thanks for using');
    }
}
