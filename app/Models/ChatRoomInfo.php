<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/26/2018
 * Time: 10:26 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ChatRoomInfo extends Model
{
    protected $table = "ChatRoomInfo";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public static function CreateChatInfo($user_id, $room_id) {
        // create new one
        $info = new self;
        $info->UserID = $user_id;
        $info->ChatRoomID = $room_id;
        $info->save();

        return $info;
    }

    public static function SetArchive($user_id, $room_id) {
        $info = self::query()->where('UserID', $user_id)
                            ->where('ChatRoomID', $room_id)
                            ->first();

        if ($info === null) {
            $info = self::CreateChatInfo($user_id, $room_id);
        }

        // set archive
        $info->IsArchived = 'yes';
        $info->save();
    }

    public static function SetUnArchive($user_id, $room_id) {
        $info = self::query()->where('UserID', $user_id)
            ->where('ChatRoomID', $room_id)
            ->first();

        if ($info == null) {
            return;
        }
        $info->IsArchived = 'no';
        $info->save();
    }

    public static function RemoveInfo($user_id, $room_id) {
        self::query()->where('UserID', $user_id)
            ->where('ChatRoomID', $room_id)
            ->delete();
    }
}