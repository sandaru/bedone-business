<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Booking extends Model
{
    public static $snakeAttributes = false;
    protected $table = "Booking";
    protected $primaryKey = "ID";
    public $timestamps = false;

    // accessor

    // relationship
    public function BookingResource() {
        return $this->belongsTo('App\Models\BookingResource', 'BookingResourceID');
    }

    public function BookingUser() {
        return $this->hasMany('App\Models\BookingUser', 'BookingID');
    }

    public function AdminUser() {
        return $this->belongsTo('App\Models\User', 'AdminUserID');
    }

    public static function GetByID($id) {
        $item = Booking::find($id);
        if ($item == null)
            return null;

        return self::dataPostProcessing([$item])[0];
    }

    // func
    public static function SearchMonthBooking($company_id, $user_id, $canAccessPrivate, $start, $end) {
        // query now
        $list = Booking::query()
                        ->with('BookingUser')
                        ->where('CompanyID', $company_id)
                        ->where('Activate', 'yes')
                        ->whereBetween('BookDate', [$start, $end]);

//        if ($canAccessPrivate == false) {
//            $list->where(function ($query) use ($user_id) {
//                $query->where('IsPrivate', 'no');
//                $query->orWhere(function($query) use ($user_id) {
//                    // even private, if user is a part of it, get it.
//                    $query->whereHas('BookingUser', function ($query) use ($user_id) {
//                        $query->where('UserID', $user_id);
//                    });
//                    $query->orWhere('AdminUserID', $user_id);
//                });
//            });
//        }
//
//        $list->whereHas('BookingUser', function ($query) use ($user_id) {
//            $query->where('UserID', $user_id);
//        });
//        $list->orWhere('AdminUserID', $user_id);
        $result = $list->get();

        foreach($result as $row) {
            if ($canAccessPrivate) {
                continue;
            }

            // private mode, do not show any info
            if ($row->IsPrivate === 'yes' && $row->BookingUser->where('UserID', $user_id)->count() <= 0) {
                $row->Title = __('system.PrivateBooking');
            }
        }

        return self::dataPostProcessing($result);
    }

    public static function GetUserBookingFromNow($user_id) {
        $list = Booking::query()
                        ->with(['BookingUser'])
                        ->where('BookDate', ">=", date(config('bedone.DATE_FORMAT')))
                        ->where('Activate', 'yes')
                        ->orderBy('BookDate', 'ASC');

        $list->where(function($query) use ($user_id) {
            $query->whereHas('BookingUser', function ($query) use ($user_id) {
                $query->where('BookingUser.UserID', $user_id);
            });
            $query->orWhere('AdminUserID', $user_id);
        });

        return $list->get();
    }

    public static function GetAllUserBooking($user_id) {
        $list = Booking::query()
                        ->with(['BookingUser'])
                        ->whereHas('BookingUser', function ($query) use ($user_id) {
                            $query->where('BookingUser.UserID', $user_id);
                        });

        return $list->get();
    }

    public static function GetNearlyBooking($time_limit) {
        // time solving
        $time = carbonTime(date('H'), date('i'));
        $time->addMinutes($time_limit);

        // get
        $list = Booking::query()
            ->with(['BookingUser'])
            ->where('BookDate', "=", date(config('bedone.DATE_FORMAT')))
            ->where('StartTime', '=', $time->format('H:i:s'))
            ->where('Activate', 'yes')
            ->where('IsAllDay', 'no');

        return $list->get();
    }

    public static function dataPostProcessing($result) {
        // compressing data
        $data = [];
        foreach ($result as $row) {
            $perRow = [
                'ID'                    => encode_id($row->ID),
                'AdminUserID'           => $row->AdminUserID,
                'start'                 => $row->BookDate . (!empty($row->StartTime) ? "T" . $row->StartTime : ""),
                'end'                   => $row->BookDate . (!empty($row->EndTime) ? "T" . $row->EndTime : ""),
                'title'                 => $row->Title,
                'Description'           => $row->Description,
                'BookDate'              => $row->BookDate,
                'allDay'                => $row->IsAllDay === 'yes' ? true : false,
                'IsPrivate'             => $row->IsPrivate === 'yes' ? true : false,
                'AdminUserName'         => $row->CreatedByName,
                'BookingResourceID'     => $row->BookingResourceID,
                'Resource'              => [
                    'Name'              => $row->BookingResource->Name,
                    'Description'       => $row->BookingResource->Description,
                    'Address'           => $row->BookingResource->Address,
                    'Gallery'           => $row->BookingResource->Gallery,
                ],
                'Users'                 => [],
            ];

            // user things
//            $perRow['Users'][] = [
//                'Name'              => $row->AdminUser->full_name,
//                'ProfileImage'      => $row->AdminUser->ProfileImage,
//            ];
            foreach ($row->BookingUser as $user) {
                $perRow['Users'][] = $user->UserID;
//                $perRow['Users'][] = [
//                    'Name'              => $user->User->full_name,
//                    'ProfileImage'      => $user->User->ProfileImage,
//                ];
            }

            $data[] = $perRow;
        }

        return $data;
    }

    /**
     * Check if resource available for booking
     * @param $resource_id
     * @param $company_id
     */
    public static function IsResourceAvailable($resource_id, $company_id, $date, $start_time = '', $end_time = '') {
        $list = Booking::query()
                        ->where('BookingResourceID', $resource_id)
                        ->where('CompanyID', $company_id)
                        ->where('Activate', 'yes')
                        ->where('BookDate', $date);

        if ($start_time == '') {
            $start_time = "00:00:00";
        }
        if ($end_time == '') {
            $end_time = "23:59:59";
        }

        $list->where(function($query) use ($start_time, $end_time) {
            $query->where('IsAllDay', 'yes');
            $query->orWhere(function($query) use ($start_time, $end_time) {
                $query->where(function ($query) use ($start_time) {
                    $query->where('StartTime', '<=', $start_time);
                    $query->where('EndTime', '>=', $start_time);
                });
                $query->orWhere(function ($query) use ($end_time) {
                    $query->where('StartTime', '<=', $end_time);
                    $query->where('EndTime', '>=', $end_time);
                });
                $query->orWhere(function ($query) use ($start_time, $end_time) {
                    $query->whereRaw('? <= StartTime', $start_time);
                    $query->whereRaw('EndTime <= ?', $end_time);
                });
            }); // or where ((start_tme >= 7:10 && end_time <= 7:10) or () or ())
        });

        $total = $list->count();
        return $total <= 0;
    }
}
