<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/26/2018
 * Time: 10:26 PM
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ChatMessage extends Model
{
    protected $table = "ChatMessage";
    protected $primaryKey = "ID";
    public $timestamps = false;

    // CONFIG CONSTANT
    CONST ADD_MEMBER = "addMember";
    CONST DEL_MEMBER = "delMember";
    CONST LEAVE_GROUP = "leaveGroup";

    // relationship
    public function User() {
        return $this->belongsTo('App\Models\User', 'UserID');
    }

    // needed func
    public static function GetRoomMessage($room_id, $user_id, $start = 0, $limit = 10) {
        $item = self::query()
            ->where('ChatRoomID', $room_id)
            ->orderBy('CreatedDate', 'DESC')
//            ->join('ChatRoomInfo', 'ChatRoomInfo.ChatRoomID', '=', 'ChatMessage.ChatRoomID')
//            ->where(DB::raw("(ChatRoomInfo.LastChatMessageID IS NULL OR ChatMessage.ID > ChatRoomInfo.LastChatMessageID)"))
            ->skip($start)
            ->take($limit)
            ->get();

        $result = [];
        foreach ($item as $row) {
            $result[] = self::messPostProcess($row);
        }

        return $result;
    }

    public static function GetMessAfter($room_id, $after_date, $user_id) {
        $item = self::query()
            ->where('ChatRoomID', $room_id)
            ->where('UserID', '!=', $user_id)
            ->where('CreatedDate', '>', $after_date)
            ->orderBy('CreatedDate', 'ASC')
            ->get();

        if ($item->count() > 0) {
            $result = [];
            foreach ($item as $row) {
                $result[] = self::messPostProcess($row);
            }
            return $result;
        } else {
            return false;
        }
    }

    //
    public static function GetLatestMess($room_id) {
        $item = self::query()
                            ->select(['CreatedDate'])
                            ->orderBy('CreatedDate', 'desc')
                            ->where('ChatRoomID', $room_id)
                            ->first();

        return $item;
    }

    public static function messPostProcess($mess, $afterMess = "") {
        if ($mess->CreatedDate instanceof Carbon) {
            $mess->CreatedDate = $mess->CreatedDate->format(config('bedone.DATETIME_FORMAT'));
        }

        $info = [
            'ID'    => $mess->ID,
            'Type'  => $mess->Type,
            'Message'  => $mess->Message,
            'Source'  => $mess->Source,
            'CreatedByID'   => $mess->UserID,
            'CreatedByName'  => $mess->CreatedByName,
            'CreatedDate'  => $mess->CreatedDate,
            'CreatedFirstName' => empty($mess->UserID) ? "" : $mess->User->FirstName,
            'CreatedDateTimestamp'  => sqlDateToUnixTime($mess->CreatedDate),
            'ProfileImage' => empty($mess->UserID) ? "" : $mess->User->ProfileImage,
        ];

        if (!empty($afterMess)) {
            $info['LatestMessage'] = $afterMess;
        }

        return $info;
    }

    private static function DefaultMessage($room_id, $userObj) {
        $mess = new self;
        $mess->ChatRoomID = $room_id;
        $mess->UserID = $userObj->ID;
        $mess->CreatedByID = $userObj->ID;
        $mess->CreatedDate = now();
        $mess->CreatedByName = $userObj->full_name;
        return $mess;
    }

    public static function SendTextMessage($room_id, $userObj, $text) {
        $mess = self::DefaultMessage($room_id, $userObj);
        $mess->Type = 'text';
        $mess->Message = $text;
        $mess->save();
        $afterMess = self::UpdateChatRoomAfterMessage($room_id, $mess, $userObj);
        $mess->load('User');
        return self::messPostProcess($mess, $afterMess);
    }

    public static function SendImageMessage($room_id, $userObj, $source) {
        $mess = self::DefaultMessage($room_id, $userObj);
        $mess->Type = 'image';
        $mess->Message = __('system.attachment_included');
        $mess->Source = $source;
        $mess->save();
        $afterMess = self::UpdateChatRoomAfterMessage($room_id, $mess, $userObj);
        $mess->load('User');
        return self::messPostProcess($mess, $afterMess);
    }

    public static function SendSystemMessage($room_id, $type, $adderObj, $joinerObj) {
        $mess = new self;
        $mess->Type = 'system';
        $mess->ChatRoomID = $room_id;
        $mess->CreatedByID = $adderObj->ID;
        $mess->CreatedDate = now();
        $mess->CreatedByName = $adderObj->full_name;
        switch ($type) {
            case self::ADD_MEMBER: {
                $mess->Message = __('system.mess_add_user', ['adder' => $adderObj->full_name, 'newer' => $joinerObj->full_name]);
                break;
            }
            case self::DEL_MEMBER: {
                $mess->Message = __('system.mess_rem_user', ['adder' => $adderObj->full_name, 'newer' => $joinerObj->full_name]);
                break;
            }
            case self::LEAVE_GROUP: {
                $mess->Message = __('system.mess_leave', ['user' => $adderObj->full_name]);
                break;
            }
        }
        $mess->save();
        self::UpdateChatRoomAfterMessage($room_id, $mess, $adderObj);
    }

    public static function UpdateChatRoomAfterMessage($room_id, $last_mess, $userObj) {
        $chat_room = ChatRoom::find($room_id);
        switch ($last_mess->Type) {
            case "text":
                $chat_room->LastActivityMessage = $userObj->FirstName . ": ". $last_mess->Message;
                break;
            case "sticker":
            case "image":
            case "video":
            case "sound":
                $chat_room->LastActivityMessage = __('system.attachment_included');
                break;
            default:
                $chat_room->LastActivityMessage = $last_mess->Message;
        }
        $chat_room->LastActivityDate = $last_mess->CreatedDate;
        $chat_room->save();

        return $chat_room->LastActivityMessage;
    }
}