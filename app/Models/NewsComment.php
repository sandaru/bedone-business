<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsComment extends Model
{
    public static $snakeAttributes = false;
    protected $table = "NewsComment";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'UserID');
    }
    /**
     * Get news comment
     * @param $offset
     * @param $limit
     * @param $newsid
     * @return mixed
     */
    public static function GetComment($offset, $limit, $newsid)
    {
        $list_news = NewsComment::offset($offset)
                            ->limit($limit)
                            ->where('NewsID', $newsid)
                            ->orderBy('CreatedDate', 'ASC');

        $data = $list_news->get();
        $return_data = [];
        foreach ($data as $item)
        {
            $row['ID'] = encode_id($item->ID);

            $return_data[] = [
                'ID'                    => encode_id($item->ID),
                'UserID'                => encode_id($item->UserID),
                'Comment'               => $item->Comment,
                'ProfileImage'          => $item->User->ProfileImage,
                'CreatedByName'         => $item->CreatedByName,
                'CreatedDate'           => $item->CreatedDate,
            ];
        }

        return [
            'Data'  => $return_data,
            'Total' => self::countAllComment($newsid)
        ];
    }

    public static function countAllComment($newsid)
    {
        return NewsComment::where('NewsID', $newsid)->select('ID')->count();
    }

    /**
     * Create comment
     * @param $newsid
     * @param $comment
     * @param $userObj
     */
    public static function createComment($newsid, $comment, $userObj)
    {
        $cmt = new NewsComment;
        $cmt->UserID = $userObj->ID;
        $cmt->NewsID = $newsid;
        $cmt->Comment = $comment;
        $cmt->CreatedByID = $cmt->ModifiedByID = $userObj->ID;
        $cmt->CreatedByName = $cmt->ModifiedByName = $userObj->full_name;
        $cmt->CreatedDate = $cmt->ModifiedDate = sql_date();
        $cmt->save();

        return $cmt->ID;
    }
}
