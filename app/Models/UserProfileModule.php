<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class UserProfileModule extends Model
{
    public static $snakeAttributes = false;
    protected $table = "UserProfileModule";
    protected $primaryKey = "ID";
    public $timestamps = false;
    protected $fillable = [
        'ModuleID',
        'UserProfileID',
        'Status'
    ];

    public function Module()
    {
        return $this->belongsTo("App\Models\Module", "ModuleID");
    }

    public function Functions()
    {
        return $this->hasMany('App\Models\UserProfileFunction', "UserProfileModuleID");
    }

    public function scopeFilterByProfileID($query, $profile_id) {
        return $query->where('UserProfileID', $profile_id);
    }
    public function scopeFilterStatusOn($query) {
        return $query->where('Status', 'on');
    }

    /**
     * get this by module id
     * @param $module_id
     * @param $user_profile_id
     */
    public static function GetByModuleID($module_id, $user_profile_id)
    {
        return UserProfileModule::where('ModuleID', $module_id)
            ->where('UserProfileID', $user_profile_id)
            ->first();
    }

    /**
     * Get this by module name
     * @param $module_name
     * @param $user_profile_id
     * @return UserProfileModule|\Illuminate\Database\Eloquent\Builder|Model|null|object
     */
    public static function GetByModuleName($module_name, $user_profile_id)
    {
        return UserProfileModule::with('Module')
            ->whereHas('Module', function ($query) use ($module_name) {
                $query->where('Name', $module_name);
            })
            ->where('UserProfileID', $user_profile_id)
            ->first();
    }

    /**
     * Create a profile module
     * @param $profile_id
     * @param $module_id
     * @param $status
     */
    public static function createProfileModule($profile_id, $module_id, $status) {
        // insert
        $profile_module = new UserProfileModule;
        $profile_module->UserProfileID = $profile_id;
        $profile_module->ModuleID = $module_id;
        $profile_module->Status = $status === true ? "on" : "off";
        $profile_module->save();

        return $profile_module;
    }

    /**
     * Can access module???
     * @param $module_name
     * @param $profile_id
     */
    public static function canAccessModule($module_name, $profile_id)
    {
        $info = UserProfileModule::query()
                                ->with('Module')
                                ->whereHas('Module', function ($query) use ($module_name) {
                                    $query->where('Name', $module_name);
                                })
                                ->where('UserProfileID', $profile_id)
                                ->where('Status', 'on')
                                ->first();

        if ($info != null)
            return $info->ModuleID;
        else
            return false;
    }

    public static function HaveChildrenOff($profile_id, $module_id) {
        $all_child = Module::where('ParentID', $module_id)->pluck('ID');
        $total_off_children = self::query()
                                    ->where('UserProfileID', $profile_id)
                                    ->whereIn('ModuleID', $all_child)
                                    ->where('Status', 'off')
                                    ->count();

        return $total_off_children > 1;
    }
}