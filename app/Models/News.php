<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public static $snakeAttributes = false;
    protected $table = "News";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function Category()
    {
        return $this->belongsTo("App\Models\NewsCategory", "NewsCategoryID");
    }

    public function Comments() {
        return $this->hasMany('App\Models\NewsComment', 'NewsID');
    }

    // accessor
    public function getNewsFeatureImageAttribute()
    {
        return explode(",", $this->FeatureImage);
    }

    /**
     * Get news
     * @param $offset
     * @param $limit
     * @param $company_id
     * @param null $category_id
     * @return mixed
     */
    public static function GetNews($offset, $limit, $company_id, $category_id = null)
    {
        $list_news = News::offset($offset)
                            ->limit($limit)
                            ->where('CompanyID', $company_id)
                            ->orderBy('CreatedDate', 'DESC');

        if ($category_id != null)
            $list_news = $list_news->where("NewsCategoryID", $category_id);

        $data = $list_news->with('Category')->get();
        $return_data = [];
        foreach ($data as $item)
        {
            $row['ID'] = encode_id($item->ID);
            $row['NewsCategoryID'] = encode_id($item->NewsCategoryID);
            $row['CategoryName'] = $item->Category->Name;

            $featureImg = $item->NewsFeatureImage;
            if ($featureImg == null || $featureImg == [""])
                $featureImg = [];

            $return_data[] = [
                'ID'                    => encode_id($item->ID),
                'NewsCategoryID'        => encode_id($item->NewsCategoryID),
                'Title'                 => $item->Title,
                'Description'           => $item->Description,
                'FeatureImage'          => $featureImg,
                'Content'               => $item->Content,
                'CategoryName'          => $item->Category->Name,
                'CreatedByName'         => $item->CreatedByName,
                'CreatedDate'           => toDate($item->CreatedDate, config('bedone.DATETIME_FORMAT_SHOW')),
            ];
        }

        return [
            'Data'  => $return_data
        ];
    }

    /**
     * Create news
     * @param $category_id
     * @param $title
     * @param $description
     * @param $content
     * @param $imgs
     * @param $userObj
     */
    public static function createNews($category_id, $title, $description, $content, $imgs, $userObj)
    {
        $news = new News;
        $news->NewsCategoryID = $category_id;
        $news->CompanyID = $userObj->CompanyID;
        $news->Title = $title;
        $news->Description = $description;
        $news->Content = $content;
        $news->FeatureImage = implode(",", $imgs);
        $news->CreatedByID = $news->ModifiedByID = $userObj->ID;
        $news->CreatedByName = $news->ModifiedByName = $userObj->full_name;
        $news->CreatedDate = $news->ModifiedDate = sql_date();
        $news->save();

        return $news;
    }

    /**
     * Edit news
     * @param $id
     * @param $category_id
     * @param $title
     * @param $description
     * @param $content
     * @param $imgs
     * @param $userObj
     */
    public static function editNews($id, $category_id, $title, $description, $content, $imgs, $userObj)
    {
        $news = News::find($id);
        $news->NewsCategoryID = $category_id;
        $news->CompanyID = $userObj->CompanyID;
        $news->Title = $title;
        $news->Description = $description;
        $news->Content = $content;
        $news->FeatureImage = implode(",", $imgs);
        $news->ModifiedByID = $userObj->ID;
        $news->ModifiedByName = $userObj->full_name;
        $news->ModifiedDate = sql_date();
        $news->save();

        return $news;
    }

    public function delete()
    {
        // delete news :(
        $this->Comments()->delete(); // delete comment if exists
        return parent::delete(); // delete news
    }
}
