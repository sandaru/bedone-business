<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class CompanyJoinRequest extends Model
{
    protected $table = "CompanyJoinRequest";
    protected $primaryKey = "ID";
    public $timestamps = false;

    // relation, get company
    public function Company()
    {
        return $this->belongsTo('App\Models\Company','CompanyID');
    }

    // relationship - user
    public function User()
    {
        return $this->belongsTo('App\Models\User', 'UserID');
    }

    /**
     * Join company
     * @param $company_id
     * @param $userObj
     * @return boolean
     */
    public static function joinCompany($company_id, $userObj)
    {
        $company = Company::find($company_id);

        if ($company == null)
            return false;

        $joinRequest = new CompanyJoinRequest;
        $joinRequest->UserID = $userObj->ID;
        $joinRequest->CompanyID = $company_id;
        $joinRequest->Status = 'pending';
        $joinRequest->CreatedByID = $userObj->ID;
        $joinRequest->CreatedByName = $userObj->full_name;
        $joinRequest->CreatedDate = $joinRequest->PushedDate = sql_date();
        $joinRequest->save();

        return true;
    }

    public static function GetJoinList($company_id, $keyword, $filter_by)
    {
        $query = CompanyJoinRequest::query()
                                    ->with('User')
                                    ->orderBy('PushedDate', 'DESC')
                                    ->where('CompanyID', $company_id);

        switch ($filter_by)
        {
            case "full_name":
                {
                    $query->whereHas('User', function ($query) use ($keyword) {
                        $query->whereRaw("CONCAT(FirstName, ' ', LastName) LIKE ?", ['%'.$keyword.'%']);
                    });
                    break;
                }
            case "email":
                {
                    $query->whereHas('User', function ($query) use ($keyword) {
                        $query->where('Email', 'like', '%'.$keyword.'%');
                    });
                    break;
                }
        }

        return $query->paginate(config('bedone.MAX_USER_RESULT'));
    }
}
