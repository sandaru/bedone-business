<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfileAccess extends Model
{
    protected $table = "UserProfileAccess";
    protected $primaryKey = "ID";
    public $timestamps = false;

    // relationship - user profile
    public function UserProfile()
    {
        return $this->belongsTo("App\Models\UserProfile", "UserProfileID");
    }

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'UserID');
    }

    /**
     * Set user for profile
     * @param $user_id
     * @param $user_profile_id
     */
    public static function setUserProfile($user_id, $user_profile_id)
    {
        $access = new UserProfileAccess;
        $access->UserID = $user_id;
        $access->UserProfileID = $user_profile_id;
        $access->CreatedDate = sql_date();
        $access->save();
    }

    public static function userByProfile($profile_id)
    {
        $data = [];
        $list = UserProfileAccess::where('UserProfileID', $profile_id)->get();

        foreach ($list as $item) {
            $data[] = [
                'Name'      => $item->User->full_name,
                'Email'     => $item->User->Email
            ];
        }

        return ['data' => $data];
    }
}
