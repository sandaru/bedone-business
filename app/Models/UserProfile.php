<?php

namespace App\Models;

use function foo\func;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = "UserProfile";
    protected $primaryKey = "ID";
    public $timestamps = false;

    // relationship - UserModule
    public function UserProfileModule()
    {
        return $this->hasMany('App\Models\UserProfileModule', 'UserProfileID');
    }

    // relationship - UserProfileAccess
    public function UserProfileAccess() {
        return $this->hasMany('App\Models\UserProfileAccess', 'UserProfileID');
    }

    /**
     * Search profile
     * @param $company_id
     * @param string $keyword
     * @param string $filter_by
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function SearchProfile($company_id, $keyword = '', $filter_by = 'name')
    {
        $query = UserProfile::query()
                                ->where('CompanyID', $company_id);

        switch ($filter_by)
        {
            case "name":
                $query->where('Name','like', "%".$keyword."%");
                break;
            case "description":
                $query->where('Description','like', "%".$keyword."%");
                break;
            case "creator":
                $query->where('CreatedByName','like', "%".$keyword."%");
                break;
        }

        return $query->paginate(config('bedone.MAX_USER_RESULT'));
    }

    public static function GetProfileByCompany($company_id, $canAccessMasterAdmin = false) {
        $query = UserProfile::query()
            ->where('CompanyID', $company_id);

        if (!$canAccessMasterAdmin) {
            $query->where('IsMasterAdmin', 'no');

        }

        return $query->get();
    }

    /**
     * Check if this profile is belong in this company only
     * @param $profile_id
     * @param $company_id
     */
    public static function isProfileInCompany($profile_id, $company_id)
    {
        $profile = UserProfile::where('ID', $profile_id)
                                ->where('CompanyID', $company_id)
                                ->count();

        return $profile > 0;
    }

    /**
     * Check if this profile is master admin
     * @param $profile_id
     */
    public static function IsMasterAdmin($profile_id) {
        $profile = self::find($profile_id);
        return $profile->IsMasterAdmin == 'yes';
    }

    /**
     * Get a new user profile
     * @param $name
     * @param $description
     * @param $userObj
     */
    public static function createUserProfile($name, $description, $userObj)
    {
        $profile = new UserProfile;
        $profile->Name = $name;
        $profile->Description = $description;
        $profile->CompanyID = $userObj->CompanyID;
        $profile->CreatedByID = $profile->ModifiedByID = $userObj->ID;
        $profile->CreatedByName = $profile->ModifiedByName = $userObj->full_name;
        $profile->CreatedDate = $profile->ModifiedDate = now();
        $profile->save();

        return $profile;
    }

    public function delete()
    {
        // delete relationship
        foreach ($this->UserProfileModule as $module) {
            $module->Functions()->delete();
            $module->delete();
        }

        return parent::delete();
    }
}
