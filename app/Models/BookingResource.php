<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class BookingResource extends Model
{
    public static $snakeAttributes = false;
    protected $table = "BookingResource";
    protected $primaryKey = "ID";
    public $timestamps = false;

    protected $hidden = [
        'CreatedDate',
        'CreatedByID',
        'CreatedByName',
        'CompanyID',
        'ModifiedDate',
        'ModifiedByID',
        'ModifiedByName',
    ];

    // acessor
    public function getGalleryItemsAttribute() {
        $exp = explode(",", $this->Gallery);
        return $exp !== [''] ? $exp : [];
    }

    // relationship


    // solving getting...

    /**
     * Search Booking
     * @param $company_id
     * @param $keyword
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function SearchBooking($company_id, $keyword) {
        $list = BookingResource::query()
                                ->where('CompanyID', $company_id)
                                ->where('Activate', 'yes')
                                ->where(function ($query) use ($keyword) {
                                    $query->where('Name', 'like', '%'. $keyword .'%');
                                    $query->orWhere('Description', 'like', '%'. $keyword .'%');
                                });

        return $list->paginate(config('bedone.MAX_RESULT'));
    }

    /**
     * @param $company_id
     */
    public static function GetCompanyResource($company_id) {
        $list = BookingResource::query()
                        ->where('CompanyID', $company_id)
                        ->where('Activate', 'yes')
                        ->get()
                        ->toArray();

        $result = [];
        foreach ($list as $row) {
            $row['ID'] = encode_id($row['ID']);
            $result[] = $row;
        }

        return $result;
    }

    public static function FindCompanyResource($resource_id, $company_id) {
        return BookingResource::query()
            ->where('CompanyID', $company_id)
            ->where('ID', $resource_id)
            ->first();
    }
}
