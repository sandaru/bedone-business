<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/26/2018
 * Time: 9:39 PM
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ChatRoom extends Model
{
    protected $table = "ChatRoom";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function FromUser() {
        return $this->belongsTo('App\Models\User', 'UserID');
    }

    public function ToUser() {
        return $this->belongsTo('App\Models\User', 'ToUserID');
    }

    public function Group() {
        return $this->belongsTo('App\Models\Group', 'GroupID');
    }

    public function ChatInfo() {
        return $this->hasMany('App\Models\ChatRoomInfo', 'ChatRoomID');
    }
    public function ChatMessages() {
        return $this->hasMany('App\Models\ChatMessage', 'ChatRoomID');
    }


    public static function GetUserChatRoom($from_user_id, $to_user_id) {
        $room = ChatRoom::query()
                            ->where(function ($query) use ($from_user_id, $to_user_id) {
                                $query->where('UserID', $from_user_id);
                                $query->where('ToUserID', $to_user_id);
                            })
                            ->orWhere(function ($query) use ($from_user_id, $to_user_id) {
                                $query->where('UserID', $to_user_id);
                                $query->where('ToUserID', $from_user_id);
                            })
                            ->first();
        return $room;
    }
    public static function GetGroupChatRoom($group_id) {
        $room = ChatRoom::query()
                    ->where("GroupID", $group_id)
                    ->first();
        return $room;
    }

    public static function GetAllUserChatRoom($user_id) {
        $room = ChatRoom::query()
                            ->where('UserID', $user_id)
                            ->orWhere('ToUserID', $user_id);
        return $room;
    }

    public static function GetChatRoom($user_id, $keyword = '', $start = 0, $limit = 10) {
        $rooms = ChatRoom::query()
                        ->with(['Group', 'Group.Members', 'ToUser', 'ChatInfo'])
                        ->where(function ($query) use ($user_id) {
                            $query->where("UserID", $user_id)
                                ->orWhere("ToUserID", $user_id)
                                ->orWhereHas('Group.Members', function ($query) use ($user_id) {
                                    $query->where('UserID', $user_id);
                                });
                        })
                        ->whereHas('ChatInfo', function ($query) use ($user_id) {
                            $query->where('UserID', $user_id);
                            $query->where('IsArchived', 'no');
                        })
                        ->skip($start)
                        ->take($limit)
                        ->orderBy('LastActivityDate', 'DESC');

        if ($keyword !== '') {
            $rooms->where(function ($query) use ($keyword) {
                $query->whereHas('Group', function ($query) use ($keyword) {
                    $query->where('Name', 'like', '%'.$keyword.'%');
                });
                $query->orWhereHas('ToUser', function ($query) use ($keyword) {
                    $query->where(DB::raw("CONCAT(FirstName, ' ', LastName)"), 'like', '%'.$keyword.'%');
                });
            });
        }

        $rooms = $rooms->get();

        $data = [];
        foreach ($rooms as $room) {
            $data[] = self::compileSingleRow($room, $user_id);
        }

        return $data;
    }

    public static function compileSingleRow($room, $user_id = 0) {
        $data = [
            'ID'                    => encode_id($room->ID),
            'IsGroupChat'           => $room->IsGroupChat === 'yes',
            'LastActivityDate'      => Carbon::parse($room->LastActivityDate)->diffForHumans(),
            'LastActivityMessage'   => $room->LastActivityMessage,
        ];

        if (!empty($room->GroupID)) {
            $group_admin_id = $room->Group->UserID;
            $data['Name'] = $room->Group->Name;
            $data['ProfileImage'] = $room->Group->GroupImage;
            $data['GroupInfo'] = [
                'ID'                => encode_id($room->GroupID),
                'Members'           => [],
                'IsCurrentAdmin'    => $group_admin_id == $user_id,
            ];

            $members = $room->Group->Members;
            foreach ($members as $member) {
                $data['GroupInfo']['Members'][] = [
                    'UserID'                        => $member->UserID,
                    'UserIDEncrypted'               => encode_id($member->UserID),
                    'FullName'                      => $member->User->full_name,
                    'ProfileImage'                  => $member->User->ProfileImage,
                    'IsAdmin'                       => $group_admin_id == $member->UserID
                ];
            }
        } else {
            if ($user_id == $room->UserID) {
                $data['Name'] = $room->ToUser->full_name;
                $data['ProfileImage'] = $room->ToUser->ProfileImage;
            } else {
                $data['Name'] = $room->FromUser->full_name;
                $data['ProfileImage'] = $room->FromUser->ProfileImage;
            }
        }

        return $data;
    }

    public function delete()
    {
        // remove chat mess + info
        $this->ChatInfo()->delete();
        $this->ChatMessages()->delete();
        return parent::delete(); // TODO: Change the autogenerated stub
    }
}