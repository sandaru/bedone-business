<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserNotification extends Model
{
    //
    protected $table = "UserNotification";
    protected $primaryKey = "ID";
    public $timestamps = false;

    // Company Relationship
    public function Company(){
        return $this->belongsTo('App\Models\Company', 'CompanyID');
    }

    // Poster of NewsID
    public function NewsPoster() {
        return $this->belongsTo('App\Models\User', 'UserNewsID');
    }

    public function Booking() {
        return $this->belongsTo('App\Models\Booking', 'BookingID');
    }

    /**
     * COunt all unread notification
     * @param $user_id
     * @return mixed
     */
    public static function UnreadNotification($user_id) {
        return UserNotification::where('UserID', $user_id)
                                ->where('IsSeen', 'no')
                                ->count();
    }

    /**
     * Get notification
     * @param $user_id
     * @param $offset
     * @param $limit
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public static function SearchNotification($user_id, $offset, $limit) {
        $noti = UserNotification::query()
                                ->where('UserID', $user_id)
                                ->orderBy('CreatedDate', 'DESC')
                                ->skip($offset)
                                ->limit($limit);

        return $noti->get();
    }

    public static function SeenAllNotification($user_id) {
        UserNotification::query()
            ->where('UserID', $user_id)
            ->update(['IsSeen' => 'yes']);
    }

    /**
     * Base - Send notification
     * @param UserNotification $noti
     * @param $type
     * @param $userId
     */
    private static function sendNotification(UserNotification $noti)
    {
        $noti->CreatedDate = now();
        $noti->IsSeen = 'no';
        $noti->save();
    }

    /**
     * Send reject company notification
     * @param $user_id
     */
    public static function sendRejectCompanyNotify($company_id, $user_id)
    {
        $noti = new UserNotification;
        $noti->UserID = $user_id;
        $noti->CompanyID = $company_id;
        $noti->Type = "RejectJoinOrg";
        self::sendNotification($noti);
    }

    /**
     * Send Approve company notification
     * @param $user_id
     */
    public static function sendApproveCompanyNotify($company_id, $user_id)
    {
        $noti = new UserNotification;
        $noti->UserID = $user_id;
        $noti->CompanyID = $company_id;
        $noti->Type = "AcceptJoinOrg";
        self::sendNotification($noti);
    }

    public static function sendBookingNotify($booking_id, $user_id) {
        $noti = new UserNotification;
        $noti->UserID = $user_id;
        $noti->BookingID = $booking_id;
        $noti->Type = "BookingAdded";
        self::sendNotification($noti);
    }

    public static function sendBookingAlert($booking_id, $user_id) {
        $noti = new UserNotification;
        $noti->UserID = $user_id;
        $noti->BookingID = $booking_id;
        $noti->Type = "BookingNotification";
        self::sendNotification($noti);
    }

    public static function sendBookingCancelNotify($booking_id, $user_id) {
        $noti = new UserNotification;
        $noti->UserID = $user_id;
        $noti->BookingID = $booking_id;
        $noti->Type = "BookingCancel";
        self::sendNotification($noti);
    }
}
