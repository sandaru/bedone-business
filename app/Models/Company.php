<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = "Company";
    protected $primaryKey = "ID";
    public $timestamps = false;

    protected $fillable = [
        'Name',
        'UserAdminID',
        'CreatedByID',
        'CreatedByName',
        'CreatedDate',
        'ModifiedByID',
        'ModifiedByName',
        'ModifiedDate',
    ];

    /***
     * Final admin of this company
     */
    public function UserAdmin()
    {
        return $this->hasOne('App\Models\User', 'ID', 'UserAdminID');
    }

    public function Users() {
        return $this->hasMany('App\Models\User', 'CompanyID');
    }


    public function TotalUserInCompany() {
        return $this->Users()->count();
    }
}
