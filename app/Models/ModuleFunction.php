<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleFunction extends Model
{
    protected $table = "ModuleFunction";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function Module()
    {
        return $this->belongsTo("App\Models\Module", "ModuleID");
    }

    /**
     * Get all functions access
     * @param $module_id
     */
    public static function getAllFunctionByModuleID($module_id)
    {
        return ModuleFunction::where('ModuleID', $module_id)->get();
    }

    /**
     * Get single function
     * @param $module_id
     * @param $function_name
     */
    public static function getFunction($module_id, $function_name)
    {
        return ModuleFunction::where('ModuleID', $module_id)
            ->where('Name', $function_name)
            ->first();
    }

    /**
     * Get function by module name and functio name
     * @param $module_name
     * @param $function_name
     */
    public static function getFunctionByModuleName($module_name, $function_name)
    {
        return ModuleFunction::with('Module')
                            ->whereHas('Module', function ($query) use ($module_name)
                            {
                                $query->where('Name', $module_name);
                            })
                            ->where('Name', $function_name)
                            ->first();
    }
}
