<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserResetPassword extends Model
{
    public static $snakeAttributes = false;
    protected $table = "UserResetPassword";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'UserID');
    }

    public function scopeByUserID($query, $id)
    {
        return $query->where('UserID', $id);
    }
}
