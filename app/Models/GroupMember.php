<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/26/2018
 * Time: 10:26 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    protected $table = "GroupMember";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function User() {
        return $this->belongsTo('App\Models\User', 'UserID');
    }

    public static function AddMember($user_id, $group_id) {
        $mem = new self;
        $mem->UserID = $user_id;
        $mem->GroupID = $group_id;
        $mem->save();
        return $mem;
    }

    public static function IsUserInGroup($user_id, $group_id) {
        $mem = self::query()
            ->where('UserID', $user_id)
            ->where('GroupID', $group_id)->count();

        return $mem > 0;
    }

    public static function RemoveUser($user_id, $group_id) {
        self::query()
                ->where('UserID', $user_id)
                ->where('GroupID', $group_id)
                ->delete();
    }
}