<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingUser extends Model
{
    public static $snakeAttributes = false;
    protected $table = "BookingUser";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function User() {
        return $this->belongsTo('App\Models\User', 'UserID');
    }

    public static function DeleteUser($user_id, $booking_id) {
        BookingUser::query()->where('UserID', $user_id)->where('BookingID', $booking_id)->delete();
    }
}
