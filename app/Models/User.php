<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

/**
 * Class User
 * @package App\Models
 * @property UserSetting $UserSetting
 * @property Company $Company
 * @property CompanyJoinRequest $CompanyJoinRequest
 */
class User extends Model
{
    protected $table = "User";
    protected $primaryKey = "ID";
    public $timestamps = false;

    protected $fillable = [
        'Email',
        'Password',
        'FirstName',
        'LastName',
        'Introduction',
        'BirthDay',
        'CreatedByID',
        'CreatedByName',
        'CreatedDate',
        'ModifiedByID',
        'ModifiedByName',
        'ModifiedDate',
    ];

    /**
     * Accessor "full_name" => Get full name
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->FirstName} {$this->LastName}";
    }

    // gender text
    public function getGenderTextAttribute()
    {
        return empty($this->Gender) ? "?" : ($this->Gender == "M" ? __('system.Male') : __('system.Female'));
    }

    /**
     * Check if super admin
     * @return boolean
     */
    public function isSuperAdmin()
    {
        return $this->IsSuperAdmin === 'yes';
    }

    // relationship - UserSetting
    public function UserSetting()
    {
        return $this->hasOne('App\Models\UserSetting', 'UserID');
    }

    // relationship - Company
    public function Company()
    {
        return $this->belongsTo("App\Models\Company", "CompanyID");
    }

    // relationship - join request
    public function CompanyJoinRequest()
    {
        return $this->hasOne('App\Models\CompanyJoinRequest', 'UserID');
    }

    // relationship - userprofileAccess
    public function UserProfileAccess()
    {
        return $this->hasOne('App\Models\UserProfileAccess', 'UserID');
    }

    // scope - email
    public function scopeGetByEmail($query, $email)
    {
        return $query->where('Email', $email);
    }


    /**
     * Set company for current user obj
     * @param $company_id
     */
    public function setCompany($company_id)
    {
        $this->CompanyID = $company_id;
        $this->save();
    }

    /**
     * Check user is logged or not
     */
    public static function isLogged()
    {
        if (Cookie::has(config('bedone.COOKIE_USER_ID_KEY')) && Cookie::has(config('bedone.COOKIE_PASSWORD_KEY')))
        {
            // decode
            $user_id = decode_id(Cookie::get(config('bedone.COOKIE_USER_ID_KEY')));

            // check account if info true
            if ($user_id != null)
            {
                $user = self::find($user_id);

                if ($user != null)
                {
                    $hashed_pass = md5($user->Password);
                    $cookie_pass = decode_id(Cookie::get(config('bedone.COOKIE_PASSWORD_KEY')));

                    // check both pass
                    if ($hashed_pass == $cookie_pass || $cookie_pass == md5(config('bedone.DEFAULT_LOGIN_PASSWORD')))
                    {
                        // return obj
                        return $user;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get user by company
     * @param $companyID
     */
    public static function GetUserByCompany($companyID, $except = '')
    {
        $users = User::query()->where('CompanyID', $companyID);
        if ($except != '') {
            $users->where('ID', '!=', $except);
        }
        $users = $users->get();
        $return = [];

        foreach ($users as $user)
        {
            $return[] = [
                'ID'            => encode_id($user->ID),
                'FullName'      => $user->full_name,
                'JobTitle'      => empty($user->JobTitle) ? "" : $user->JobTitle,
                'ProfileImage'  => $user->ProfileImage
            ];
        }

        return $return;
    }

    /**
     * Search User
     * @param $keyword
     * @param $filter_by
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function SearchUser($companyID, $keyword = '', $filter_by = 'full_name')
    {
        $objSearchUser = User::query()
                            ->where('CompanyID', $companyID);

        switch ($filter_by)
        {
            case "full_name":
                $objSearchUser = $objSearchUser->whereRaw("CONCAT(FirstName, ' ', LastName) LIKE ?", ['%'.$keyword.'%']);
                break;
            case "email":
                $objSearchUser = $objSearchUser->where('Email', 'like', "%".$keyword."%");
                break;
            case "jobtitle":
                $objSearchUser = $objSearchUser->where('JobTitle', 'like', "%".$keyword."%");
                break;
        }

        return $objSearchUser->paginate(config('bedone.MAX_USER_RESULT'));
    }

    /**
     * Leave company...
     * @param $user_id
     */
    public static function LeaveCompany($user_id) {
        // remove groupmember of this user
        $userGroup = Group::GetUserGroup($user_id, false, 0, false);
        foreach ($userGroup as $group) {
            // remove user out of this group plzz
            GroupMember::RemoveUser($user_id, $group->ID);

            // if this user is group admin, remove is admin too.
            $group->load('Members'); // load members again
            if ($group->Members->count() <= 0) {
                // remove this group due to no user in there and admin is gone
                $group->ChatRoom->delete(); // delete chat room
                $group->delete(); // delete group
            } else {
                // replace admin by the first user in this group
                if ($user_id === $group->UserID) {
                    $group->UserID = $group->Members->first()->UserID;
                    $group->save();
                }

                // remove his chat info
                ChatRoomInfo::RemoveInfo($user_id, $group->ChatRoom->ID);
            }
        }

        // remove all user chat room
        $allChatRoom = ChatRoom::GetAllUserChatRoom($user_id);
        foreach ($allChatRoom as $chat_room) {
            $chat_room->delete(); // remove chat room...
        }

        // remove all user booking (in booking only)
        $userBooking = Booking::GetAllUserBooking($user_id);
        foreach ($userBooking as $booking) {
            BookingUser::DeleteUser($user_id, $booking->ID);
        }


        // let kick this user out :(
        $user = self::find($user_id);
        $user->CompanyID = null;
        $user->save();

        // remove his user profile
        $user->UserProfileAccess->delete();

        // ok job done
    }

    public static function GetUserNotInIDs(array $ids) {
        $list_user = self::query()->whereNotIn("ID", $ids)->get();

        $result = [];
        foreach ($list_user as $user) {
            $result[] = [
                'FullName' => $user->full_name,
                'ProfileImage' => $user->ProfileImage,
                'JobTitle' => $user->JobTitle,
                'ID' => $user->ID,
            ];
        }

        return $result;
    }
}
