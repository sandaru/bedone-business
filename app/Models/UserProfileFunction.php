<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserProfileFunction extends Model
{
    protected $table = "UserProfileFunction";
    protected $primaryKey = "ID";
    public $timestamps = false;
    protected $fillable = [
        'ModuleFunctionID',
        'UserProfileModuleID',
        'Status'
    ];

    public function ModuleFunction()
    {
        return $this->belongsTo('App\Models\ModuleFunction', 'ModuleFunctionID');
    }

    /**
     * Check if user have permission to access this function
     * @param $module_id string|integer
     * @param $function_name
     * @param $user_id
     */
    public static function CanAccessFunction($module_id, $function_name, $user_profile_id)
    {
        // get all functions of this module name
        $function = null;
        if (is_numeric($module_id))
            $function = ModuleFunction::getFunction($module_id, $function_name);
        else
            $function = ModuleFunction::getFunctionByModuleName($module_id, $function_name);

        if ($function == null)
            return false;

        // ok get this module from user profile
        $upModule = UserProfileModule::GetByModuleID($function->ModuleID, $user_profile_id);

        if ($upModule == null)
            return false;

        // get function access
        $access_function = $upModule->Functions->where('ModuleFunctionID', $function->ID)->first();

        if ($access_function == null)
            return false;

        if ($access_function->Status == "off")
            return false;

        return true;
    }

    /**
     * Get all functions
     * @param $module_id
     * @param $user_profile_id
     */
    public static function GetAllFunctionPermission($module_id, $user_profile_id)
    {
        $list_access = [];

        // get list functions
        $functionIDs = ModuleFunction::getAllFunctionByModuleID($module_id);

        // ok get this module from user profile
        $upModule = UserProfileModule::GetByModuleID($module_id, $user_profile_id);
        if ($upModule == null)
            return $list_access;

        $access = UserProfileFunction::whereIn('ModuleFunctionID', $functionIDs->pluck('ID'))
                                        ->where('UserProfileModuleID', $upModule->ID)
                                        ->get();

        foreach ($access as $index => $item)
        {
            $list_access[$functionIDs[$index]->Name] = $item->Status == "on";
        }

        return $list_access;
    }

    /**
     * Create profile function
     * @param $up_module_id
     * @param $func_id
     * @param $status
     */
    public static function createProfileFunction($up_module_id, $func_id, $status)
    {
        // cast
        settype($status, 'boolean');

        // insert
        $up_func = new UserProfileFunction;
        $up_func->UserProfileModuleID = $up_module_id;
        $up_func->ModuleFunctionID = $func_id;
        $up_func->Status = $status === true ? "on" : "off";
        $up_func->save();

        return $up_func;
    }
}