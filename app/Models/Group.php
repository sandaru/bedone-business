<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/26/2018
 * Time: 10:26 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = "Group";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function Members() {
        return $this->hasMany('App\Models\GroupMember', 'GroupID');
    }

    public function ChatRoom() {
        return $this->hasOne('App\Models\ChatRoom', 'GroupID', 'ID');
    }

    public function AdminUser() {
        return $this->belongsTo('App\Models\User', 'UserID');
    }

    public static function GetUserGroup($user_id, $isMasterAdmin, $companyID, $encode = true) {
        $group = self::query()
                        ->with('Members');

        if ($isMasterAdmin) {
            $group->where('CompanyID', $companyID);
        } else {
            $group->whereHas('Members', function ($query) use ($user_id) {
                $query->where('UserID', $user_id);
            });
        }

        $group = $group->get();

        if ($encode) {
            $data = [];
            foreach ($group as $row) {
                $data[] = [
                    'ID' => encode_id($row->ID),
                    'Name' => $row->Name,
                    'GroupImage' => $row->GroupImage,
                    'TotalMembers' => $row->Members->count(),
                ];
            }

            return $data;
        } else {
            return $group;
        }
    }
}