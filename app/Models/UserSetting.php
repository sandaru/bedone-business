<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    //
    protected $table = "UserSetting";
    protected $primaryKey = "ID";
    public $timestamps = false;

    protected $fillable = [
        'UserID',
        'ModifiedByID',
        'ModifiedByName',
        'ModifiedDate',
    ];
}
