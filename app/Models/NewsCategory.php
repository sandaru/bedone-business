<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    public static $snakeAttributes = false;
    protected $table = "NewsCategory";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function getEncodeIDAttribute()
    {
        return encode_id($this->ID);
    }

    public function News()
    {
        return $this->hasMany('App\Models\News', 'NewsCategoryID');
    }

    public static function createCategory($name, $description, $icon, $userObj)
    {
        $item = new NewsCategory;
        $item->CompanyID = $userObj->CompanyID;
        $item->Name = $name;
        $item->Description = $description;
        $item->Icon = $icon;
        $item->CreatedByID = $item->ModifiedByID = $userObj->ID;
        $item->CreatedByName = $item->ModifiedByName = $userObj->full_name;
        $item->CreatedDate = $item->ModifiedDate = sql_date();
        $item->save();

        return $item;
    }

    public static function updateCategory($id, $name, $description, $icon, $userObj)
    {
        $item = NewsCategory::find($id);
        $item->Name = $name;
        $item->Description = $description;
        $item->Icon = $icon;
        $item->ModifiedByID = $userObj->ID;
        $item->ModifiedByName = $userObj->full_name;
        $item->ModifiedDate = sql_date();
        $item->save();

        return true;
    }

    /**
     * Get all by company
     * @param $company_id
     * @return Collection
     */
    public static function GetAllByCompany($company_id)
    {
        return NewsCategory::where('CompanyID', $company_id)->get();
    }
}
