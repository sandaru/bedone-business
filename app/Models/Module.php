<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public static $snakeAttributes = false;
    protected $table = "Module";
    protected $primaryKey = "ID";
    public $timestamps = false;

    // get children relationship
    public function Children()
    {
        return $this->hasMany('App\Models\Module', 'ParentID', 'ID');
    }

    public function Functions()
    {
        return $this->hasMany('App\Models\ModuleFunction', 'ModuleID');
    }

    /**
     * Get module function
     * @param $module_id
     */
    public static function getModuleFunction($module_id)
    {
        return ModuleFunction::where('ModuleID', $module_id)->get();
    }
}
