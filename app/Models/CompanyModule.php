<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyModule extends Model
{
    public static $snakeAttributes = false;
    protected $table = "CompanyModule";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function Module()
    {
        return $this->belongsTo('App\Models\Module', 'ModuleID');
    }

    /**
     * Default module of this company
     * @param $company_id
     * @param $userObj
     */
    public static function setDefaultModule($company_id, $userObj)
    {
        $default_modules = config('bedone.MODULE_ALLOWANCE');

        foreach ($default_modules as $module_id)
        {
            $company_module = new CompanyModule;
            $company_module->ModuleID = $module_id;
            $company_module->CompanyID = $company_id;
            $company_module->CreatedDate = $company_module->ModifiedDate = sql_date();
            $company_module->CreatedByID = $company_module->ModifiedByID = $userObj->ID;
            $company_module->CreatedByName = $company_module->ModifiedByName = $userObj->full_name;
            $company_module->save();
        }
    }

    /**
     * This one will take module parent only
     * @param $company_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getCompanyModule($company_id)
    {
        return CompanyModule::query()
            ->with(['Module', 'Module.Children', 'Module.Functions', 'Module.Children.Functions'])
            ->where('CompanyID', $company_id)
            ->whereHas('Module', function ($query) {
                $query->whereNull('ParentID');
            })
            ->get();
    }

    /**
     * This one will take all
     * @param $company_id
     */
    public static function getAllCompanyModule($company_id)
    {
        return CompanyModule::query()
            ->with(['Module', 'Module.Children', 'Module.Functions', 'Module.Children.Functions'])
            ->where('CompanyID', $company_id)
            ->get();
    }

    /**
     * Check if this company can access this module
     * @param $company_id
     * @param $module_id
     */
    public static function isCompanyModule($company_id, $module_id)
    {
        $count = CompanyModule::query()
                                ->where('CompanyID', $company_id)
                                ->where('ModuleID', $module_id)
                                ->count();

        return $count > 0;
    }
}
