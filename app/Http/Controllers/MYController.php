<?php

namespace App\Http\Controllers;

use App\Models\CompanyModule;
use App\Models\User;
use App\Models\UserNotification;
use App\Models\UserProfileModule;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class MYController extends Controller
{
    /*
     * Properties
     */

    /*
     * Permission
     */
    protected $permission_required = true;
    protected $logged_required = true;
    protected $has_sidebar = true;
    protected $has_notification = true;

    /**
     * Current module
     * @var string
     */
    protected $module_name = "";
    protected $module_id = "";

    /**
     * Logged user obj
     * @var User
     */
    protected $userObj = null;
    protected $userProfileID = null;

    /*
     * CompanyID
     */
    protected $companyID = null;

    /*
     * Data will pass to view
     */
    protected $data = [
        'JsInclude'     => [],
        'CssInclude'    => [],
        'ActionMenu'    => [],
        'Sidebar'       => [],
    ];

    /*
     * Necessary function
     */
    public function __construct()
    {
        // check login
        if ($this->logged_required == true)
        {
            $isLogged = User::isLogged();
            if ($isLogged === false)
            {
                Redirect::route('loginPage')->send();
            }

            // set obj
            $this->userObj = $isLogged;
            $this->companyID = $this->userObj->CompanyID;
        }

        // check permission
        $current_class = get_called_class();
        $this->module_name = str_replace("Controller", "", substr($current_class, strrpos($current_class, '\\')+1));
        if ($this->permission_required == true)
        {
            // if don't have any permission
            if ($this->userObj == null || $this->userObj->UserProfileAccess == null)
                Redirect::route('logoutAction')->send();

            // ok this one have permission, we check step by step now
            $this->permission_checking();
        }

        // generate sidebar
        if ($this->has_sidebar)
            $this->generate_sidebar();

        // construct here
        $this->defaultFrontEndFiles();

        // set user into this
        $this->data['userObj'] = $this->userObj;

        // global lang
        $this->globalLangInit();

        // notification
        if ($this->has_notification) {
            $this->userNotification();
        }
    }

    /**
     * Check permission
     */
    private function permission_checking()
    {
        $profileID = $this->userObj->UserProfileAccess->UserProfileID;
        $this->userProfileID = $profileID;
//        $profile_modules_on = $profile_access->UserProfile
//                                            ->UserProfileModule
//                                            ->where('Status', 'on');

        $isPermission = UserProfileModule::canAccessModule($this->module_name, $profileID);
//        foreach ($profile_modules_on as $module)
//        {
//            if ($module->Module->Name === $this->module_name)
//            {
//                $this->module_id = $module->Module->ID;
//
//                // if this company can access this module
//                if (CompanyModule::isCompanyModule($this->companyID, $this->module_id)) {
//                    $isPermission = true;
//                }
//                break;
//            }
//        }

        if ($isPermission === false)
            Redirect::route("logoutAction")->send();
        else
            $this->module_id = $isPermission;
    }

    /**
     *
     */
    private function generate_sidebar()
    {
        $profile_access = $this->userObj->UserProfileAccess;

        if ($profile_access == null)
            return;

        $module_on = UserProfileModule::filterByProfileID($profile_access->UserProfileID)
                                    ->filterStatusOn()
                                    ->join('Module', 'Module.ID', '=', 'UserProfileModule.ModuleID')
                                    ->orderBy('Module.Order', 'ASC')
                                    ->get();

        $sidebar = [];

        foreach ($module_on as $item)
        {
            $module = $item->Module;
            if ($module->ParentID != null)
                continue;

            // add parent
            $parent_info = [
                'Name' => $module->Name,
                'Icon' => $module->Icon,
                'Current' => $module->Name === $this->module_name,
                'RouteName' => $module->RouteName,
                'HasChild' => false,
                'HasOpenChild' => false,
                'Children' => []
            ];

            // add child
            foreach ($module->Children as $child)
            {
                $is_on = UserProfileModule::filterStatusOn()
                                            ->where('ModuleID', $child->ID)
                                            ->where('UserProfileID', $profile_access->UserProfileID)
                                            ->count();
                if ($is_on <= 0) {
                    continue;
                }

                $parent_info['HasChild'] = true;

                $parent_info['Children'][] = [
                    'Name' => $child->Name,
                    'Icon' => $child->Icon,
                    'RouteName' => $child->RouteName,
                    'Current' => $child->Name === $this->module_name
                ];

                if ($parent_info['HasOpenChild'] == false && $child->Name === $this->module_name)
                    $parent_info['HasOpenChild'] = true;
            }

            $sidebar[] = $parent_info;
        }

        $this->data['Sidebar'] = $sidebar;
    }

    /**
     * Add js for page
     * @param $file_path
     */
    protected function addJs($file_path)
    {
        $this->data['JsInclude'][] = $file_path;
    }

    /**
     * Add css for page
     * @param $file_path
     */
    protected function addCss($file_path)
    {
        $this->data['CssInclude'][] = $file_path;
    }

    /**
     * Default Js and Css File
     */
    private function defaultFrontEndFiles()
    {
        // CSS
        $this->addCss(asset('css/bootstrap.min.css'));
        $this->addCss(asset('css/select2.min.css'));
        $this->addCss(asset('css/jquery-ui.min.css'));
        $this->addCss(asset('css/jquery-ui.structure.min.css'));
        $this->addCss(asset('css/jquery-ui.theme.min.css'));
        $this->addCss(asset('css/fontawesome-all.min.css'));
        $this->addCss(asset('css/light-bootstrap-dashboard.css'));
        $this->addCss(asset('css/jquery-input-char-count-bootstrap3.min.css'));
        $this->addCss(asset('css/bootstrap-toggle.min.css'));

        // JS
        $this->addJs(asset('js/jquery.3.2.1.min.js'));
        $this->addJs(asset('js/vue.min.js'));
        $this->addJs(asset('js/jquery-ui.min.js'));
        $this->addJs(asset('js/moment.min.js'));
        $this->addJs(asset('js/underscore-min.js'));
        $this->addJs(asset('js/popper.min.js'));
        $this->addJs(asset('js/bootstrap.min.js'));
        $this->addJs(asset('js/bootstrap-toggle.min.js'));
        $this->addJs(asset('js/select2.min.js'));
        $this->addJs(asset('js/jquery-input-char-count.min.js'));
        $this->addJs(asset('js/bootstrap-datetimepicker.js'));
        $this->addJs(asset('js/light-bootstrap-dashboard.js'));
        $this->addJs(asset('js/jquery.noty.packaged.js'));
        $this->addJs(asset('js/sweetalert2.js'));
        $this->addJs(asset('js/BeDoneToaster.js'));
        $this->addJs(asset('js/BeDoneAjax.js'));
        $this->addJs(asset('js/BeDoneNotification.js'));
        $this->addJs(asset('js/helper.js'));
        $this->addJs(asset('js/error_mess_toaster.js'));
    }

    /**
     * Global lang
     */
    private function globalLangInit(){
        $lang = [];
        foreach (config('language.global_system') as $key) {
            $lang[$key] = __('system.'.$key);
        }

        $this->data['global_lang'] = json_encode($lang);
    }

    /**
     * User Notification
     */
    private function userNotification(){
        $this->data['Notification'] = [
            'Unread'        => UserNotification::UnreadNotification($this->userObj->ID),
            'Data'          => UserNotification::SearchNotification($this->userObj->ID, 0, config('bedone.MAX_RESULT'))
        ];
    }

    /**
     * Set page title
     * @param $title
     */
    protected function setPageTitle($title)
    {
        $this->data['page_title'] = $title;
    }

    /**
     * Set action menu action
     * @param array $menuInfo
     */
    protected function addActionMenu($title, $icon = "", $url = "", $action = "")
    {
        $this->data['ActionMenu'][] = [
            'Title'     => $title,
            'Icon'      => $icon,
            'URL'       => $url,
            'JsAction'  => $action,
        ];
    }

    /**
     * Fast validator running (POST and GET ONLY)
     * @param array $arrInfo Rules ([Field => Value])
     * @param Request $request
     * @return array
     */
    protected function runValidator($arrInfo, Request $request, $type = "POST")
    {
        $data = [];
        $returnArr = [
            'Valid'     => true,
        ];

        // retrieve data
        foreach ($arrInfo as $key => $value)
        {
            if ($type == "POST")
            {
                // array of files
                if (stristr($key,'.*')) {
                    continue;
                }
                else if (stristr($key, "_FILE")) {
                    $new_key = str_replace("_FILE", "", $key);
                    $data[$new_key] = $request->file($new_key);

                    // remove old value in arrInfo
                    $old = $arrInfo[$key];
                    unset($arrInfo[$key]);
                    $arrInfo[$new_key] = $old;
                }
                else {
                    $data[$key] = $request->post($key);
                }
            }
            else {
                $data[$key] = $request->get($key);
            }
        }

        // Valid now
        $valid = Validator::make($data, $arrInfo);

        if ($valid->fails())
        {
            $returnArr['Valid'] = false;
            $returnArr['Errors'] = $valid;
        }
        else
            $returnArr['Data'] = $data;


        return $returnArr;
    }

    /**
     * Return an invalid params
     */
    protected function API_Failed()
    {
        return response([
            'error' => __('system.invalid_params')
        ],422);
    }

    /**
     * Success data for API
     * @param $data
     */
    protected function API_Success($data)
    {
        return response(json_encode($data), 200);
    }
}
