<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 7/1/2018
 * Time: 12:56 AM
 */

namespace App\Http\Controllers\MyNetworkModule;


use App\Http\Controllers\MYController;
use App\Models\ChatMessage;
use App\Models\ChatRoomInfo;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class GroupController extends MYController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
        $this->setPageTitle(__('module.MODULE_Group'));
        $this->addJs(asset('js/modules/mynetwork/group.js'));
        return view('bedone.group.index', $this->data);
    }

    public function getUserGroup() {
        $isMasterAdmin = UserProfile::IsMasterAdmin($this->userProfileID);
        $result = Group::GetUserGroup($this->userObj->ID, $isMasterAdmin, $this->companyID);
        foreach ($result as &$group) {
            $group['GroupInfoURL'] = route('groupInfoPage', [$group['ID']]);
            $group['GroupImage'] = group_picture($group['GroupImage']);
        }

        return response()->json($result);
    }

    public function view($group_id) {
        $group_id = decode_id($group_id);
        $group = Group::find($group_id);
        if ($group == null) {
            return redirect()->route('groupPage')->with('error', __('system.not_found_group'));
        }

        // set data
        $this->data['group'] = $group;

        // get group member only
        $this->data['IsAdmin'] = $group->AdminUser->ID === $this->userObj->ID;
        $this->data['adminUser'] = $group->AdminUser;
        $this->data['groupMember'] = $group->Members->where('UserID', '!=', $group->UserID);
        $isMasterAdmin = UserProfile::IsMasterAdmin($this->userProfileID);
        $this->data['IsMasterAdmin'] = $isMasterAdmin;
        if ($this->data['IsAdmin'] || $isMasterAdmin) {
            $this->addActionMenu(__('system.EditGroup'), "fa-edit", null, "GroupViewAction.edit_group()");
            $this->addActionMenu(__('system.Addmorepeople'), "fa-users", null, "GroupViewAction.add_more_people()");
            $this->addActionMenu(__('system.DeleteGroup'), "fa-trash", null, "GroupViewAction.remove_group()");

            // load special data
            $groupMemberIDs = $group->Members->pluck('UserID');
            $this->data['user_option'] = $this->userDataProcess(User::GetUserNotInIDs($groupMemberIDs->toArray()));

            // in group can leave too lol
            if ($group->Members->where('UserID', $this->userObj->ID)->count() > 0) {
                $this->addActionMenu(__('system.LeaveGroup'), "fa-user-minus", null, "GroupViewAction.leave_group()");
            }
        } else {
            $this->addActionMenu(__('system.LeaveGroup'), "fa-user-minus", null, "GroupViewAction.leave_group()");
        }

        // ok stackup data
        $this->setPageTitle($group->Name);
        $this->addJs(asset('js/modules/mynetwork/group_view.js'));

        return view('bedone.group.view', $this->data);
    }

    public function removeMember($group_id, $user_id) {
        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return redirect()->route('groupPage')->with('error', __('system.not_found_group'));
        }

        // admin check
        if ($group->UserID !== $this->userObj->ID) {
            return redirect()->route('groupInfoPage', [$group_id])->with('error', __('system.permission_denied'));
        }

        $user = User::find(decode_id($user_id));
        if ($user == null) {
            return redirect()->route('groupInfoPage', [$group_id])->with('error', __('system.not_found_user'));
        }

        if (!GroupMember::IsUserInGroup($user->ID, $group->ID)) {
            return redirect()->route('groupInfoPage', [$group_id])->with('error', __('system.user_not_in_group'));
        }

        // ok remove user
        GroupMember::RemoveUser($user->ID, $group->ID);
        return redirect()->route('groupInfoPage', [$group_id])->with('success', __('system.action_success'));
    }

    public function edit($group_id, Request $rq) {
        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return redirect()->route('groupPage')->with('error', __('system.not_found_group'));
        }

        // admin check
        $isMasterAdmin = UserProfile::IsMasterAdmin($this->userProfileID);
        if ($group->UserID !== $this->userObj->ID && !$isMasterAdmin) {
            return redirect()->route('groupInfoPage', [$group_id])->with('error', __('system.permission_denied'));
        }

        // ok edit
        $rules = [
            'Name' => 'required|max:50',
            'Description' => 'required',
        ];

        if ($rq->has('GroupImage')) {
            $rules['GroupImage_FILE'] = "required|image|max:512";
        }
        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return redirect()->route('groupInfoPage', [$group_id])->withErrors($dataValid['Errors']);
        }

        // ok edit
        $group->Name = $dataValid['Data']['Name'];
        $group->Description = strip_tags($dataValid['Data']['Description']);
        if (isset($dataValid['Data']['GroupImage']) && $dataValid['Data']['GroupImage']->isValid()) {
            // process upload
            $path = public_path(config('bedone.GROUP_IMAGE_PATH'));
            $newName = time() . "_group_" . rand(0,999999) . $this->companyID . "." . $dataValid['Data']['GroupImage']->getClientOriginalExtension();
            $full_path = config('bedone.GROUP_IMAGE_PATH') . "/" . $newName;

            // move file
            $dataValid['Data']['GroupImage']->move($path, $newName);
            $group->GroupImage = $full_path;
        }
        $group->save();

        return redirect()->route('groupInfoPage', [$group_id])->with('success', __('system.action_success'));
    }

    public function leave_group($group_id) {
        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return redirect()->route('groupPage')->with('error', __('system.not_found_group'));
        }

        if (!GroupMember::IsUserInGroup($this->userObj->ID, $group->ID)) {
            return redirect()->route('groupPage')->with('error', __('system.you_not_in_group'));
        }

        GroupMember::RemoveUser($this->userObj->ID, $group->ID);

        // if this user is group admin, remove is admin too.
        $group->load('Members'); // load members again
        if ($group->Members->count() <= 0) {
            // remove this group due to no user in there and the last one is gone
            $group->ChatRoom->delete(); // delete chat room
            $group->delete(); // delete group
        } else {
            // replace admin by the first user in this group
            if ($this->userObj->ID === $group->UserID) {
                $group->UserID = $group->Members->first()->UserID;
                $group->save();
            }

            // remove his chat info
            ChatRoomInfo::RemoveInfo($this->userObj->ID, $group->ChatRoom->ID);
        }

        return redirect()->route('groupPage')->with('success', __('system.action_success'));
    }

    public function add_more_people($group_id, Request $rq) {
        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return redirect()->route('groupPage')->with('error', __('system.not_found_group'));
        }

        // admin check
        $isMasterAdmin = UserProfile::IsMasterAdmin($this->userProfileID);
        if ($group->UserID !== $this->userObj->ID || !$isMasterAdmin) {
            return redirect()->route('groupInfoPage', [$group_id])->with('error', __('system.permission_denied'));
        }

        // ok edit
        $rules = [
            'user_ids' => 'required|array|min:1',
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return redirect()->route('groupInfoPage', [$group_id])->withErrors($dataValid['Errors']);
        }

        foreach ($dataValid['Data']['user_ids'] as $user_id) {
            $user = User::find(decode_id($user_id));
            if ($user == null) {
                continue;
            }

            // check is in group
            if (GroupMember::IsUserInGroup($user->ID, $group->ID)) {
                continue;
            }

            // ok add into group
            $member = GroupMember::AddMember($user->ID, $group->ID);
            ChatRoomInfo::CreateChatInfo($user->ID, $group->ChatRoom->ID);
            ChatMessage::SendSystemMessage($group->ChatRoom->ID, ChatMessage::ADD_MEMBER, $this->userObj, $user);
        }

        return redirect()->route('groupInfoPage', [$group_id])->with('success', __('system.action_success'));
    }

    public function delete_group($group_id) {
        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return redirect()->route('groupPage')->with('error', __('system.not_found_group'));
        }

        // admin check
        $isMasterAdmin = UserProfile::IsMasterAdmin($this->userProfileID);
        if ($group->UserID !== $this->userObj->ID || !$isMasterAdmin) {
            return redirect()->route('groupInfoPage', [$group_id])->with('error', __('system.permission_denied'));
        }

        // remove this group
        $group->ChatRoom->delete(); // delete chat room
        foreach ($group->Members as $mem) {
            $mem->delete();
        }
        $group->delete(); // delete group

        return redirect()->route('groupPage')->with('success', __('system.action_success'));
    }

    private function userDataProcess($users) {
        $data = [];
        foreach ($users as $user) {
            $data[] = [
                'id'        => encode_id($user['ID']),
                'text'      => $user['FullName'],
                'html'      => View::make('bedone.partial.user_media', ['user' => $user])->render(),
            ];
        }

        return $data;
    }
}