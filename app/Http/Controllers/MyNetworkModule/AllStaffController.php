<?php

namespace App\Http\Controllers\MyNetworkModule;

use App\Http\Controllers\MYController;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class AllStaffController extends MYController
{
    private $userRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo)
    {
        parent::__construct();
        if ($this->companyID == null)
            Redirect::route('userProfilePage')->with('error', __('system.permission_denied'))->send();

        $this->userRepo = $userRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->setPageTitle(__('module.AllStaff'));
        $this->addJs(asset('js/modules/mynetwork/allstaff.js'));
        return view('bedone.allstaff.index', $this->data);
    }

    /**
     * Get all staff in the same company API
     */
    public function getAllStaff()
    {
        return response()->json(User::GetUserByCompany($this->companyID));
    }
}
