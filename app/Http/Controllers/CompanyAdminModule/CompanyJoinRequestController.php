<?php

namespace App\Http\Controllers\CompanyAdminModule;

use App\Http\Controllers\MYController;
use App\Models\CompanyJoinRequest;
use App\Models\UserNotification;
use App\Models\UserProfile;
use App\Models\UserProfileAccess;
use App\Models\UserProfileFunction;
use Illuminate\Http\Request;

class CompanyJoinRequestController extends MYController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $rq)
    {
        $filter_by = $rq->get('filter_by','full_name');
        $keyword = $rq->get('keyword','');

        // get function access
        $this->data['functionAccess'] = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userProfileID);

        // get user list
        $this->data['listUser'] = CompanyJoinRequest::GetJoinList($this->userObj->CompanyID, $keyword, $filter_by);

        // set default data
        $this->setPageTitle(__('module.CompanyJoinRequest'));

        // add js
        $this->addJs(asset('js/modules/company_admin/search_join.js'));

        return view('bedone.company_admin.search_join', $this->data);
    }

    public function approvePage($id)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Approve', $this->userProfileID))
            return redirect()->back()->with('error', __('system.permission_denied'));

        $id = decode_id($id);
        $join = CompanyJoinRequest::find($id);
        if ($join == null)
            return redirect()->back()->with('error', __('system.not_found_information'));

        $isProfileMasterAdmin = UserProfile::IsMasterAdmin($this->userProfileID);
        $this->data['ListUserProfile'] = UserProfile::GetProfileByCompany($this->companyID, $isProfileMasterAdmin)->pluck('Name', 'ID');
        $this->data['JoinRequest'] = $join;

        // set title
        $this->setPageTitle(__('system.ApproveUser'));

        // set js
        $this->addJs(asset('js/modules/company_admin/approve.js'));


        return view('bedone.company_admin.approve_join', $this->data);
    }

    public function Approving($id, Request $rq)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Approve', $this->userProfileID))
            return redirect()->back()->with('error', __('system.permission_denied'));

        $id = decode_id($id);
        $join = CompanyJoinRequest::find($id);
        if ($join == null)
            return redirect()->back()->with('error', __('system.not_found_information'));

        // validation
        $rules = [
            'UserProfileID'     => 'required'
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] === false) {
            return redirect()->back()->withErrors($dataValid['Errors']);
        }

        $userProfileID = decode_id($dataValid['Data']['UserProfileID']);
        if (!UserProfile::isProfileInCompany($userProfileID, $join->CompanyID)) {
            return redirect()->back()->with("error", __('system.user_profile_not_found'));
        }

        // ok set user to this company
        $user = $join->User;
        $user->CompanyID = $join->CompanyID;
        setObjLoggedInfo($user, $this->userObj);
        $user->save();

        // set user profile
        UserProfileAccess::setUserProfile($user->ID, $userProfileID);

        // send notification
        UserNotification::sendApproveCompanyNotify($join->CompanyID, $user->ID);

        // then remove this request
        $join->delete();

        return redirect()->route('companyJoinRequestPage')->with('success', __('system.action_success'));
    }

    public function DeclineRequest($id)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Decline', $this->userProfileID))
            return redirect()->back()->with('error', __('system.permission_denied'));

        $id = decode_id($id);
        $join = CompanyJoinRequest::find($id);
        if ($join == null)
            return redirect()->back()->with('error', __('system.not_found_information'));

        // send notification for user
        UserNotification::sendRejectCompanyNotify($join->CompanyID, $join->UserID);

        // delete request
        $join->delete();

        return redirect()->back()->with('success', __('system.action_success'));
    }
}
