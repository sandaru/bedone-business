<?php

namespace App\Http\Controllers\CompanyAdminModule;

use App\Http\Controllers\MYController;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserProfileFunction;
use Illuminate\Http\Request;

class UserMaintainanceController extends MYController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $rq)
    {
        $filter_by = $rq->get('filter_by','full_name');
        $keyword = $rq->get('keyword','');

        // get function access
        $this->data['functionAccess'] = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userProfileID);

        // get user list
        $this->data['listUser'] = User::SearchUser($this->userObj->CompanyID, $keyword, $filter_by);

        // set default data
        $this->setPageTitle(__('module.UserMaintenance'));

        // add js
        $this->addJs(asset('js/modules/company_admin/search_user.js'));


        return view('bedone.company_admin.search_user', $this->data);
    }

    public function edit($id)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Edit', $this->userProfileID))
            return redirect()->route('userMaintenancePage')->with('error', __('system.permission_denied'));

        // check now
        $id = decode_id($id);
        $user = User::find($id);
        if ($user == null)
            return redirect()->route('userMaintenancePage')->with('error', __('system.not_found_information'));

        // is Super Admin (Normal user can't edit super admin)
        $isSuperAdmin = false;
        if ($user->Company->UserAdminID === $user->ID) {
            $isSuperAdmin = true;
        }
        if ($isSuperAdmin || $user->ID === $this->userObj->ID) {
            return redirect()->route('userMaintenancePage')->with('error', __('system.permission_denied'));
        }

        // get function access
        $this->data['functionAccess'] = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userProfileID);
        $this->data['user'] = $user;
        $isProfileMasterAdmin = UserProfile::IsMasterAdmin($this->userProfileID);
        $this->data['ListUserProfile'] = UserProfile::GetProfileByCompany($this->companyID, $isProfileMasterAdmin)->pluck('Name', 'ID');

        $this->setPageTitle(__("system.EditUserInfo"));
        $this->addJs(asset('js/modules/company_admin/edit_user.js'));

        return view('bedone.company_admin.edit_user', $this->data);
    }

    public function editing($id, Request $rq)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Edit', $this->userProfileID))
            return redirect()->route('userMaintenancePage')->with('error', __('system.permission_denied'));

        // check now
        $id = decode_id($id);
        $user = User::find($id);
        if ($user == null)
            return redirect()->route('userMaintenancePage')->with('error', __('system.not_found_information'));

        // is Super Admin (Normal user can't edit super admin)
        $isSuperAdmin = false;
        if ($user->Company->UserAdminID === $user->ID) {
            $isSuperAdmin = true;
        }
        if ($isSuperAdmin || $user->ID === $this->userObj->ID) {
            return redirect()->route('userMaintenancePage')->with('error', __('system.permission_denied'));
        }

        // retrieve data with rules
        $rules = [
            'FirstName'     => 'required|max:50',
            'LastName'      => 'required|max:50',
            'JobTitle'      => '',
            'WorkPhone'     => '',
            'WorkAddress'   => '',
        ];

        if ($rq->has('ProfileImage')) {
            $rules['ProfileImage_FILE'] = 'required|image|max:500';
        }
        if ($rq->has('FeatureImage')) {
            $rules['FeatureImage_FILE'] = 'required|image|max:1024';
        }
        if ($rq->has('UserProfileID')) {
            if (!UserProfileFunction::CanAccessFunction($this->module_id, 'EditUserProfile', $this->userProfileID))
                return redirect()->back()->with('error', __('system.permission_denied'));

            $rules['UserProfileID'] = "required";
        }

        // ok validator
        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return redirect()->back()->withInput()->withErrors($dataValid['Errors']);
        }

        // ok process update
        $i = 0;
        foreach($dataValid['Data'] as $key => $value) {
            $user->$key = $value;
            $i++;
            if ($i == 5)
                break;
        }

        // solving upload - profile image
        if (isset($dataValid['Data']['ProfileImage']) && $dataValid['Data']['ProfileImage']->isValid())
        {
            $path = public_path(config('bedone.PROFILE_IMAGE_PATH'));
            $newName = time() . "_profile_" . $user->ID . "." . $dataValid['Data']['ProfileImage']->getClientOriginalExtension();
            $full_path = config('bedone.PROFILE_IMAGE_PATH') . "/" . $newName;

            // move file
            $dataValid['Data']['ProfileImage']->move($path, $newName);
            $user->ProfileImage = $full_path;
        }

        // solving upload - feature image
        if (isset($dataValid['Data']['FeatureImage']) && $dataValid['Data']['FeatureImage']->isValid())
        {
            $path = public_path(config('bedone.COVER_IMAGE_PATH'));
            $newName = time() . "_cover_" . $user->ID . "." . $dataValid['Data']['FeatureImage']->getClientOriginalExtension();
            $full_path = config('bedone.COVER_IMAGE_PATH') . "/" . $newName;

            // move file
            $dataValid['Data']['FeatureImage']->move($path, $newName);
            $user->FeatureImage = $full_path;
        }

        // save changes
        $user->save();

        // update user profile
        if ($rq->has('UserProfileID')) {
            $profile_id = decode_id($dataValid['Data']['UserProfileID']);
            $userProfile = UserProfile::find($profile_id);
            if ($profile_id != null) {
                $user->UserProfileAccess->UserProfileID = $userProfile->ID;
                $user->UserProfileAccess->save();
            }
        }

        return redirect()->route('userMaintenancePage')->with('success', __('system.saved_changes'));
    }

    public function removeUser($id)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'RemoveUser', $this->userProfileID))
            return redirect()->route('userMaintenancePage')->with('error', __('system.permission_denied'));

        // check now
        $id = decode_id($id);
        $user = User::find($id);

        if ($user == null)
            return redirect()->back()->with('error', __('system.not_found_information'));

        // kick user...
        User::LeaveCompany($user->ID);

        // done
        return redirect()->back()->with('success', __('system.action_success'));
    }
}
