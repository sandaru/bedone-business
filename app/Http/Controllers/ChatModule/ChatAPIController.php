<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/26/2018
 * Time: 9:12 PM
 */

namespace App\Http\Controllers\ChatModule;


use App\Http\Controllers\MYController;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\ChatRoomInfo;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\User;
use Illuminate\Http\Request;

class ChatAPIController extends MYController
{
    protected $permission_required = false;

    public function __construct()
    {
        parent::__construct();
    }

    public function getChatRoom(Request $rq) {
        $keyword = $rq->get('keyword', '');
        $start = $rq->get('start', 0);
        $limit = $rq->get('limit', config('bedone.MAX_RESULT'));
        return response()->json(ChatRoom::GetChatRoom($this->userObj->ID, $keyword, $start, $limit));
    }

    public function chatOneOnOne($to_user_id) {
        $to_user_id = decode_id($to_user_id);
        $to_user = User::find($to_user_id);
        if ($to_user === null) {
            return response()->json(['error' => __('system.not_found_user')]);
        }

        // ok get chat room
        $chat_room = ChatRoom::GetUserChatRoom($this->userObj->ID, $to_user->ID);
        if ($chat_room == null) {
            // create one chat room for both of this user
            $chat_room = new ChatRoom;
            $chat_room->UserID = $this->userObj->ID;
            $chat_room->ToUserID = $to_user->ID;
            $chat_room->LastActivityDate = now();
            $chat_room->save();
            $chat_room->load('ToUser');

            // create chat info
            ChatRoomInfo::CreateChatInfo($chat_room->UserID, $chat_room->ID);
            ChatRoomInfo::CreateChatInfo($chat_room->ToUserID, $chat_room->ID);
        } else {
            // set chat room not archive anymore
            ChatRoomInfo::SetUnArchive($this->userObj->ID, $chat_room->ID);
        }

        // return chat room
        $room = ChatRoom::compileSingleRow($chat_room, $this->userObj->ID);
        return response()->json(['data' => $room]);
    }

    public function chatGroup($group_id) {
        $group_id = decode_id($group_id);
        $group = Group::find($group_id);
        if ($group == null) {
            return response()->json(['error' => __('system.not_found_group')]);
        }

        // ok group existed, that mean this group have chat room already
        $group_chat = ChatRoom::GetGroupChatRoom($group->ID);
        if ($group_chat == null) {
            return response()->json(['error' => __('system.not_found_group')]);
        }
        ChatRoomInfo::SetUnArchive($this->userObj->ID, $group_chat->ID);

        $room = ChatRoom::compileSingleRow($group_chat, $this->userObj->ID);
        return response()->json(['data' => $room]);
    }

    public function getMessage($room_id, Request $rq) {
        $room_id = decode_id($room_id);
        $room = ChatRoom::find($room_id);
        if ($room == null) {
            return json_encode(['error' => __('system.no_chat_room')]);
        }

        // get data
        $start = $rq->get('start', 0);
        $limit = $rq->get('limit', config('bedone.MAX_RESULT'));

        return response()->json(['data' => ChatMessage::GetRoomMessage($room->ID, $this->userObj->ID, $start, $limit)]);
    }

    public function getNewMessage($room_id, Request $rq) {
        $room_id = decode_id($room_id);
        $room = ChatRoom::find($room_id);
        if ($room == null) {
            return json_encode(['error' => __('system.no_chat_room')]);
        }

        session_write_close();
        $time = 0;
        $time_limit = config('bedone.POILLING_LIMIT_TIME');
        $last_ajax_call = $rq->get('CreatedDate');
        if (empty($last_ajax_call)) {
            $last_ajax_call = sql_date();
        }

        // we did't care about time limit thought :(
        set_time_limit($time_limit + 70); // add more 5 second for good...

        $has_new_mess = ChatMessage::GetMessAfter($room->ID, $last_ajax_call, $this->userObj->ID);
        while ($has_new_mess === false && $time < $time_limit) {
            sleep(1);
            $time++;
            $has_new_mess = ChatMessage::GetMessAfter($room->ID, $last_ajax_call, $this->userObj->ID);
        }

        if ($time >= $time_limit) {
            return json_encode(['time' => $last_ajax_call]);
        } else {
            return json_encode(['data' => $has_new_mess]);
        }
    }

    public function sendTextMessage($room_id, Request $rq) {
        $room_id = decode_id($room_id);
        $room = ChatRoom::find($room_id);
        if ($room == null) {
            return json_encode(['error' => __('system.no_chat_room')]);
        }

        // ok post new mess
        $mess = ChatMessage::SendTextMessage($room->ID, $this->userObj, $rq->post('Message'));
        return response()->json(['data' => $mess]);
    }

    public function sendImageMessage($room_id, Request $rq) {
        $room_id = decode_id($room_id);
        $room = ChatRoom::find($room_id);
        if ($room == null) {
            return json_encode(['error' => __('system.no_chat_room')]);
        }

        $rules = [
            'Image_FILE' => 'required|image|max:1024',
        ];
        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // ok upload image
        if ($dataValid['Data']['Image']->isValid()) {
            $path = public_path(config('bedone.UTIL_PATH'));
            $newName = time() . "_chatimage_" . rand(0,999999) . $this->companyID."_" . $this->userObj->ID . "." . $dataValid['Data']['Image']->getClientOriginalExtension();
            $full_path = config('bedone.UTIL_PATH') . "/" . $newName;

            // move file
            $dataValid['Data']['Image']->move($path, $newName);

            // ok post new mess
            $mess = ChatMessage::SendImageMessage($room->ID, $this->userObj, $full_path);
            return response()->json(['data' => $mess]);
        } else {
            return response()->json(['error' => __('system.upload_failed')]);
        }
    }

    /*
     * Down here mostly group/util functional
     */

    public function newGroup(Request $rq) {
        $rules = [
            'Name'          => 'required|max:50',
            'Description'   => 'required',
            'Members'       => 'required|array|min:1',
        ];

        if ($rq->has('GroupImage')) {
            $rules['GroupImage_FILE'] = 'required|image|max:512';
        }

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // ok new group
        $group = new Group;
        $group->CompanyID = $this->companyID;
        $group->Name = $dataValid['Data']['Name'];
        $group->Description = $dataValid['Data']['Description'];
        $group->UserID = $this->userObj->ID;
        setObjLoggedInfo($group, $this->userObj, true);

        // upload image
        if (isset($dataValid['Data']['GroupImage']) && $dataValid['Data']['GroupImage']->isValid()) {
            $path = public_path(config('bedone.GROUP_IMAGE_PATH'));
            $newName = time() . "_group_" . rand(0,999999) . $this->companyID . "." . $dataValid['Data']['GroupImage']->getClientOriginalExtension();
            $full_path = config('bedone.GROUP_IMAGE_PATH') . "/" . $newName;

            // move file
            $dataValid['Data']['GroupImage']->move($path, $newName);
            $group->GroupImage = $full_path;
        }

        if ($group->save() == false) {
            return response()->json(['error' => __('system.action_failed')]);
        }

        // ok insert chat room
        $chat_room = new ChatRoom;
        $chat_room->GroupID = $group->ID;
        $chat_room->IsGroupChat = 'yes';
        $chat_room->LastActivityDate = now();
        $chat_room->save();
        $chat_room->load('Group');

        // ok insert user into this group
        $dataValid['Data']['Members'][] = encode_id($this->userObj->ID);
        foreach($dataValid['Data']['Members'] as $user_id) {
            $user = User::find(decode_id($user_id));
            if ($user === null) {
                continue;
            }

            GroupMember::AddMember($user->ID, $group->ID);
            ChatRoomInfo::CreateChatInfo($user->ID, $chat_room->ID);
        }

        $room = ChatRoom::compileSingleRow($chat_room, $this->userObj->ID);
        return response()->json(['data' => $room]);
    }

    public function archiveRoom($room_id) {
        $room_id = decode_id($room_id);
        $room = ChatRoom::find($room_id);
        if ($room == null) {
            return response()->json(['error' => __('system.not_found_information')]);
        }

        // ok room available, archive now
        ChatRoomInfo::SetArchive($this->userObj->ID, $room->ID);
        return response()->json(['success' => __('system.action_success')]);
    }

    public function leaveGroup(Request $rq) {
        $group_id = $rq->post('GroupID', '');
        $group = Group::find(decode_id($group_id));
        if ($group === null) {
            return response()->json(['error' => __('system.not_found_group')]);
        }

        // ok kick logged user out of this group
        GroupMember::RemoveUser($this->userObj->ID, $group->ID);
        ChatRoomInfo::RemoveInfo($this->userObj->ID, $group->ChatRoom->ID);
        ChatMessage::SendSystemMessage($group->ChatRoom->ID, ChatMessage::LEAVE_GROUP, $this->userObj, null);
        return response()->json(['success' => __('system.action_success')]);
    }

    public function addGroupMember($group_id, Request $rq) {
        $rules = [
            'users' => 'required|array|min:1',
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return response()->json(['error' => __('system.not_found_group')]);
        }
        if ($group->UserID !== $this->userObj->ID) {
            return response()->json(['error' => __('system.permission_denied')]);
        }

        // add users now
        $added = [];
        foreach ($dataValid['Data']['users'] as $user_id) {
            $user_id = decode_id($user_id);
            $user = User::find($user_id);
            if ($user == null) {
                continue;
            }

            if (!GroupMember::IsUserInGroup($user->ID, $group->ID)) {
                $member = GroupMember::AddMember($user->ID, $group->ID);
                ChatRoomInfo::CreateChatInfo($user->ID, $group->ChatRoom->ID);
                $member->load('User');
                $added[] = [
                    'UserID'            => $member->UserID,
                    'UserIDEncrypted'            => encode_id($member->UserID),
                    'FullName'      => $member->User->full_name,
                    'ProfileImage'  => $member->User->ProfileImage,
                    'IsAdmin'       => false
                ];

                ChatMessage::SendSystemMessage($group->ChatRoom->ID, ChatMessage::ADD_MEMBER, $this->userObj, $user);
            }
        }

        return response()->json(['success' => __('system.action_success'), 'data' => $added]);
    }

    public function updateGroupImage($group_id, Request $rq) {
        $rules = [
            'GroupImage_FILE' => 'required|image|max:512',
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return response()->json(['error' => __('system.not_found_group')]);
        }
        if ($group->UserID !== $this->userObj->ID) {
            return response()->json(['error' => __('system.permission_denied')]);
        }
        if (!$dataValid['Data']['GroupImage']->isValid()) {
            return response()->json(['error' => __('system.upload_failed')]);
        }

        // process upload
        $path = public_path(config('bedone.GROUP_IMAGE_PATH'));
        $newName = time() . "_group_" . rand(0,999999) . $this->companyID . "." . $dataValid['Data']['GroupImage']->getClientOriginalExtension();
        $full_path = config('bedone.GROUP_IMAGE_PATH') . "/" . $newName;

        // move file
        $dataValid['Data']['GroupImage']->move($path, $newName);
        $group->GroupImage = $full_path;
        setObjLoggedInfo($group, $this->userObj, true);
        $group->save();

        return response()->json(['success' => __('system.action_success'), 'data' => $full_path]);
    }

    public function updateGroupName($group_id, Request $rq) {
        $rules = [
            'Name' => 'required|min:1|max:50',
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return response()->json(['error' => __('system.not_found_group')]);
        }
        if ($group->UserID !== $this->userObj->ID) {
            return response()->json(['error' => __('system.permission_denied')]);
        }

        // change now
        $group->Name = $dataValid['Data']['Name'];
        setObjLoggedInfo($group, $this->userObj, true);
        $group->save();

        return response()->json(['success' => __('system.action_success'), 'data' => $group->Name]);
    }

    public function removeMember($group_id, Request $rq) {
        $group = Group::find(decode_id($group_id));
        if ($group == null) {
            return response()->json(['error' => __('system.not_found_group')]);
        }
        if ($group->UserID !== $this->userObj->ID) {
            return response()->json(['error' => __('system.permission_denied')]);
        }

        $rules = [
            'UserID' => 'required',
        ];
        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // remove now
        $user_id = $dataValid['Data']['UserID'];
        $user = User::find($user_id);
        if ($user == null) {
            return response()->json(['error' => __('system.not_found_user')]);
        }

        // ok check if user in this group
        if (!GroupMember::IsUserInGroup($user->ID, $group->ID)) {
            return response()->json(['error' => __('system.user_not_in_group')]);
        }

        // ok remove
        GroupMember::RemoveUser($user->ID, $group->ID);
        ChatMessage::SendSystemMessage($group->ChatRoom->ID, ChatMessage::DEL_MEMBER, $this->userObj, $user);
        return response()->json(['success' => __('system.action_success')]);
    }
}