<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/24/2018
 * Time: 12:10 PM
 */

namespace App\Http\Controllers\ChatModule;


use App\Http\Controllers\MYController;
use App\Models\Group;
use App\Models\User;
use Illuminate\Support\Facades\View;

class ChatController extends MYController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {

        // needed data
        $this->data['users'] = $this->userDataProcess(User::GetUserByCompany($this->companyID, $this->userObj->ID));
        $this->data['groups'] = $this->groupDataProcess(Group::GetUserGroup($this->userObj->ID, false, 0));

        // page info
        $this->setPageTitle(__('system.Chat'));
        $this->addCss(asset('css/featherlight.min.css'));
        $this->addCss(asset('css/modules/chat.css'));
        $this->addJs(asset('js/featherlight.min.js'));
        $this->addJs(asset('js/app.js'));
        return view('bedone.chat.index', $this->data);
    }

    private function userDataProcess($users) {
        $data = [];
        foreach ($users as $user) {
            $data[] = [
                'id'        => $user['ID'],
                'text'      => $user['FullName'],
                'html'      => View::make('bedone.partial.user_media', ['user' => $user])->render(),
            ];
        }

        return $data;
    }

    private function groupDataProcess($groups) {
        $data = [];
        foreach ($groups as $group) {
            $data[] = [
                'id'        => $group['ID'],
                'text'      => $group['Name'],
                'html'      => View::make('bedone.partial.group_media', ['group' => $group])->render(),
            ];
        }

        return $data;
    }
}