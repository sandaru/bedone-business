<?php

namespace App\Http\Controllers\BookingModule;

use App\Http\Controllers\MYController;
use App\Jobs\NotifyBookingJob;
use App\Models\Booking;
use App\Models\BookingResource;
use App\Models\BookingUser;
use App\Models\User;
use App\Models\UserNotification;
use App\Models\UserProfileFunction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BookingApiController extends MYController
{
    protected $has_notification = false;
    protected $has_sidebar = false;
    protected $permission_required = false;

    public function __construct()
    {
        parent::__construct();

        if (request()->ajax() === false || $this->companyID === null) {
            redirect()->to('/')->send();
        }
    }

    public function getMonthBooking(Request $rq) {
        // get all booking in this month
        $start = $rq->get('start', sql_date());
        $end = $rq->get('end', sql_date());

        // check permission that this user can access private
        $canAccessPrivate = UserProfileFunction::CanAccessFunction("Booking", "ViewPrivate", $this->userObj->UserProfileAccess->UserProfileID);

        // get data
        $result = Booking::SearchMonthBooking($this->companyID, $this->userObj->ID, $canAccessPrivate, $start, $end);

        // back data
        return response()->json($result);
    }

    public function getBooking($id) {
        $id = decode_id($id);
        $item = Booking::GetByID($id);
        if ($item == null) {
            return response()->json(['error' => __('system.not_found_information')]);
        }

        return response()->json(['data' => $item]);
    }

    public function createBooking(Request $rq) {
        $rules = [
            'Title'             => 'required',
            'Description'       => 'required',
            'BookingResourceID' => 'required',
            'BookDate'          => 'required|date_format:Y-m-d',
            'IsPrivate'         => 'required',
            'StartTime'         => 'required|date_format:H:i|before:EndTime',
            'EndTime'           => 'required|date_format:H:i|after:StartTime',
            'IsAllDay'          => 'required',
            'AttendUsers'       => 'required|array|min:1',
        ];

        if ($rq->has('IsAllDay') && $rq->post('IsAllDay') === "true") {
            $rules['StartTime'] = '';
            $rules['EndTime'] = '';
        }

        // run validator
        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] === false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // check time with now time - all day pass hehehe
        $startTime = null;
        if ($rules['StartTime'] !== '' && isTodayDate($dataValid['Data']['BookDate'])) {
            $now = carbonTime();
            list($hour, $minute) = explode(":", $dataValid['Data']['StartTime']);
            $startTime = carbonTime($hour, $minute);

            // check
            if ($startTime->lt($now)) {
                // error, start time can't be less than now
                return response()->json(['error' => __('system.booking_start_time_err')]);
            }
        }

        $resourceID = $dataValid['Data']['BookingResourceID'];
        $resource = BookingResource::FindCompanyResource($resourceID, $this->companyID);
        if ($resource === null) {
            return response()->json(['error' => __('system.not_found_resource')]);
        }

        // check resource availability
        if (!Booking::IsResourceAvailable($resourceID, $this->companyID, $dataValid['Data']['BookDate'], $dataValid['Data']['StartTime'], $dataValid['Data']['EndTime'])) {
            return response()->json(['error' => __('system.booking_resource_not_available')]);
        }

        // ok we start to create a new freaking booking
        $booking = new Booking;
        $booking->CompanyID = $this->companyID;
        $booking->BookingResourceID = $resourceID;
        $booking->Title = $dataValid['Data']['Title'];
        $booking->Description = $dataValid['Data']['Description'];
        $booking->BookDate = $dataValid['Data']['BookDate'];
        $booking->IsPrivate = stringToBoolean($dataValid['Data']['IsPrivate']) ? 'yes' : 'no';
        $booking->StartTime = $dataValid['Data']['StartTime'];
        $booking->EndTime = $dataValid['Data']['EndTime'];
        $booking->IsAllDay = stringToBoolean($dataValid['Data']['IsAllDay']) ? 'yes' : 'no';
        $booking->AdminUserID = $this->userObj->ID;
        setObjLoggedInfo($booking, $this->userObj, true);

        if (!$booking->save()) {
            return response()->json(['error' => __('system.action_failed')]);
        }

        // insert user to booking user
        foreach ($dataValid['Data']['AttendUsers'] as $user_id) {
            $user = User::find($user_id);
            if ($user == null) {
                continue;
            }

            // add to booking user
            $bookingUser = new BookingUser;
            $bookingUser->UserID = $user->ID;
            $bookingUser->BookingID = $booking->ID;
            $bookingUser->save();

            // Notify
            UserNotification::sendBookingNotify($booking->ID, $user->ID);
        }

        // ok we create done, return back data
        return response()->json(['success' => __('system.action_success')]);
    }


    public function editBooking($id, Request $rq) {
        $rules = [
            'Title'             => 'required',
            'Description'       => 'required',
            'BookingResourceID' => 'required',
            'BookDate'          => 'required|date_format:Y-m-d',
            'IsPrivate'         => 'required',
            'StartTime'         => 'required|date_format:H:i|before:EndTime',
            'EndTime'           => 'required|date_format:H:i|after:StartTime',
            'IsAllDay'          => 'required',
            'AttendUsers'       => 'required|array|min:1',
        ];

        if ($rq->has('IsAllDay') && $rq->post('IsAllDay') === "true") {
            $rules['StartTime'] = '';
            $rules['EndTime'] = '';
        }

        // run validator
        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] === false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // check time with now time - all day pass hehehe
        $startTime = null;
        if ($rules['StartTime'] !== '' && isTodayDate($dataValid['Data']['BookDate'])) {
            $now = carbonTime();
            list($hour, $minute) = explode(":", $dataValid['Data']['StartTime']);
            $startTime = carbonTime($hour, $minute);

            // check
            if ($startTime->lt($now)) {
                // error, start time can't be less than now
                return response()->json(['error' => __('system.booking_start_time_err')]);
            }
        }

        $resourceID = $dataValid['Data']['BookingResourceID'];
        $resource = BookingResource::FindCompanyResource($resourceID, $this->companyID);
        if ($resource === null) {
            return response()->json(['error' => __('system.not_found_resource')]);
        }

        $booking = Booking::find(decode_id($id));
        if ($booking == null) {
            return response()->json(['error' => __('system.not_found_information')]);
        }

        // only check if user change to another resource
        if ($booking->BookingResourceID !== $resource->ID) {
            // check resource availability
            if (!Booking::IsResourceAvailable($resourceID, $this->companyID, $dataValid['Data']['BookDate'], $dataValid['Data']['StartTime'], $dataValid['Data']['EndTime'])) {
                return response()->json(['error' => __('system.booking_resource_not_available')]);
            }
        }

        // ok we start edit booking
        $booking->Title = $dataValid['Data']['Title'];
        $booking->Description = $dataValid['Data']['Description'];
        $booking->BookDate = $dataValid['Data']['BookDate'];
        $booking->IsPrivate = stringToBoolean($dataValid['Data']['IsPrivate']) ? 'yes' : 'no';
        $booking->StartTime = $dataValid['Data']['StartTime'];
        $booking->EndTime = $dataValid['Data']['EndTime'];
        $booking->IsAllDay = stringToBoolean($dataValid['Data']['IsAllDay']) ? 'yes' : 'no';
        $booking->AdminUserID = $this->userObj->ID;
        setObjLoggedInfo($booking, $this->userObj);

        if (!$booking->save()) {
            return response()->json(['error' => __('system.action_failed')]);
        }

        // delete old list
        $oldIDs = $booking->BookingUser->map(function ($item) {
            return $item->UserID;
        })->all();
        foreach($booking->BookingUser as $book_user) {
            $book_user->delete();
        }

        // insert user to booking user
        foreach ($dataValid['Data']['AttendUsers'] as $user_id) {
            $user = User::find($user_id);
            if ($user == null) {
                continue;
            }

            // add to booking user
            $bookingUser = new BookingUser;
            $bookingUser->UserID = $user->ID;
            $bookingUser->BookingID = $booking->ID;
            $bookingUser->save();

            // Notify
            if (in_array($user->ID, $oldIDs)) {
                UserNotification::sendBookingNotify($booking->ID, $user->ID);
            }
        }

        // ok we create done, return back data
        return response()->json(['success' => __('system.action_success')]);
    }

    public function cancelBooking($id) {
        $id = decode_id($id);
        $booking = Booking::find($id);
        if ($booking == null) {
            return response()->json(['error' => __('system.not_found_information')]);
        }

        // ok hide this booking
        $booking->Activate = 'no';
        setObjLoggedInfo($booking, $this->userObj);
        $booking->save();

        // Notify to all user
        foreach ($booking->BookingUser as $user) {
            UserNotification::sendBookingCancelNotify($booking->ID, $user->UserID);
        }

        return response()->json(['success' => __('system.action_success')]);
    }
}
