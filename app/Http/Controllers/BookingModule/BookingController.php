<?php

namespace App\Http\Controllers\BookingModule;

use App\Http\Controllers\MYController;
use App\Models\Booking;
use App\Models\BookingResource;
use App\Models\User;
use App\Models\UserProfileFunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BookingController extends MYController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->companyID === null) {
            redirect()->to("/")->send();
        }
    }

    public function index()
    {
        $functionAccess = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userProfileID);
        $this->data['functionAccess'] = $functionAccess;

        // get all booking resource of this company
        $this->data['resources'] = BookingResource::GetCompanyResource($this->companyID);
        $this->data['users'] = $this->userDataProcess(User::GetUserByCompany($this->companyID));
        $this->data['myBooking'] = Booking::GetUserBookingFromNow($this->userObj->ID);

        // set page title
        $this->setPageTitle(__('module.MODULE_Booking'));
        $this->includingLib();
        return view('bedone.booking.index', $this->data);
    }

    private function includingLib() {
        $this->addJs(asset('js/fullcalendar.min.js'));
        $this->addJs(asset('js/modules/booking/booking.js'));
    }

    private function userDataProcess($users) {
        $data = [];
        foreach ($users as $user) {
            $data[] = [
                'id'        => decode_id($user['ID']),
                'text'      => $user['FullName'],
                'html'      => View::make('bedone.partial.user_media', ['user' => $user])->render(),
            ];
        }

        return $data;
    }

    // view resource
    public function viewResource($id) {
        $id = decode_id($id);
        $resource = BookingResource::find($id);
        if ($resource == null) {
            return redirect()->route('bookingPage')->with('error', __('system.not_found_resource'));
        }

        $this->data['resource'] = $resource;
        return view('bedone.booking.resource', $this->data);
    }
}
