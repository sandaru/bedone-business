<?php

namespace App\Http\Controllers\BookingModule;

use App\Http\Controllers\MYController;
use App\Models\BookingResource;
use App\Models\UserProfileFunction;
use App\Models\UserProfileModule;
use Illuminate\Http\Request;

class BookingResourceController extends MYController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $rq)
    {
        $keyword = $rq->get('keyword','');

        // function access
        $functionAccess = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userProfileID);
        $this->data['functionAccess'] = $functionAccess;

        // get data
        $this->data['listResource'] = BookingResource::SearchBooking($this->companyID, $keyword);
        $this->data['listResource']->withPath(route('bookingResourcePage'));

        // primary info
        $this->setPageTitle(__('module.MODULE_BookingResource'));
        $this->addJs(asset('js/modules/booking_resource/search.js'));

        return view('bedone.booking_resource.index', $this->data);
    }

    /**
     * Add Page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function add() {
        // check function access
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Add", $this->userProfileID)) {
            return redirect()->route('bookingResourcePage')->with('error', __('system.permission_denied'));
        }

        // primary info
        $this->setPageTitle(__('system.AddBookingResource'));
        $this->addJs(asset('js/modules/booking_resource/add.js'));
        $this->addCss(asset('css/modules/booking_resource.css'));

        return view('bedone.booking_resource.add', $this->data);
    }

    public function adding(Request $rq) {
        // check function access
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Add", $this->userProfileID)) {
            return response()->json(['error' => __('system.permission_denied')]);
        }

        // validation now
        $rules = [
            'Name'              => 'required|max:50',
            'Description'       => 'required|max:100',
            'Address'           => 'required|max:100',
        ];

        if ($rq->has('Gallery')) {
            $rules['Gallery_FILE'] = 'required|array';
            $rules['Gallery.*'] = 'image|max:1024';
        }

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // ok add new now
        $resource = new BookingResource;
        $resource->CompanyID = $this->companyID;
        $resource->Name = $dataValid['Data']['Name'];
        $resource->Description = $dataValid['Data']['Description'];
        $resource->Address = $dataValid['Data']['Address'];
        setObjLoggedInfo($resource, $this->userObj,true);

        // solving update
        $fileUploaded = [];
        if ($rq->has('Gallery')) {
            foreach ($dataValid['Data']['Gallery'] as $fileObj) {
                $file_name = $this->solveUpload($fileObj);

                if ($file_name != null) {
                    $fileUploaded[] = $file_name;
                }
            }
        }

        // set gallery
        $resource->Gallery = implode(",", $fileUploaded);

        // ok add
        if ($resource->save()) {
            return response()->json(['success' => __('system.action_success')]);
        } else {
            return response()->json(['error' => __('system.action_failed')]);
        }
    }

    /**
     * Edit page
     * @param $id
     */
    public function edit($id) {
        // check function access
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Edit", $this->userProfileID)) {
            return redirect()->route('bookingResourcePage')->with('error', __('system.permission_denied'));
        }

        $id = decode_id($id);
        $resource = BookingResource::find($id);
        if ($resource == null) {
            return redirect()->route('bookingResourcePage')->with('error', __('system.not_found_information'));
        }

        $this->data['resource'] = $resource;

        // primary info
        $this->setPageTitle(__('system.EditBookingResource'));
        $this->addJs(asset('js/modules/booking_resource/edit.js'));
        $this->addCss(asset('css/modules/booking_resource.css'));

        // view
        return view('bedone.booking_resource.edit', $this->data);
    }

    public function editing($id, Request $rq) {
        // check function access
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Edit", $this->userProfileID)) {
            return response()->json(['error' => __('system.permission_denied')]);
        }

        $id = decode_id($id);
        $resource = BookingResource::find($id);
        if ($resource == null) {
            return response()->json(['error' => __('system.not_found_information')]);
        }

        // validation now
        $rules = [
            'Name'              => 'required|max:50',
            'Description'       => 'required|max:100',
            'Address'           => 'required|max:100',
        ];

        if ($rq->has('OldGallery')) {
            $rules['OldGallery'] = 'required|array';
        }
        if ($rq->has('Gallery')) {
            $rules['Gallery_FILE'] = 'required|array';
            $rules['Gallery.*'] = 'image|max:1024';
        }

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // ok edit now
        $resource->Name = $dataValid['Data']['Name'];
        $resource->Description = $dataValid['Data']['Description'];
        $resource->Address = $dataValid['Data']['Address'];
        setObjLoggedInfo($resource, $this->userObj);

        // upload now
        $fileUploaded = [];
        if ($rq->has('Gallery')) {
            foreach ($dataValid['Data']['Gallery'] as $fileObj) {
                $file_name = $this->solveUpload($fileObj);

                if ($file_name != null) {
                    $fileUploaded[] = $file_name;
                }
            }
        }

        if (isset($dataValid['Data']['OldGallery'])) {
            $fileUploaded = array_merge($dataValid['Data']['OldGallery'], $fileUploaded);
        }

        // set gallery
        $resource->Gallery = implode(",", $fileUploaded);

        // ok add
        if ($resource->save()) {
            return response()->json(['success' => __('system.action_success')]);
        } else {
            return response()->json(['error' => __('system.action_failed')]);
        }
    }

    private function solveUpload($fileObj)
    {
        if ($fileObj->isValid())
        {
            $path = public_path(config('bedone.RESOURCE_IMAGE_PATH'));
            $newName = time() . "_resource_" . rand(0,999999) . "_" . $this->companyID . "." . $fileObj->getClientOriginalExtension();
            $full_path = config('bedone.RESOURCE_IMAGE_PATH') . "/" . $newName;

            // move file
            $fileObj->move($path, $newName);
            return $full_path;
        }

        return null;
    }

    /**
     * Delete
     * @param $id
     */
    public function delete($id) {
        // check function access
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Delete", $this->userProfileID)) {
            return redirect()->route('bookingResourcePage')->with('error', __('system.permission_denied'));
        }

        $id = decode_id($id);
        $resource = BookingResource::find($id);
        if ($resource == null) {
            return redirect()->route('bookingResourcePage')->with('error', __('system.not_found_information'));
        }

        // ok delete
        $resource->Activate = 'no';

        if ($resource->save()) {
            return redirect()->route('bookingResourcePage')->with('success', __('system.action_success'));
        } else {
            return redirect()->route('bookingResourcePage')->with('error', __('system.action_failed'));
        }
    }
}
