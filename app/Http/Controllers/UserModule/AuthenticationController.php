<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserResetPassword;
use App\Repositories\UserRepository;
use App\Repositories\UserSettingRepository;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Vinkla\Hashids\Facades\Hashids;

class AuthenticationController extends MYController
{
    private $userRepo;
    private $userSettingRepo;

    // this page doesn't need this
    protected $permission_required = false;
    protected $logged_required = false;
    protected $has_sidebar = false;
    protected $has_notification = false;

    public function __construct(UserRepository $userRepo, UserSettingRepository $userSettingRepo)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
        $this->userSettingRepo = $userSettingRepo;

        if (User::isLogged())
        {
            Redirect::to(config('bedone.DEFAULT_URL_LOGGED'))->send();
        }
    }

    /**
     * Login page
     */
    public function login()
    {
        // page title
        $this->data['page_title'] = __('system.login');

        // add more js
        //$this->addJs(asset('js/modules/auth/login.js'));

        return view('bedone.auth.login', $this->data);
    }

    /**
     * Login action
     */
    public function logging()
    {
        $rules = [
            'Email'         => 'required|email',
            'Password'      => 'required'
        ];

        $valid = Validator::make(Input::all(), $rules);

        if ($valid->fails())
        {
            return redirect()->back()->withInput()->withErrors($valid);
        }

        // login now
        $email = Input::get('Email');
        $pass = Input::get('Password');

        $user_log = $this->userRepo->login($email, $pass);
        if ($user_log !== false)
        {
            // set cookie
            Cookie::queue(config('bedone.COOKIE_USER_ID_KEY'), $user_log->ID, config('bedone.COOKIE_TIME'));
            Cookie::queue(config('bedone.COOKIE_PASSWORD_KEY'), md5($user_log->Password), config('bedone.COOKIE_TIME'));

            // to default page
            return redirect()
                ->to(config('bedone.DEFAULT_URL_LOGGED'));
        }
        else
        {
            return redirect()->back()->withInput()->with('error', __('system.login_failed'));
        }
    }

    /**
     * Register page
     */
    public function register()
    {
        // page title
        $this->data['page_title'] = __('system.register');

        // add more js
        //$this->addJs(asset('js/modules/auth/register.js'));

        return view('bedone.auth.register', $this->data);
    }

    /**
     * Registering
     */
    public function registering(Request $rq)
    {
        $rules = [
            'Email'                 => 'required|email|unique:User,Email',
            'Password'              => 'required|min:8',
            'RetypePassword'        => 'required|min:8|same:Password',
            'FirstName'             => 'required',
            'LastName'              => 'required'
        ];

        // validation data
        $dataValid = $this->runValidator($rules, $rq);

        // check valid status
        if ($dataValid['Valid'] == true)
        {
            // log info
            $dataValid['Data']['CreatedByID'] = $dataValid['Data']['ModifiedByID'] = "0"; // not know
            $dataValid['Data']['CreatedByName'] = $dataValid['Data']['ModifiedByName'] = $dataValid['Data']['FirstName'] . " " . $dataValid['Data']['LastName'];
            $dataValid['Data']['CreatedDate'] = $dataValid['Data']['ModifiedDate'] = date(config('bedone.DATETIME_FORMAT'));

            // password encryption
            $dataValid['Data']['Password'] = password_encrypt($dataValid['Data']['Password']);

            // registering now...
            $newUser = $this->userRepo->create($dataValid['Data']);
            $this->userSettingRepo->createDefaultSetting($newUser);

            // Change log
            $newUser->CreatedByID = $newUser->ModifiedByID = $newUser->ID;
            $newUser->save();

            // back to login
            return redirect()->route('loginPage')->with('success', __('system.register_success'));
        }
        else {
            // failed validation
            return redirect()->back()->withErrors($dataValid['Errors'])->withInput();
        }
    }

    /**
     * Forgot password page
     */
    public function forgot()
    {
        // page title
        $this->data['page_title'] = __('system.forgot');

        // add more js
        //$this->addJs(asset('js/modules/auth/forgot.js'));

        return view('bedone.auth.forgot', $this->data);
    }

    /**
     * Forgotting
     */
    public function forgotting(Request $rq)
    {
        $rules = [
            'Email' => 'required|email|exists:User,Email'
        ];

        // validation data
        $dataValid = $this->runValidator($rules, $rq);

        // check valid status
        if ($dataValid['Valid'] == true)
        {
            // get user by email
            $user = User::getByEmail($dataValid['Data']['Email'])->first();

            // generate token and insert into UserRecover
            $hash_key = sha1(md5($user->ID . $user->Email) . time());
            Log::debug("LENGTH: " . strlen($hash_key));

            // remove old request
            $old_request = UserResetPassword::byUserID($user->ID)->get();
            foreach ($old_request as $request)
            {
                $request->delete();
            }

            // create new row
            $user_hash = new UserResetPassword;
            $user_hash->UserID = $user->ID;
            $user_hash->ResetKey = $hash_key;
            $user_hash->CreatedDate = sql_date();
            $user_hash->save();

            // send email ($User and $HashKey)
            try {
                Mail::send('bedone.mail.forgot_password', ['User' => $user, 'HashKey' => $hash_key], function($message) use ($user) {
                    $message->from(config('bedone.SENDER_EMAIL'), config('bedone.SENDER_NAME'));

                    $message->to($user->Email);

                    $message->subject(__('system.forgot_password_title'));
                });
            }
            catch (\Exception $e)
            {
                Log::debug("ERROR:" . $e->getMessage());
                return redirect()->route('forgotPage')->with('error', __('system.forgot_email_failed'));
            }

            // back to login
            return redirect()->route('forgotPage')->with('success', __('system.forgot_success'));
        }
        else {
            // failed validation
            return redirect()->back()->withErrors($dataValid['Errors'])->withInput();
        }
    }

    /**
     * @param $key
     */
    public function retrievePass($key)
    {
        // find if this key exists
        $user_key = UserResetPassword::where('ResetKey', $key)->get();

        if ($user_key->count() <= 0)
            return redirect()->route('loginPage')->with('error', __('system.forgot_key_failed'));

        $this->setPageTitle(__('system.ChangePassword'));

        $this->data['key'] = $key;
        return view('bedone.auth.forgot_changepass', $this->data);
    }

    /**
     * @param $key
     * @param Request $rq
     */
    public function doRetrievePass($key, Request $rq)
    {
        // find if this key exists
        $user_key = UserResetPassword::where('ResetKey', $key)->get();

        if ($user_key->count() <= 0)
            return redirect()->route('loginPage')->with('error', __('system.forgot_key_failed'));

        // validation
        $rules = [
            'Password'              => 'required|min:8',
            'RetypePassword'        => 'required|min:8|same:Password',
        ];

        $dataValid = $this->runValidator($rules, $rq);

        if ($dataValid['Valid'] === false)
            return redirect()->back()->withErrors($dataValid['Errors']);

        // get info
        $userKey = $user_key->first();
        $user = $userKey->User;

        // update info
        $user->Password = password_encrypt($dataValid['Data']['Password']);
        $user->save();

        return redirect()->route('loginPage')->with('success', __('system.password_changed_forgot'));
    }
}
