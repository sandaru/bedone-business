<?php

namespace App\Http\Controllers;

use App\Repositories\CompanyRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;

class UserController extends MYController
{
    private $userRepo;
    private $companyRepo;
    protected $permission_required = false;

    public function __construct(UserRepository $userRepo, CompanyRepository $companyRepo)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
        $this->companyRepo = $companyRepo;
    }

    /**
     * View my user profile
     * @param $id
     */
    public function UserProfile($id = null)
    {
        $this->data['isCurrentUser'] = true;
        if (empty($id))
        {
            // set data user
            $this->data['User'] = $this->userObj;

            // get necessary data
            if ($this->userObj->CompanyID === null)
            {
                $this->data['AllCompanies'] = $this->companyRepo->getAllCompany();
            }

            // add menu action
            $this->addActionMenu(__('module.EditProfile'), "fa-edit", route('editProfilePage'));
        }
        else
        {
            // check if this current logged user is have company
            if ($this->userObj->CompanyID === null && $this->userObj->isSuperAdmin() == false)
            {
                // not have yet, can't access
                return redirect()
                    ->to(config('bedone.DEFAULT_URL_LOGGED'))
                    ->with('error', __("system.permission_denied"));
            }

            // decrypt ID
            $decrypted_id = decode_id($id);

            // query
            $user = $this->userRepo->find($decrypted_id);

            // check if this user is exists or same company or not
            if ($user == null || ($user->CompanyID != $this->userObj->CompanyID && $this->userObj->isSuperAdmin() == false))
            {
                return redirect()
                    ->to(config('bedone.DEFAULT_URL_LOGGED'))
                    ->with('error', __("system.user_profile_not_found"));
            }

            $this->data['User'] = $user;
            $this->data['isCurrentUser'] = false;
        }

        // set title
        $this->setPageTitle(__('module.MyProfile'));

        // add js
        $this->addJs(asset('js/modules/user/userprofile.js'));

        return view("bedone.usermodule.userprofile", $this->data);
    }

    /**
     * Edit current logged user profile
     */
    public function EditProfile()
    {
        $this->data['User'] = $this->userObj;

        // set title
        $this->setPageTitle(__('module.MyProfile'));

        // set script
        $this->addJs(asset('js/modules/user/editprofile.js'));

        return view("bedone.usermodule.editprofile", $this->data);
    }

    /**
     * Editing profile
     */
    public function EditingProfile(Request $rq)
    {
        $rules = [
            "Introduction"      => "",
            "FirstName"         => "required",
            "LastName"          => "required",
            "Gender"            => ['required', 'min:1', 'max:1', 'regex:/F|M/'],
            "Birthday"          => 'required|date_format:"Y-m-d"',
            "JobTitle"          => '',
            "WorkAddress"       => '',
            "WorkPhone"         => '',
            "HomePhone"         => '',
        ];

        if ($rq->has('ProfileImage'))
            $rules['ProfileImage_FILE']  = 'required|image|max:500';
        if ($rq->has('FeatureImage'))
            $rules['FeatureImage_FILE']  = 'required|image|max:1024';

        $dataValid = $this->runValidator($rules, $rq);

        if ($dataValid['Valid'])
        {
            // data is valid, edit now
            foreach($dataValid['Data'] as $key => $value)
                $this->userObj->$key = $value;

            // solving upload - profile image
            if (isset($dataValid['Data']['ProfileImage']) && $dataValid['Data']['ProfileImage']->isValid())
            {
                $path = public_path(config('bedone.PROFILE_IMAGE_PATH'));
                $newName = time() . "_profile_" . $this->userObj->ID . "." . $dataValid['Data']['ProfileImage']->getClientOriginalExtension();
                $full_path = config('bedone.PROFILE_IMAGE_PATH') . "/" . $newName;

                // move file
                $dataValid['Data']['ProfileImage']->move($path, $newName);
                $this->userObj->ProfileImage = $full_path;
            }

            // solving upload - feature image
            if (isset($dataValid['Data']['FeatureImage']) && $dataValid['Data']['FeatureImage']->isValid())
            {
                $path = public_path(config('bedone.COVER_IMAGE_PATH'));
                $newName = time() . "_cover_" . $this->userObj->ID . "." . $dataValid['Data']['FeatureImage']->getClientOriginalExtension();
                $full_path = config('bedone.COVER_IMAGE_PATH') . "/" . $newName;

                // move file
                $dataValid['Data']['FeatureImage']->move($path, $newName);
                $this->userObj->FeatureImage = $full_path;
            }

            // save changes
            $this->userObj->save();

            return redirect()->route('userProfilePage')->with('success', __('system.saved_changes'));
        }
        else {
            // failed
            return redirect()->back()->withInput()->withErrors($dataValid['Errors']);
        }
    }

    /**
     * Resend request to join company
     */
    public function ResendRequest()
    {
        if ($this->userObj->CompanyJoinRequest === null)
            return redirect()->back();

        $joinRequest = $this->userObj->CompanyJoinRequest;
        $joinRequest->PushedDate = sql_date();
        $joinRequest->save();

        return redirect()->route('userProfilePage')->with('success', __('system.re_request_success'));
    }

    /**
     * Cancel request to join company
     */
    public function CancelRequest()
    {
        if ($this->userObj->CompanyJoinRequest === null)
            return redirect()->back();

        $joinRequest = $this->userObj->CompanyJoinRequest;
        $joinRequest->delete();
        $this->userObj->CompanyJoinRequest = null;

        return redirect()->route('userProfilePage')->with('success', __('system.cancel_request_success'));
    }


    /**
     * Log out
     */
    public function logout()
    {
        // clear cookie
        Cookie::queue(Cookie::forget(config('bedone.COOKIE_USER_ID_KEY')));
        Cookie::queue(Cookie::forget(config('bedone.COOKIE_PASSWORD_KEY')));

        return redirect()->route("loginPage");
    }


    /**
     * View settings
     */
    public function setting()
    {
        $this->data['Setting'] = $this->userObj->UserSetting;
        $this->data['Field'] = [
            'Birthday',
            'Gender',
            'Email',
            'WorkPhone',
            'HomePhone',
            'WorkAddress'
        ];

        // set title
        $this->setPageTitle(__('module.Settings'));
        return view("bedone.usermodule.setting", $this->data);
    }

    /**
     * Change password action
     * @param Request $rq
     */
    public function changePassword(Request $rq)
    {
        $rules = [
            'OldPassword'       => 'required|min:6',
            'NewPassword'       => 'required|min:8',
            'RetypeNewPassword' => 'required|min:8|same:NewPassword'
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false)
            return redirect()->back()->withErrors($dataValid['Errors']);

        // check if old password is same as new password
        $hashed_old = password_encrypt($dataValid['Data']['OldPassword']);
        if ($hashed_old !== $this->userObj->Password)
            return redirect()->back()->with('error', __('system.old_password_wrong'));

        // ok change password
        $this->userObj->Password = password_encrypt($dataValid['Data']['NewPassword']);
        $this->userObj->save();

        // set cookies again
        Cookie::queue(config('bedone.COOKIE_USER_ID_KEY'), $this->userObj->ID, config('bedone.COOKIE_TIME'));
        Cookie::queue(config('bedone.COOKIE_PASSWORD_KEY'), md5($this->userObj->Password), config('bedone.COOKIE_TIME'));

        return redirect()->back()->with('success', __('system.password_changed'));
    }

    /**
     * Change settings
     * @param Request $rq
     */
    public function changeSetting(Request $rq)
    {
        $rules = [
            'Birthday' => 'required',
            'Gender' => 'required',
            'Email' => 'required',
            'WorkPhone' => 'required',
            'HomePhone' => 'required',
            'WorkAddress' => 'required',
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false)
            return redirect()->back()->withErrors($dataValid['Errors']);

        // ok change setting
        $setting = $this->userObj->UserSetting;
        foreach ($dataValid['Data'] as $key => $value) {
            $setting->$key = $value;
        }
        $setting->save();

        return redirect()->back()->with('success', __('system.privacy_changed'));
    }
}
