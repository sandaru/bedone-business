<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/12/2018
 * Time: 8:39 PM
 */

namespace App\Http\Controllers;


use App\Models\UserNotification;
use Illuminate\Http\Request;

class NotificationController extends MYController
{
    protected $permission_required = false;
    protected $has_sidebar = false;
    protected $has_notification = false;

    public function __construct()
    {
        parent::__construct();

        // ajax only
        if (request()->ajax() === false) {
            redirect()->to("/")->send();
        }
    }

    public function SeenNotification() {
        UserNotification::SeenAllNotification($this->userObj->ID);

        return response()->json(['success' => __('system.action_success')]);
    }

    public function GetNotification(Request $rq) {
        $offset = $rq->get('offset', 0);
        $limit = $rq->get('limit', config('bedone.MAX_RESULT'));

        $Notification = ['Data' => UserNotification::SearchNotification($this->userObj->ID, $offset, $limit)];

        return view('bedone.notification.render_item', ['Notification' => $Notification]);
    }
}