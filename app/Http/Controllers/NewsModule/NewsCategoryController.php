<?php

namespace App\Http\Controllers\NewsModule;

use App\Http\Controllers\MYController;
use App\Models\NewsCategory;
use App\Models\UserProfileFunction;
use Illuminate\Http\Request;

class NewsCategoryController extends MYController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (empty($this->userObj->CompanyID))
            redirect()->to(config('bedone.DEFAULT_URL_LOGGED'))->with("error", __('system.permission_denied'))->send();
    }

    /**
     * News Category Maintenance page
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all category name
        $this->data['AllCategories'] = NewsCategory::GetAllByCompany($this->userObj->CompanyID);

        // all permission
        $this->data['PagePermission'] = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userObj->UserProfileAccess->UserProfileID);

        // set page title
        $this->setPageTitle(__('module.NewsCategoryMaintenance'));

        // add js
        $this->addJs(asset("js/modules/news/search_page.js"));

        return view('bedone.news_maintenance.index', $this->data);
    }

    /**
     * Add new category
     */
    public function add()
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Edit', $this->userObj->UserProfileAccess->UserProfileID))
            return redirect()->back()->with('error', __('system.permission_denied'));

        $this->setPageTitle(__('module.AddCategory'));
        $this->addJs(asset('js/modules/news/add_category.js'));

        // send to view
        return view("bedone.news_maintenance.add", $this->data);
    }

    /**
     * On adding newsCategory
     * @param Request $rq
     */
    public function adding(Request $rq)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Edit', $this->userObj->UserProfileAccess->UserProfileID))
            return redirect()->back()->with('error', __('system.permission_denied'));

        $rules = [
            'Name'              => 'required|max:30',
            'Description'       => 'required|max:50',
            'Icon'              => 'required'
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false)
            return redirect()->back()->withInput()->withErrors($dataValid['Errors']);

        NewsCategory::createCategory($dataValid['Data']['Name'], $dataValid['Data']['Description'], $dataValid['Data']['Icon'], $this->userObj);
        return redirect()->route('newsMaintenancePage')->with('success', __('system.saved_changes'));
    }

    /**
     * Edit page
     * @param $id
     */
    public function edit($id)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Edit', $this->userObj->UserProfileAccess->UserProfileID))
            return redirect()->back()->with('error', __('system.permission_denied'));

        $id = decode_id($id);

        // get category
        $category = NewsCategory::find($id);
        if ($category == null)
            return redirect()->back()->with('error', __('system.not_found_information'));

        // set data
        $this->data['Category'] = $category;

        // set page title
        $this->setPageTitle(__('system.Edit') . " - " . $category->Name);
        $this->addJs(asset('js/modules/news/edit_category.js'));

        // send to view
        return view('bedone.news_maintenance.edit', $this->data);
    }

    /**
     * Editing news category
     * @param $id
     * @param Request $rq
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editing($id, Request $rq)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Edit', $this->userObj->UserProfileAccess->UserProfileID))
            return redirect()->back()->with('error', __('system.permission_denied'));

        $id = decode_id($id);

        // get category
        $category = NewsCategory::find($id);
        if ($category == null)
            return redirect()->back()->with('error', __('system.not_found_information'));

        $rules = [
            'Name'              => 'required|max:30',
            'Description'       => 'required|max:50',
            'Icon'              => 'required'
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false)
            return redirect()->back()->withInput()->withErrors($dataValid['Errors']);

        NewsCategory::updateCategory($id, $dataValid['Data']['Name'], $dataValid['Data']['Description'], $dataValid['Data']['Icon'], $this->userObj);
        return redirect()->route('newsMaintenancePage')->with('success', __('system.saved_changes'));
    }


    /**
     * Delete category
     * @param $id
     */
    public function DeleteCategory($id)
    {
        // check permission
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Delete', $this->userObj->UserProfileAccess->UserProfileID))
            return redirect()->back()->with('error', __('system.permission_denied'));

        $id = decode_id($id);

        $category = NewsCategory::find($id);
        if ($category == null)
            return redirect()->back()->with('error', __('system.delete_failed'));

        // check if we still have news in this category
        if ($category->News != null)
            return redirect()->back()->with('error', __('system.delete_failed'));

        // nope, delete now
        $category->delete();

        return redirect()->back()->with('success', __('system.delete_success'));
    }
}
