<?php

namespace App\Http\Controllers\NewsModule;

use App\Http\Controllers\MYController;
use App\Models\CompanyJoinRequest;
use App\Models\CompanyModule;
use App\Models\News;
use App\Models\NewsComment;
use App\Models\UserProfileAccess;
use App\Models\UserProfileFunction;
use App\Repositories\CompanyRepository;
use App\Repositories\UserProfileFunctionRepository;
use App\Repositories\UserProfileModuleRepository;
use App\Repositories\UserProfileRepository;
use Illuminate\Http\Request;

/**
 * Class NewsCommentApiController
 */
class NewsCommentApiController extends MYController
{
    protected $permission_required = false;
    protected $has_sidebar = false;

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Get Comment
     * @param $newsid
     */
    public function GetComment($newsid, Request $rq)
    {
        $offset = $rq->get('offset', 0);
        $limit = $rq->get('limit', config('bedone.MAX_RESULT'));

        return NewsComment::GetComment($offset, $limit, decode_id($newsid));
    }

    /**
     * Create comment
     * @param $newsid
     * @param Request $rq
     * @return \Illuminate\Http\JsonResponse
     */
    public function CreateComment($newsid, Request $rq)
    {
        if (!UserProfileFunction::CanAccessFunction("NewsFeed", 'AddComment', $this->userObj->UserProfileAccess->UserProfileID))
            return response()->json(['error' => __('system.permission_denied')]);

        $newsid = decode_id($newsid);
        if (News::find($newsid) == false)
            return response()->json(['error' => __('system.not_found_information')]);

        // ok validate data
        $rules = [
            'Comment'       => 'required|max:300',
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid']===false)
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);

        // ok create a comment
        $cmt_id = NewsComment::createComment($newsid, strip_tags($dataValid['Data']['Comment']), $this->userObj);

        if ($cmt_id > 0)
            return response()->json(['success' => __('system.action_success')]);
        else
            return response()->json(['error' => __('system.action_failed')]);
    }

    /**
     * Edit comment
     * @param $cmtid
     * @param Request $rq
     */
    public function EditComment($cmtid, Request $rq)
    {
        $cmtid = decode_id($cmtid);
        $cmt = NewsComment::find($cmtid);
        if ($cmt == null)
            return response()->json(['error' => __('system.not_found_information')]);

        // verify comment
        $comment = $rq->post('Comment');

        if ($comment === "")
            return response()->json(['error' => __('system.invalid_params')]);

        // edit comment
        $cmt->Comment = strip_tags($comment);

        // log data
        $cmt->ModifiedByID = $this->userObj->ID;
        $cmt->ModifiedByName = $this->userObj->full_name;
        $cmt->ModifiedDate = now();
        $cmt->save();

        return response()->json(['success' => __('system.action_success')]);
    }

    /**
     * Delete comment
     * @param $cmtid
     */
    public function DeleteComment($cmtid)
    {
        if (!UserProfileFunction::CanAccessFunction("NewsFeed", 'DeleteComment', $this->userObj->UserProfileAccess->UserProfileID))
            return response()->json(['error' => __('system.permission_denied')]);

        $cmtid = decode_id($cmtid);
        $cmt = NewsComment::find($cmtid);
        if ($cmt == null)
            return response()->json(['error' => __('system.not_found_information')]);

        $cmt->delete();
        return response()->json(['success' => __('system.delete_success')]);
    }
}
