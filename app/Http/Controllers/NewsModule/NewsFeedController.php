<?php

namespace App\Http\Controllers\NewsModule;

use App\Http\Controllers\MYController;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\UserProfileFunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class NewsFeedController extends MYController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->companyID == null)
            Redirect::route('userProfilePage')->with('error', __('system.permission_denied'))->send();
    }

    /**
     * Newsfeed page
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // all categories
        $list_cate = NewsCategory::GetAllByCompany($this->companyID);
        $this->data['Categories'] = [];
        foreach ($list_cate as $item)
            $this->data['Categories'][] = [
                'ID'            => encode_id($item->ID),
                'Name'          => $item->Name,
                'Icon'          => $item->Icon,
                'Description'   => $item->Description
            ];


        // title
        $this->setPageTitle(__('module.News'));

        // get function access
        $functionAccess = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userProfileID);
        if ($functionAccess['Add'])
            $this->addActionMenu(__('system.AddNews'), "fa-plus", null, "VueNewsFeed.addNews()");

        // set function
        $this->data['functionAccess'] = $functionAccess;

        // add js
        $this->addJs(asset('js/modules/news/newsfeed.js'));

        // add css
        $this->addCss(asset('css/modules/news.css'));

        return view('bedone.news.index', $this->data);
    }

    /**
     * [API] Get data for newsfeed
     * @param Request $rq
     */
    public function get_data(Request $rq)
    {
        $categoryID = $rq->get('CategoryID');
        $offset = $rq->get('offset', 0);
        $limit = $rq->get('limit', config('bedone.MAX_RESULT'));

        $categoryID = decode_id($categoryID);
        if ($categoryID==="failed")
            $categoryID = null;

        // TODO: Query data
        return response()->json(News::GetNews($offset, $limit, $this->companyID, $categoryID),200);
    }

    public function addNews(Request $rq)
    {
        // check function access
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Add", $this->userProfileID)) {
            return response()->json(['error' => __('system.permission_denied')]);
        }

        $rules = [
            'Title'             => 'required|max:150',
            'Content'           => 'required',
            'Description'       => 'required|max:200',
            'CategoryID'        => 'required',
        ];

        if ($rq->has('UploadImages'))
        {
            $rules['UploadImages_FILE'] = 'required|array';
            $rules['UploadImages.*'] = 'image|max:1024';
        }

        // run validator
        $dataValid = $this->runValidator($rules, $rq);

        // fail
        if ($dataValid['Valid'] == false)
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);

        // check categories
        $cate_id = decode_id($dataValid['Data']['CategoryID']);
        if (NewsCategory::find($cate_id) == null)
            return response()->json(['error' => __('system.not_found_category')]);

        // ok add news posts

        // upload images first
        $img_uploaded = [];
        if ($rq->has('UploadImages'))
        {
            foreach ($dataValid['Data']['UploadImages'] as $img)
            {
                $upload = $this->solveUpload($img);
                if ($upload != null)
                    $img_uploaded[] = $upload;
            }
        }

        // ok create news
        $news = News::createNews($cate_id, $dataValid['Data']['Title'], $dataValid['Data']['Description'], $dataValid['Data']['Content'], $img_uploaded, $this->userObj);

        if ($news->ID > 0)
            return response()->json(['success' => __('system.news_created')]);
        else
            return response()->json(['error' => __('system.action_failed')]);
    }

    public function editNews($id, Request $rq)
    {
        // check function access
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Edit", $this->userProfileID)) {
            return response()->json(['error' => __('system.permission_denied')]);
        }

        $id = decode_id($id);
        if (News::find($id) == null)
            return response()->json(['error' => __('system.not_found_information')]);

        $rules = [
            'Title'             => 'required|max:150',
            'Content'           => 'required',
            'Description'       => 'required|max:200',
            'CategoryID'        => 'required',
            'FeatureImage'      => ''
        ];

        if ($rq->has('UploadImages'))
        {
            $rules['UploadImages_FILE'] = 'required|array';
            $rules['UploadImages.*'] = 'image|max:1024';
        }

        // run validator
        $dataValid = $this->runValidator($rules, $rq);
        // fail
        if ($dataValid['Valid'] == false)
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);

        // check categories
        $cate_id = decode_id($dataValid['Data']['CategoryID']);
        if (NewsCategory::find($cate_id) == null)
            return response()->json(['error' => __('system.not_found_category')]);

        // upload images first
        $img_uploaded = [];
        if ($rq->has('UploadImages'))
        {
            foreach ($dataValid['Data']['UploadImages'] as $img)
            {
                $upload = $this->solveUpload($img);
                if ($upload != null)
                    $img_uploaded[] = $upload;
            }
        }

        // if still have old imgs
        if (isset($dataValid['Data']['FeatureImage']) && is_array($dataValid['Data']['FeatureImage']))
        {
            // old image still exist, append into img_url
//            $oldImgs = getTagValue($dataValid['Data']['FeatureImage']);
            $img_uploaded = array_merge($dataValid['Data']['FeatureImage'], $img_uploaded);
        }

        // edit now
        $news = News::editNews($id, $cate_id, $dataValid['Data']['Title'], $dataValid['Data']['Description'], $dataValid['Data']['Content'], $img_uploaded, $this->userObj);
        return response()->json(['success' => __('system.news_edited')]);
    }

    /**
     * Solving upload
     * @param $fileObj
     */
    private function solveUpload($fileObj)
    {
        if ($fileObj->isValid())
        {
            $path = public_path(config('bedone.NEWS_IMAGE_PATH'));
            $newName = time() . "_news_" . rand(0, 999999) . "_" . $this->companyID . "." . $fileObj->getClientOriginalExtension();
            $full_path = config('bedone.NEWS_IMAGE_PATH') . "/" . $newName;

            // move file
            $fileObj->move($path, $newName);
            return $full_path;
        }

        return null;
    }

    /**
     * Delete news
     * @param $id
     */
    public function deleteNews($id)
    {

        // check permission
        if (!UserProfileFunction::CanAccessFunction($this->module_id, 'Delete', $this->userProfileID))
            return response()->json(['error' => __('system.permission_denied')]);

        $id = decode_id($id);
        $news = News::find($id);
        if ($news == null)
            return response()->json(['error' => __('system.not_found_information')]);

        $news->delete();
        return response()->json(['success' => __('system.delete_success')]);
    }

    /**
     * View news
     */
    public function viewNews($id)
    {
        $id = decode_id($id);
        $news = News::find($id);
        if ($news == null)
            return response()->json(['error' => __('system.not_found_information')]);

        // set title
        $this->setPageTitle($news->Title);

        // add js
        $this->addCss(asset('css/prism.css'));
        $this->addJs(asset('js/prism.js'));
        $this->addJs(asset('js/modules/news/view_news.js'));

        // get function access
        $functionAccess = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userObj->UserProfileAccess->UserProfileID);

        // set function
        $this->data['functionAccess'] = $functionAccess;
        $this->data['News'] = $news;

        return view("bedone.news.view", $this->data);
    }
}
