<?php

namespace App\Http\Controllers\CompanyModule;

use App\Http\Controllers\MYController;
use App\Models\User;
use App\Models\UserProfileFunction;
use App\Repositories\CompanyRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CompanyController extends MYController
{
    private $companyRepo;

    public function __construct(CompanyRepository $companyRepo)
    {
        parent::__construct();
        $this->companyRepo = $companyRepo;
    }

    /**
     * View user current company
     */
    public function ViewCompany()
    {
        if ($this->userObj->Company == null)
            return redirect()->to(config('bedone.DEFAULT_URL_LOGGED'))->with('error', __('system.permission_denied'));

        // company data
        $this->data['Company'] = $this->userObj->Company;

        // title will be this company name
        $this->setPageTitle($this->userObj->Company->Name);
        $this->addJs(asset('js/modules/company/company.js'));

        // set action page
        if (UserProfileFunction::CanAccessFunction($this->module_id, "Edit", $this->userObj->UserProfileAccess->UserProfileID))
        {
            $this->addActionMenu(__('module.EditCompany'), "fa-edit", route('editCompanyPage'));
        }
        $this->addActionMenu(__('module.LeaveCompany'), "fa-user-minus", null, "CompanyPageAction.leaveCompany()");

        // get all administrator of this company
        $this->data['AllAdmins'] = $this->companyRepo->getAdmins($this->data['Company']);

        return view("bedone.company.index", $this->data);
    }

    /**
     * Edit company page
     */
    public function EditCompany()
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Edit", $this->userObj->UserProfileAccess->UserProfileID))
            return redirect()->to(config('bedone.DEFAULT_URL_LOGGED'))->with('error', __('system.permission_denied'));

        // company data
        $this->data['Company'] = $this->userObj->Company;

        // title will be this company name
        $this->setPageTitle(__('system.Edit'). " " .$this->userObj->Company->Name);

        // get all administrator of this company
        $this->data['AllAdmins'] = $this->companyRepo->getAdmins($this->data['Company']);

        // add js
        $this->addJs(asset('js/modules/company/edit.js'));

        return view('bedone.company.edit', $this->data);
    }

    /**
     * Do Edit Company
     * @param $rq
     */
    public function EditingCompany(Request $rq)
    {
        if (!UserProfileFunction::CanAccessFunction($this->module_id, "Edit", $this->userObj->UserProfileAccess->UserProfileID))
            return redirect()->to(config('bedone.DEFAULT_URL_LOGGED'))->with('error', __('system.permission_denied'));

        $rules = [
            'Name'          => 'required|min:10|max:50',
            'Description'   => 'required'
        ];

        if ($rq->has('FeatureImage'))
            $rules['FeatureImage_FILE'] = "required|image|max:1024";

        $dataValid = $this->runValidator($rules, $rq);

        if ($dataValid['Valid'] == false)
            return redirect()->back()->withInput()->withErrors($dataValid["Errors"]);

        // ok we update company info
        $company = $this->userObj->Company;
        $company->Name = $dataValid['Data']['Name'];
        $company->Description = $dataValid['Data']['Description'];

        // if have upload
        if (isset($dataValid['Data']['FeatureImage']) && $dataValid['Data']['FeatureImage']->isValid())
        {
            $path = public_path(config('bedone.COVER_COMPANY_IMAGE_PATH'));
            $newName = time() . "_cover_" . $company->ID . "." . $dataValid['Data']['FeatureImage']->getClientOriginalExtension();
            $full_path = config('bedone.COVER_COMPANY_IMAGE_PATH') . "/" . $newName;

            // move file
            $dataValid['Data']['FeatureImage']->move($path, $newName);
            $company->FeatureImage = $full_path;
        }

        $company->save();

        return redirect()->route('companyPage')->with('success', __('system.saved_changes'));
    }

    /**
     * Leave company..
     */
    public function leaving_company() {
        $company = $this->userObj->Company;
        if ($company->UserAdminID === $this->userObj->ID) {
            return redirect()->route('companyPage')->with('error', __('system.creator_of_company_cant_leave'));
        }

        User::LeaveCompany($this->userObj->ID);
        return redirect()->route('userProfilePage')->with('success', __('system.action_success'));
    }
}
