<?php

namespace App\Http\Controllers\CompanyModule;

use App\Http\Controllers\MYController;
use App\Models\CompanyJoinRequest;
use App\Models\CompanyModule;
use App\Models\UserProfileAccess;
use App\Repositories\CompanyRepository;
use App\Repositories\UserProfileFunctionRepository;
use App\Repositories\UserProfileModuleRepository;
use App\Repositories\UserProfileRepository;
use Illuminate\Http\Request;

/**
 * Class CompanyApiController
 */
class CompanyApiController extends MYController
{
    protected $permission_required = false;
    protected $has_sidebar = false;
    private $companyRepo;
    private $userProfileRepo;
    private $profileModuleRepo;
    private $profileFunctionRepo;

    public function __construct(CompanyRepository $companyRepo, UserProfileRepository $userProfileRepo,
                                UserProfileModuleRepository $profileModuleRepo, UserProfileFunctionRepository $profileFunctionRepo)
    {
        parent::__construct();
        $this->companyRepo = $companyRepo;
        $this->userProfileRepo = $userProfileRepo;
        $this->profileModuleRepo = $profileModuleRepo;
        $this->profileFunctionRepo = $profileFunctionRepo;
    }

    /**
     * Create Company API
     * @param Request $rq
     */
    public function createCompany(Request $rq)
    {
        $company_name = $rq->post('Name');

        if ($company_name === "")
            return $this->API_Failed();

        // create a new company
        $new_company_id = $this->companyRepo->createNewCompany($company_name, $this->userObj);

        // set module for this company
        CompanyModule::setDefaultModule($new_company_id, $this->userObj);

        // create a master profile
        $profile = $this->userProfileRepo->createDefaultProfile($new_company_id, $this->userObj);

        // set modules for this profile
        $profileModule = $this->profileModuleRepo->setDefaultModule($profile->ID);

        // set functions for this profile
        $this->profileFunctionRepo->setDefaultModuleFunction($profileModule);

        // set this user into the profile
        UserProfileAccess::setUserProfile($this->userObj->ID, $profile->ID);

        // and set this user into this company
        $this->userObj->setCompany($new_company_id);

        // return now
        $return_data = [
            'data' => [
                'CompanyID'    => $new_company_id
            ]
        ];

        return $this->API_Success($return_data);
    }

    /**
     * Join company api
     * @param Request $rq
     */
    public function joinCompany(Request $rq)
    {
        $company_id = $rq->post('CompanyID');
        $company_id = decode_id($company_id);

        if (is_numeric($company_id) == false || $this->userObj->CompanyJoinRequest != null)
            return $this->API_Failed();

        // request to join company now
        $join = CompanyJoinRequest::joinCompany($company_id, $this->userObj);

        if ($join)
            return $this->API_Success(['success' => __('system.join_company_success')]);
        else
            return $this->API_Failed();
    }
}
