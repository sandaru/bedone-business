<?php

namespace App\Http\Controllers\UserProfileModule;

use App\Http\Controllers\MYController;
use App\Models\CompanyModule;
use App\Models\Module;
use App\Models\UserProfile;
use App\Models\UserProfileAccess;
use App\Models\UserProfileFunction;
use App\Models\UserProfileModule;
use function GuzzleHttp\Promise\settle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserProfileController extends MYController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // add necessary css/js for this module only
        $this->addCss(asset('themes/default/style.min.css'));
        $this->addJs(asset('js/jstree.min.js'));
        $this->addJs(asset('js/lodash.min.js'));
    }

    /**
     * Search page
     */
    public function index(Request $rq)
    {
        $filter_by = $rq->get('filter_by','full_name');
        $keyword = $rq->get('keyword','');

        // get function access
        $this->data['functionAccess'] = UserProfileFunction::GetAllFunctionPermission($this->module_id, $this->userObj->UserProfileAccess->UserProfileID);

        // get data
        $this->data['listUser'] = UserProfile::SearchProfile($this->companyID, $keyword, $filter_by);
        $this->data['listUser']->withPath(route('profileMaintenancePage'));

        // set title
        $this->setPageTitle(__('module.MODULE_UserProfile'));

        // add source
        $this->addCss(asset('css/datatables.min.css'));
        $this->addCss(asset('css/dataTables.bootstrap4.min.css'));
        $this->addJs(asset('js/datatables.min.js'));
        $this->addJs(asset('js/dataTables.bootstrap4.min.js'));
        $this->addJs(asset('js/modules/user_profile/search.js'));

        return view('bedone.user_profile.search', $this->data);
    }

    /**
     * add page
     */
    public function add()
    {
        $this->data['CompanyModules'] = CompanyModule::getCompanyModule($this->companyID);

        // set title
        $this->setPageTitle(__('system.AddUserProfile'));

        // add js
        $this->addJs(asset('js/modules/user_profile/add.js'));

        return view('bedone.user_profile.add', $this->data);
    }

    /**
     * Adding a new user profile
     * @param Request $rq
     */
    public function adding(Request $rq)
    {
        $rules = [
            'Data'              => 'required|array',
            'Data.*.ID'         => 'required|integer',
            'Data.*.Checked'    => 'required',
            'Data.*.Functions'  => 'array',
            'Name'              => 'required|max:50',
            'Description'       => 'required|max:150'
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // ok add new now
        $user_profile = UserProfile::createUserProfile($dataValid['Data']['Name'], $dataValid['Data']['Description'], $this->userObj);

        // process user profile module
        foreach ($dataValid['Data']['Data'] as $module) {
            $profile_module = UserProfileModule::createProfileModule($user_profile->ID, $module['ID'], stringToBoolean($module['Checked']));

            // then we insert functions of this modules
            if (isset($module['Functions']) && is_array($module['Functions'])) {
                foreach ($module['Functions'] as $func) {
                    UserProfileFunction::createProfileFunction($profile_module->ID, $func['ID'], stringToBoolean($func['Checked']));
                }
            }
        }

        return response()->json(['success' => __('system.action_success')]);
    }

    public function getAllModule()
    {
        // get all modules that this company can access
        return response()->json(CompanyModule::getCompanyModule($this->companyID));
    }

    /**
     * Edit u_p
     * @param $id
     */
    public function edit($id)
    {
        $id = decode_id($id);
        $profile = UserProfile::find($id);
        if ($profile == null) {
            return redirect()->back()->with('error', __('system.not_found_information'));
        }

        // get old list
        $modules_id = $profile->UserProfileModule->pluck('ModuleID')->toArray();
        $company_module = CompanyModule::getAllCompanyModule($this->companyID)->pluck('ModuleID')->all();

        // more module or less module
        $isMoreModule = false;
        if (count($company_module) > count($modules_id)) {
            $isMoreModule = true;
        }

        // get id that isnt in this profile et
        $moduleNotExist = array_diff($modules_id, $company_module);

        // add new module to this profile obviously
        if (count($moduleNotExist) > 0) {
            if ($isMoreModule) {
                $this->processMoreModule($profile->ID, $moduleNotExist);
            } else {
                $this->processLessModule($profile->ID, $moduleNotExist);
            }
        }

        // reload eloquent
        $profile->load('UserProfileModule');

        // ok we retrieve data here and back to view
        $this->data['UserProfile'] = $profile;
        $this->data['CompanyModules'] = $this->editDataPostProcessing($profile->UserProfileModule);

        // title + js
        $this->setPageTitle(__("system.EditUserProfile"));
        $this->addJs(asset('js/modules/user_profile/edit.js'));

        return view('bedone.user_profile.edit', $this->data);
    }

    private function processMoreModule($profile_id, $moduleIDs)
    {
        foreach ($moduleIDs as $moduleID) {
            $moduleInfo = Module::find($moduleID);

            // add module
            $profile_module = UserProfileModule::createProfileModule($profile_id, $moduleID, true);

            // add function
            foreach ($moduleInfo->Functions as $func) {
                UserProfileFunction::createProfileFunction($profile_module->ID, $func->ID, true);
            }
        }
    }

    private function processLessModule($profile_id, $moduleIDs)
    {
        foreach ($moduleIDs as $moduleID) {
            $profile_module = UserProfileModule::where('UserProfileID', $profile_id)
                ->where('ModuleID', $moduleID)->first();

            if ($profile_module == null)
                continue;

            // remove function
            foreach ($profile_module->Functions as $func) {
                $func->delete();
            }

            // remove module
            $profile_module->delete();
        }
    }

    private function editDataPostProcessing($up_modules)
    {
        $data = [];
        foreach ($up_modules as $module) {
            $row = [];
            $row['id'] = $module->ModuleID;
            $row['text'] = $module->Module->Name;
            $row['parent'] = $module->Module->ParentID == null ? "#" : $module->Module->ParentID;
            $row['state'] = [
                'selected' => UserProfileModule::HaveChildrenOff($module->UserProfileID, $module->ModuleID) ? false : $module->Status == "on",
            ];

            // function data
            $row['data'] = [
                '_id'   => $module->ID,
                'functions' => [],
            ];
            foreach ($module->Functions as $func) {
                $row['data']['functions'][] = [
                    'ID' => $func->ModuleFunctionID,
                    'Name'  => $func->ModuleFunction->Name,
                    'Checked' => $func->Status == "on",
                    '_id' => $func->ID,
                ];
            }

            $data[] = $row;
        }

        return $data;
    }

    /**
     * Editing...
     * @param $id
     * @param Request $rq
     */
    public function editing($id, Request $rq)
    {
        $id = decode_id($id);
        $profile = UserProfile::find($id);
        if ($profile == null) {
            return redirect()->back()->with('error', __('system.not_found_information'));
        }

        $rules = [
            'Data'              => 'required|array',
            'Data.*.ID'         => 'required|integer',
            'Data.*._id'        => 'required|integer',
            'Data.*.Checked'    => 'required',
            'Data.*.Functions'  => 'array',
            'Name'              => 'required|max:50',
            'Description'       => 'required|max:150'
        ];

        $dataValid = $this->runValidator($rules, $rq);
        if ($dataValid['Valid'] == false) {
            return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
        }

        // ok now we editing...
        $profile->Name = $dataValid['Data']['Name'];
        $profile->Description = $dataValid['Data']['Description'];
        setObjLoggedInfo($profile, $this->userObj);
        $profile->save();

        // edit modules
        foreach($dataValid['Data']['Data'] as $module) {
            $objModule = UserProfileModule::find($module['_id']);
            if ($objModule == null)
                continue;

            // ok we edit the checked
            $objModule->Status = stringToBoolean($module['Checked']) === true ? "on" : "off";
            $objModule->save();

            // now to the functions
            if (isset($module['Functions']) && is_array($module['Functions'])) {
                foreach ($module['Functions'] as $func) {
                    $up_func = UserProfileFunction::find($func['_id']);
                    if ($up_func == null)
                        continue;

                    $up_func->Status = stringToBoolean($func['Checked']) === true ? "on" : "off";
                    $up_func->save();
                }
            }
        }

        return response()->json(['success' => __('system.action_success')]);
    }

    /**
     * View user profile
     * @param $id
     */
    public function view($id)
    {
        $id = decode_id($id);
        $profile = UserProfile::find($id);
        if ($profile == null) {
            return redirect()->back()->with('error', __('system.not_found_information'));
        }

        // ok we retrieve data here and back to view
        $this->data['UserProfile'] = $profile;
        $this->data['CompanyModules'] = $this->editDataPostProcessing($profile->UserProfileModule);

        // title + js
        $this->setPageTitle(__("system.ViewUserProfile"));
        $this->addJs(asset('js/modules/user_profile/view.js'));

        return view('bedone.user_profile.view', $this->data);
    }

    /**
     * Delete user_profile
     */
    public function deleteProfile($id)
    {
        $id = decode_id($id);
        $profile = UserProfile::find($id);
        if ($profile == null) {
            return redirect()->back()->with('error', __('system.not_found_information'));
        }

        if ($profile->UserProfileAccess->count() > 0) {
            return redirect()->back()->with('error', __('system.user_depend_on_this_profile'));
        }

        // available to delete
        $profile->delete();

        return redirect()->back()->with('success', __('system.action_success'));

    }

    /**
     * Get user using this profile
     * @param $profile_id
     */
    public function getUserUsingProfile($id) {
        $id = decode_id($id);
        $profile = UserProfile::find($id);
        if ($profile == null) {
            return response()->json([]);
        }

        return response()->json(UserProfileAccess::userByProfile($id));
    }
}
