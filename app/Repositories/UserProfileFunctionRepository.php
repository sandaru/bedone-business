<?php
/**
 * Created by PhpStorm.
 * User: 84066
 * Date: 5/18/2018
 * Time: 10:58 AM
 */

namespace App\Repositories;

use App\Models\Module;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserProfileModule;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserProfileFunctionRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return "App\Models\UserProfileFunction";
    }


    /**
     * set default module function (new register)
     * @param $profile_module
     * @return array of IDs
     */
    public function setDefaultModuleFunction($profile_module)
    {
        $default_modules = config('bedone.MODULE_ALLOWANCE');

        $inserted_id = [];
        foreach ($default_modules as $index => $module_id)
        {
            // get all module function
            $functions = Module::getModuleFunction($module_id);
            foreach ($functions as $function)
            {
                $profileModule = $this->create(['UserProfileModuleID' => $profile_module[$index], 'ModuleFunctionID' => $function->ID, 'Status' => 'on']);
                $inserted_id[] = $profileModule->ID;
            }
        }

        return $inserted_id;
    }
}