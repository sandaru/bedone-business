<?php
/**
 * Created by PhpStorm.
 * User: 84066
 * Date: 5/18/2018
 * Time: 10:58 AM
 */

namespace App\Repositories;

use App\Models\User;
use App\Models\UserProfile;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserProfileRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return "App\Models\UserProfile";
    }


    /**
     * Create default user profile
     * @param $company_id
     * @param $userObj
     */
    public function createDefaultProfile($company_id, $userObj)
    {
        // create a default profile
        $profile = new UserProfile;
        $profile->CompanyID = $company_id;
        $profile->Name = __('system.MasterAdmin');
        $profile->Description = __('system.MasterAdmin');
        $profile->IsMasterAdmin = 'yes'; // always is master admin for the 1st profile of company
        $profile->CreatedByID = $profile->ModifiedByID = $userObj->ID;
        $profile->CreatedByName = $profile->ModifiedByName = $userObj->full_name;
        $profile->CreatedDate = $profile->ModifiedDate = sql_date();
        $profile->save();

        return $profile;
    }
}