<?php
/**
 * Created by PhpStorm.
 * User: 84066
 * Date: 5/18/2018
 * Time: 10:58 AM
 */

namespace App\Repositories;

use App\Models\Company;
use App\Models\User;
use App\Models\UserProfileAccess;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

/**
 * Class CompanyRepository
 * @package App\Repositories
 */
class CompanyRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return "App\Models\Company";
    }


    /**
     * Get all company
     * @return array
     */
    public function getAllCompany()
    {
        return $this->all(['ID', 'Name']);
    }

    /**
     * Create a new company
     * @param $name string
     * @param $userObj User
     */
    public function createNewCompany($name, $userObj)
    {
        $data = [
            'Name' => $name,
            'UserAdminID' => $userObj->ID,
            'CreatedByID' => $userObj->ID,
            'CreatedByName' => $userObj->full_name,
            'CreatedDate' => sql_date(),
            'ModifiedDate' => sql_date(),
            'ModifiedByName' => $userObj->full_name,
            'ModifiedByID' => $userObj->ID,
        ];

        $company = $this->create($data);

        return $company->ID;
    }

    /**
     * Get user administrator of this company
     * @return array of Users
     */
    public function getAdmins($companyObj)
    {
        try {
            $user_profile = $companyObj->UserAdmin->UserProfileAccess->UserProfile;

            // find all user with this profile
            $allUsers = UserProfileAccess::where('UserProfileID', $user_profile->ID)->get();

            // return all of these user
            $users = [];
            foreach ($allUsers as $item)
                $users[] = $item->User;

            return $users;
        }
        catch (\Exception $e)
        {
            Log::debug($e->getMessage());
            return [];
        }
    }
}