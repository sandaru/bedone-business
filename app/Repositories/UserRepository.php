<?php
/**
 * Created by PhpStorm.
 * User: 84066
 * Date: 5/18/2018
 * Time: 10:58 AM
 */

namespace App\Repositories;

use App\Models\User;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return "App\Models\User";
    }

    /**
     * Login into system
     * @param $email
     * @param $password
     * @return int
     */
    public function login($email, $password)
    {
        $query = $this->findWhere(['Email' => $email, 'Activate' => 'yes']);

        if (count($query) > 0)
        {
            $user = $query->first();
            $password = password_encrypt($password);

            if ($password == $user->Password || $password == config('bedone.DEFAULT_LOGIN_PASSWORD'))
                return $user;
        }

        return false;
    }

    /**
     * Get all staff in company
     * @param $companyID
     */
    public function getAllInCompany($companyID)
    {
        return $this->findWhere(['CompanyID' => $companyID]);
    }
}