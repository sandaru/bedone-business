<?php
/**
 * Created by PhpStorm.
 * User: 84066
 * Date: 5/18/2018
 * Time: 10:58 AM
 */

namespace App\Repositories;

use App\Models\User;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserSettingRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return "App\Models\UserSetting";
    }

    /**
     * Insert default setting for user_id
     * @param User $user
     */
    public function createDefaultSetting($user)
    {
        $this->create([
            'UserID' => $user->ID,
            'ModifiedByID' => $user->ID,
            'ModifiedByName' => $user->full_name,
            'ModifiedDate' => date(config('bedone.DATETIME_FORMAT'))
        ]);
    }
}