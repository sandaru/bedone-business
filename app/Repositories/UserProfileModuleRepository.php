<?php
/**
 * Created by PhpStorm.
 * User: 84066
 * Date: 5/18/2018
 * Time: 10:58 AM
 */

namespace App\Repositories;

use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserProfileModule;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserProfileModuleRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return "App\Models\UserProfileModule";
    }


    /**
     * set default mdoule (new register)
     * @param $user_profile_id
     * @return array of IDs
     */
    public function setDefaultModule($user_profile_id)
    {
        $default_modules = config('bedone.MODULE_ALLOWANCE');

        $inserted_id = [];
        foreach ($default_modules as $module_id)
        {
            $profileModule = $this->create(['UserProfileID' => $user_profile_id, 'ModuleID' => $module_id, 'Status' => 'on']);
            $inserted_id[] = $profileModule->ID;
        }

        return $inserted_id;
    }
}