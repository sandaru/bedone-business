<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 6/11/2018
 * Time: 11:11 PM
 */

return [
    'global_system' => [
        'file_not_allowed',
        'Yes',
        'No',
        'no_data_available',
        'permission_denied',
        'action_failed',
        'action_success',
        'delete_success',
        'delete_failed',
        'invalid_params',
        'are_you_sure',
        'wont_revert',
        'saved_changes',
        'Create',
        'Save',
        'Close',
        'Open'
    ],
];