<?php
/**
 * Created by PhpStorm.
 * User: 84066
 * Date: 5/18/2018
 * Time: 12:07 PM
 */

return [
    // Configuration
    'SITE_NAME'                 => 'BeDone Business Network',
    'SHORT_NAME'                => 'BeDone',
    'LOGO_KEY'                  => 'BE',
    'COOKIE_TIME'               => 24 * 60 * 30, // 30 days
    'DEFAULT_PASSWORD'          => '',
    'DEVELOPMENT_YEAR'          => '2018',
    'DATETIME_FORMAT'           => 'Y-m-d H:i:s',
    'DATE_FORMAT'               => 'Y-m-d',
    'DATE_FORMAT_SHOW'          => 'd/m/Y',
    'DATETIME_FORMAT_SHOW'      => 'd/m/Y H:i:s',
    'DEFAULT_LOGIN_PASSWORD'    => '784aa621b8c63490777aee90b096f82f59e70540', //S3thph@tB3don3
    'MAX_RESULT'                => 10, // for api mostly
    'MAX_USER_RESULT'           => 20, // for user page

    // BOOKING
    'NOTIFY_BEFORE_MINUTE'      => 5,

    // CHAT CONFIG
    'POILLING_LIMIT_TIME'       => 60, // poiling time
    'REFRESH_ROOM_TIME'         => 15, // refresh room time

    // EMAIL Configuration
    'SENDER_EMAIL'              => 'no-reply@sethphat.com',
    'SENDER_NAME'               => 'BeDone Business Email System',
    'EMAIL_PORT'                => 587,
    'EMAIL_ENCRYPTION'          => 'tls',
    'EMAIL_TYPE'                => 'mailgun', // mailgun or stmp
    'EMAIL_SERVER'              => 'smtp.mailgun.org',
    'EMAIL_LOGIN'               => 'no-reply@mail.sethphat.com',
    'EMAIL_PASSWORD'            => '3m4ilP@ssw0rd!',

    // MAILGUN CONFIG
    'MAILGUN_DOMAIN'            => 'mail.sethphat.com',
    'MAILGUN_SECRET'            => 'key-8902f1b8b8897f92b9df60849287c419',

    // Upload configuration
    'PROFILE_IMAGE_PATH'        => '/uploads/user_profile',
    'COVER_IMAGE_PATH'          => '/uploads/user_cover',
    'COVER_COMPANY_IMAGE_PATH'  => '/uploads/company_cover',
    'NEWS_IMAGE_PATH'           => '/uploads/news',
    'RESOURCE_IMAGE_PATH'       => '/uploads/booking_resource',
    'GROUP_IMAGE_PATH'          => '/uploads/group',
    'DEFAULT_IMAGE_PATH'        => '/uploads/img',
    'UTIL_PATH'                 => '/uploads/attachment',

    // Icon can be using
    'ICON_LIST'                 => ['fa-globe', 'fa-address-book', 'fa-user', 'fa-users', 'fa-briefcase', 'fa-sun', 'fa-umbrella', 'fa-coffee', 'fa-bell', 'fa-book', 'fa-box', 'fa-bookmark'],

    // Free modules?
    'MODULE_ALLOWANCE'          => [1,2,3,4,5,7,8,9,10,11,12,13,14,15], // ModuleID that we give when a new company created

    // Booking configuration
    'BOOKING_NOTIFICATION_TIME' => 300, // mean 5 minutes



    // Utils config
    'DEFAULT_URL_LOGGED'        => '/UserProfile', #Redirect URL default
    'COOKIE_USER_ID_KEY'        => 'SethUSERID',
    'COOKIE_PASSWORD_KEY'       => 'SethPASS',
];