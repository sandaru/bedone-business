<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * BIG FREAKING NOTE
 * All the route name must be as CAMEL CASE
 * With the route only to show page (which return view), name it as: {name}Page (Example: loginPage)
 * With the route that receive action (POST, PUT,...), name it as: {name}Action (Example: loginAction)
 */

Route::get('/', function () {
    return redirect(config('bedone.DEFAULT_URL_LOGGED'));
});


// Authentication Routing
Route::get('/login', 'AuthenticationController@login')->name('loginPage');
Route::post('/doLogin', 'AuthenticationController@logging')->name('loginAction');
Route::get('/register', 'AuthenticationController@register')->name('registerPage');
Route::post('/doRegister', 'AuthenticationController@registering')->name('registerAction');
Route::get('/forgot', 'AuthenticationController@forgot')->name('forgotPage');
Route::post('/doForgot', 'AuthenticationController@forgotting')->name('forgotAction');
Route::get('/logout', 'UserController@logout')->name('logoutAction');
Route::get('/forgot/{key}', 'AuthenticationController@retrievePass')->name('userRetrievePassPage');
Route::post('/forgot_password/{key}', 'AuthenticationController@doRetrievePass')->name('userRetrievePassAction');

/**
 * User Profile Routing
 */
Route::get('/UserProfile/{id?}', 'UserController@UserProfile')->name('userProfilePage');
Route::get('/EditProfile', 'UserController@EditProfile')->name('editProfilePage');
Route::post('/EditProfile', 'UserController@EditingProfile')->name('editProfileAction');
Route::get('/Company/ResendRequest', 'UserController@ResendRequest')->name('resendRequestAction');
Route::get('/Company/CancelRequest', 'UserController@CancelRequest')->name('cancelRequestAction');

/**
 * User Setting
 */
Route::get('/Setting', 'UserController@setting')->name('settingPage');
Route::post('/Setting/ChangePassword', 'UserController@changePassword')->name('changePasswordAction');
Route::post('/Setting/ChangePrivacy', 'UserController@changeSetting')->name('changePrivacyAction');

/**
 * Company Info
 */
Route::get('/Company', 'CompanyModule\CompanyController@ViewCompany')->name('companyPage');
Route::get('/Company/Edit', 'CompanyModule\CompanyController@EditCompany')->name('editCompanyPage');
Route::post('/Company/Edit', 'CompanyModule\CompanyController@EditingCompany')->name('editCompanyAction');
Route::get('/Company/Leave', 'CompanyModule\CompanyController@leaving_company')->name('leaveCompanyAction');

/**
 * News Maintenance
 */
Route::get('/NewsMaintenance', 'NewsModule\NewsCategoryController@index')->name('newsMaintenancePage');
Route::get('/NewsMaintenance/Edit/{id}', 'NewsModule\NewsCategoryController@edit')->name('editNewsMaintenancePage');
Route::post('/NewsMaintenance/Edit/{id}', 'NewsModule\NewsCategoryController@editing')->name('editNewsMaintenanceAction');
Route::get('/NewsMaintenance/Add', 'NewsModule\NewsCategoryController@add')->name('addNewsMaintenancePage');
Route::post('/NewsMaintenance/Add', 'NewsModule\NewsCategoryController@adding')->name('addNewsMaintenanceAction');
Route::get('/NewsMaintenance/Delete/{id}', 'NewsModule\NewsCategoryController@DeleteCategory')->name('deleteNewsMaintenanceAction');

/**
 * New page
 */
Route::get('/News', 'NewsModule\NewsFeedController@index')->name('newsPage');
Route::get('/News/{id}', 'NewsModule\NewsFeedController@viewNews')->name('viewNewsPage');

/**
 * All staffs
 */
Route::get('/AllStaffs', 'MyNetworkModule\AllStaffController@index')->name('allStaffPage');

/**
 * Group
 */
Route::get('/Groups', 'MyNetworkModule\GroupController@index')->name('groupPage');
Route::get('/Groups/Info/{group_id}', 'MyNetworkModule\GroupController@view')->name('groupInfoPage');
Route::get('/Groups/RemoveMember/{group_id}/{user_id}', 'MyNetworkModule\GroupController@removeMember')->name('removeGroupMemberAction');
Route::post("/Groups/Edit/{group_id}", "MyNetworkModule\GroupController@edit")->name('editGroupAction');
Route::get("/Groups/Leave/{group_id}", "MyNetworkModule\GroupController@leave_group")->name('leaveGroupAction');
Route::post("/Groups/AddMember/{group_id}", "MyNetworkModule\GroupController@add_more_people")->name('addMemberGroupAction');
Route::get("/Groups/Delete/{group_id}", "MyNetworkModule\GroupController@delete_group")->name('deleteGroupAction');

/**
 * User maintenance
 */
Route::get('/UserMaintenance', 'CompanyAdminModule\UserMaintainanceController@index')->name('userMaintenancePage');
Route::get('/UserMaintenance/Remove/{id}', 'CompanyAdminModule\UserMaintainanceController@removeUser')->name('removeUserMaintenanceAction');
Route::get('/UserMaintenance/Edit/{id}', 'CompanyAdminModule\UserMaintainanceController@edit')->name('editUserMaintenancePage');
Route::post('/UserMaintenance/Edit/{id}', 'CompanyAdminModule\UserMaintainanceController@editing')->name('editUserMaintenanceAction');

/**
 * Company Join Request Page
 */
Route::get('/CompanyJoinRequest', 'CompanyAdminModule\CompanyJoinRequestController@index')->name('companyJoinRequestPage');
Route::get('/CompanyJoinRequest/Reject/{id}', 'CompanyAdminModule\CompanyJoinRequestController@DeclineRequest')->name('joinRejectAction');
Route::get('/CompanyJoinRequest/Approve/{id}', 'CompanyAdminModule\CompanyJoinRequestController@approvePage')->name('joinApprovePage');
Route::post('/CompanyJoinRequest/Approving/{id}', 'CompanyAdminModule\CompanyJoinRequestController@Approving')->name('joinApproveAction');

/**
 * User profile maintenance
 */
Route::get('/ProfileMaintenance', 'UserProfileModule\UserProfileController@index')->name('profileMaintenancePage');
Route::get('/ProfileMaintenance/Add', 'UserProfileModule\UserProfileController@add')->name('addProfileMaintenancePage');
Route::get('/ProfileMaintenance/Edit/{id}', 'UserProfileModule\UserProfileController@edit')->name('editProfileMaintenancePage');
Route::get('/ProfileMaintenance/View/{id}', 'UserProfileModule\UserProfileController@view')->name('viewProfileMaintenancePage');
Route::get('/ProfileMaintenance/Del/{id}', 'UserProfileModule\UserProfileController@deleteProfile')->name('deleteProfileMaintenanceAction');

/*
 * Booking
 */
Route::get('/Booking', 'BookingModule\BookingController@index')->name('bookingPage');
Route::get('/Booking/ViewResource/{id}', 'BookingModule\BookingController@viewResource')->name('bookingViewResourcePage');

/**
 * Booking Resource
 */
Route::get('/BookingResource', 'BookingModule\BookingResourceController@index')->name('bookingResourcePage');
Route::get('/BookingResource/Add', 'BookingModule\BookingResourceController@add')->name('addBookingResourcePage');
Route::get('/BookingResource/Edit/{id}', 'BookingModule\BookingResourceController@edit')->name('editBookingResourcePage');
Route::get('/BookingResource/Delete/{id}', 'BookingModule\BookingResourceController@delete')->name('deleteBookingResourceAction');

/**
 * Chat
 */
Route::get('/Chat', 'ChatModule\ChatController@index')->name('chatPage');