<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Company API
 */
Route::post('/company/create', 'CompanyModule\CompanyApiController@createCompany');
Route::post('/company/join', 'CompanyModule\CompanyApiController@joinCompany');

/**
 * News API
 */
Route::get('/news/get_data', 'NewsModule\NewsFeedController@get_data');
Route::get('/news/delete/{id}', 'NewsModule\NewsFeedController@deleteNews');
Route::post('/news/add', 'NewsModule\NewsFeedController@addNews');
Route::post('/news/edit/{id}', 'NewsModule\NewsFeedController@editNews');

/**
 * News comment API
 */
Route::get('/news/get_comment/{newsid}', 'NewsModule\NewsCommentApiController@GetComment');
Route::post('/news/comment/add/{newsid}', 'NewsModule\NewsCommentApiController@CreateComment');
Route::post('/news/comment/del/{cmtid}', 'NewsModule\NewsCommentApiController@DeleteComment');
Route::post('/news/comment/edit/{cmtid}', 'NewsModule\NewsCommentApiController@EditComment');

/**
 * All staff api
 */
Route::get('/staff/all', 'MyNetworkModule\AllStaffController@getAllStaff');

/**
 * All group api
 */
Route::get('/group/all', 'MyNetworkModule\GroupController@getUserGroup');

/**
 * All module api
 */
Route::get('/userprofile/company_module', 'UserProfileModule\UserProfileController@getAllModule');
Route::post('/userprofile/add', 'UserProfileModule\UserProfileController@adding');
Route::post('/userprofile/edit/{id}', 'UserProfileModule\UserProfileController@editing');
Route::get('/userprofile/get_user/{id}', 'UserProfileModule\UserProfileController@getUserUsingProfile');

/**
 * Notification
 */
Route::get('/notification/seen_all', 'NotificationController@SeenNotification');
Route::get('/notification/get_more', 'NotificationController@GetNotification');

/**
 * Booking Resource API
 */
Route::post('/booking_resource/add', 'BookingModule\BookingResourceController@adding');
Route::post('/booking_resource/edit/{id}', 'BookingModule\BookingResourceController@editing');

/**
 * Booking API
 */
Route::get('/booking/month', 'BookingModule\BookingApiController@getMonthBooking');
Route::post('/booking/add', 'BookingModule\BookingApiController@createBooking');
Route::post('/booking/edit/{id}', 'BookingModule\BookingApiController@editBooking');
Route::get('/booking/get/{id}', 'BookingModule\BookingApiController@getBooking');
Route::get('/booking/cancel/{id}', 'BookingModule\BookingApiController@cancelBooking');

/**
 * Chat API
 */
Route::get('/chat/chat_room', 'ChatModule\ChatAPIController@getChatRoom');
Route::get('/chat/chat_room/{to_user_id}', 'ChatModule\ChatAPIController@chatOneOnOne');
Route::get('/chat/chat_room/mess/{room_id}', 'ChatModule\ChatAPIController@getMessage');
Route::get('/chat/chat_room/new_mess/{room_id}', 'ChatModule\ChatAPIController@getNewMessage');
Route::post('/chat/chat_room/chat/{room_id}', 'ChatModule\ChatAPIController@sendTextMessage');
Route::post('/chat/chat_room/chat_img/{room_id}', 'ChatModule\ChatAPIController@sendImageMessage');

Route::get('/chat/chat_room/group/{group_id}', 'ChatModule\ChatAPIController@chatGroup');
Route::post('/chat/chat_room/group', 'ChatModule\ChatAPIController@newGroup');
Route::get('/chat/chat_room/archive/{room_id}', 'ChatModule\ChatAPIController@archiveRoom');
Route::post('/chat/chat_room/group_member/{group_id}', 'ChatModule\ChatAPIController@addGroupMember');
Route::post('/chat/chat_room/group_image/{group_id}', 'ChatModule\ChatAPIController@updateGroupImage');
Route::post('/chat/chat_room/group_name/{group_id}', 'ChatModule\ChatAPIController@updateGroupName');
Route::post('/chat/chat_room/leave_group', 'ChatModule\ChatAPIController@leaveGroup');
Route::post('/chat/chat_room/group_member/remove/{group_id}', 'ChatModule\ChatAPIController@removeMember');