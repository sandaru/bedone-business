@extends('layout.bedone_template')

@section('body_content')
    <div class="row" id="partial" style="visibility: hidden;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-4 float-right">
                        <div class="input-group">
                            <input type="text" class="form-control" v-model="keyword"
                                   placeholder="{{__('system.Search_by_allstaff')}}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">{{__('system.Search')}}</button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 mb-3" v-for="(item, index) in list_staff"
                             v-if="(item.FullName.toLowerCase().indexOf(keyword.toLowerCase()) >= 0 ||
                                    item.JobTitle.toLowerCase().indexOf(keyword.toLowerCase()) >= 0)">
                            <div class="media border p-3">
                                <a :href="get_profile_url(item.ID)" target="_blank">
                                    <img :src="get_profile_image(item.ProfileImage)" :alt="item.FullName"
                                         class="mr-3 mt-3 rounded-circle" style="width:120px; height: 120px; object-fit: cover;">
                                </a>
                                <div class="media-body">
                                    <a :href="get_profile_url(item.ID)" target="_blank">
                                        <h4>@{{item.FullName}}</h4>
                                    </a>
                                    <p>@{{ item.JobTitle }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-if="list_staff.length <= 0">
                        <p class="text-center">{{__('system.no_staff_available')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var langText = {
            action_failed: "{{__('system.action_failed')}}",
        };
    </script>
@endsection