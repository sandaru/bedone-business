@extends('layout.bedone_template')

@section('body_content')

    <form action="{{route('editProfileAction')}}" method="post" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="file" id="fileProfile" accept="image" style="display: none;">
        <input type="file" id="fileFeature" style="display: none;">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body text-right">
                        <button class="btn btn-primary">{{__('system.Save')}}</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header no-padding">
                        <div class="card-image">
                            <img src="{{feature_image($User->FeatureImage)}}" alt="{{$User->full_name}}" id="imgCover">

                            <button class="btn btn-info" style="position: absolute; bottom: 10px; right: 10px;"
                                    type="button" id="btnChangeCover">
                                {{__('system.CoverPhoto')}}
                            </button>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="author">
                                <img class="avatar border-gray clickable" src="{{profile_picture($User->ProfileImage)}}" id="imgProfilePic"
                                     alt="{{$User->full_name}}" data-toggle="tooltip" title="{{__('system.EditProfilePic')}}">

                                <h5 class="card-title"><strong>{{$User->full_name}}</strong></h5>
                            <p class="card-description">
                                {{$User->JobTitle}}
                            </p>
                        </div>

                        <div class="col-md-6" style="padding: 0;">
                            <div class="form-group" style=" position: relative;">
                                <label>{{__('system.Introduce')}}</label>
                                <input type="text" class="form-control input-char-count"
                                       placeholder="{{__('system.Introduce')}}"
                                       name="Introduction" value="{{$User->Introduction }}"
                                       maxlength="150">
                            </div>
                        </div>

                        <hr/>

                        <div style="padding-top: 10px">
                            <h5>{{__('module.Personal_Info')}}</h5>

                            <div class="form-group">
                                <label>{{__('system.FirstName')}}</label>
                                <input type="text" class="form-control col-md-6"
                                       placeholder="{{__('system.FirstName')}}" name="FirstName"
                                       value="{{$User->FirstName}}">
                            </div>

                            <div class="form-group">
                                <label>{{__('system.LastName')}}</label>
                                <input type="text" class="form-control col-md-6" placeholder="{{__('system.LastName')}}"
                                       name="LastName" value="{{$User->LastName}}">
                            </div>

                            <div class="form-group">
                                <label>{{__('system.Gender')}}</label>
                                <br>
                                <select name="Gender" id="selectGender" class="col-md-6">
                                    <option value="">{{__('system.NotSelectedYet')}}</option>
                                    <option value="F" {{$User->Gender === "F" ? "selected" : ""}}>{{__('system.Female')}}</option>
                                    <option value="M" {{$User->Gender === "M" ? "selected" : ""}}>{{__('system.Male')}}</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label>{{__('system.BirthDay')}}</label>

                                <div class="input-group col-md-6" style="padding: 0px;">
                                    <input type="text" id="txtDate"
                                           class="form-control datetimepicker"
                                           name="Birthday"
                                           placeholder="{{__('system.Birthday')}}"
                                           value="{{$User->Birthday}}">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span>
                                </div>
                            </div>
                        </div>

                        <hr/>

                        <div style="padding-top: 10px">
                            <h5>{{__('module.Work_Info')}}</h5>

                            <div class="form-group">
                                <label>{{__('module.Company')}}</label>
                                <input type="text" class="form-control col-md-6"
                                       placeholder="{{__('system.Company')}}"
                                       value="{{!empty($User->CompanyID) ? $User->Company->Name : __('system.NoCompany')}}"
                                       readonly>
                            </div>

                            <div class="form-group">
                                <label>{{__('system.JobTitle')}}</label>
                                <input type="text" class="form-control col-md-6"
                                       name="JobTitle" placeholder="{{__('system.JobTitle')}}"
                                       value="{{empty(old('JobTitle')) ? $User->JobTitle : old('JobTitle')}}">
                            </div>

                            <div class="form-group">
                                <label>{{__('system.WorkAddress')}}</label>
                                <input type="text" class="form-control col-md-6"
                                       name="WorkAddress" placeholder="{{__('system.WorkAddress')}}"
                                       value="{{empty(old('WorkAddress')) ? $User->WorkAddress : old('WorkAddress')}}">
                            </div>
                        </div>

                        <hr/>

                        <div style="padding-top: 10px">
                            <h5>{{__('module.Contact_Info')}}</h5>

                            <div>
                                <label>{{__('system.Email')}}</label>
                                <p class="line_below">{{$User->Email}}</p>
                            </div>

                            <div class="form-group">
                                <label>{{__('system.WorkPhone')}}</label>
                                <input type="text" class="form-control col-md-6"
                                       name="WorkPhone" placeholder="{{__('system.WorkPhone')}}"
                                       value="{{empty(old('WorkPhone')) ? $User->WorkPhone : old('WorkPhone')}}">
                            </div>

                            <div class="form-group">
                                <label>{{__('system.HomePhone')}}</label>
                                <input type="text" class="form-control col-md-6"
                                       name="HomePhone" placeholder="{{__('system.HomePhone')}}"
                                       value="{{empty(old('HomePhone')) ? $User->HomePhone : old('HomePhone')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script>
        var allowed_ext = {!! json_encode(config('bedone.IMAGE_ALLOWED_EXT')) !!};
        var langText = {
            file_not_allowed: "{{__('system.file_not_allowed')}}",
        };
    </script>
@endsection