@extends('layout.bedone_template')

@section('body_content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{route('changePrivacyAction')}}" method="post">
                {!! csrf_field() !!}
                <div class="card">
                    <h5 class="card-header bg-primary" style="padding: 20px; color:#fff;">
                        {{__('system.InformationPrivacy')}}
                    </h5>

                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>{{__('system.Field')}}</th>
                                    <th class="text-center" width="20%">{{__('system.Access')}}</th>
                                    <th class="text-center" width="20%" style="display: table-cell">{{__('system.NoAccess')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($Field as $key)
                                <tr>
                                    <td>{{__('system.'.$key)}}</td>
                                    <td class="text-center">
                                        <input type="radio" name="{{$key}}" value="yes" @if($Setting->$key === "yes") checked @endif>
                                    </td>
                                    <td class="text-center" style="display: table-cell">
                                        <input type="radio" name="{{$key}}" value="no" @if($Setting->$key === "no") checked @endif>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer text-right">
                        <button class="btn btn-primary">{{__('system.ChangePrivacy')}}</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <form action="{{route('changePasswordAction')}}" method="post">
                {!! csrf_field() !!}
                <div class="card">
                    <h5 class="card-header bg-primary" style="padding: 20px; color:#fff;">
                        {{__('system.ChangePassword')}}
                    </h5>

                    <div class="card-body">
                        <div class="form-group">
                            <label>{{__('system.OldPassword')}}</label>
                            <input type="password" class="form-control" name="OldPassword">
                        </div>
                        <div class="form-group">
                            <label>{{__('system.NewPassword')}}</label>
                            <input type="password" class="form-control" name="NewPassword">
                        </div>
                        <div class="form-group">
                            <label>{{__('system.RetypeNewPassword')}}</label>
                            <input type="password" class="form-control" name="RetypeNewPassword">
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        <button class="btn btn-primary">{{__('system.ChangePassword')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection