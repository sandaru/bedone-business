@extends('layout.bedone_template')

@section('body_content')
    @if ($isCurrentUser)
    <div class="row">
        <div class="col-md-12">

            @if (empty($userObj->CompanyJoinRequest) && empty($userObj->CompanyID))
            <div class="alert alert-info">
                <div class="text-center">
                    <p>{{__('system.no_company_profile_yet')}}</p>

                    <div style="padding-bottom: 10px;">
                        <button class="btn btn-success" data-toggle="modal" data-target="#createCompanyModal">{{__('system.create_a_company')}}</button>
                        <button class="btn btn-success" data-toggle="modal" data-target="#joinCompanyModal">{{__('system.join_a_company')}}</button>
                    </div>

                    <button class="btn btn-warning" href="javascript:void(0);" onclick="UserProfileAction.viewBenefit()">
                        {{__('system.benefit_when_join')}}
                    </button>
                </div>
            </div>
            @elseif (!empty($userObj->CompanyJoinRequest) && $userObj->CompanyJoinRequest->Status == 'pending')
                <div class="alert alert-info">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                        &times;
                    </button>

                    <div class="text-center">
                        <p>{!! __('system.pending_join_company', ['company' => $User->CompanyJoinRequest->Company->Name])!!}</p>

                        <a class="btn btn-warning" href="{{route('resendRequestAction')}}">
                            {{__('system.RePushRequest')}}
                        </a>
                        <button class="btn btn-danger" onclick="UserProfileAction.confirmCancel()">
                            {{__('system.CancelRequest')}}
                        </button>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card card-user">
                <div class="card-header no-padding">
                    <div class="card-image">
                        <img src="{{feature_image($User->FeatureImage)}}" alt="{{$User->full_name}}">
                    </div>
                </div>
                <div class="card-body ">
                    <div class="author">
                        <a href="#">
                            <img class="avatar border-gray" src="{{profile_picture($User->ProfileImage)}}" alt="{{$User->full_name}}">
                            <h5 class="card-title"><strong>{{$User->full_name}}</strong></h5>
                        </a>
                        <p class="card-description">
                            {{$User->JobTitle}}
                        </p>
                    </div>
                    <p class="card-description text-center">
                        {!! nl2br($User->Introduction) !!}
                    </p>

                    <hr />

                    <div style="padding-top: 10px">
                        <h5>{{__('module.Personal_Info')}}</h5>

                        <div>
                            <label>{{__('system.FirstName')}}</label>
                            <p class="line_below">{{$User->FirstName}}</p>
                        </div>

                        <div>
                            <label>{{__('system.LastName')}}</label>
                            <p class="line_below">{{$User->LastName}}</p>
                        </div>

                        @if ($isCurrentUser || $User->UserSetting->Gender == 'yes')
                        <div>
                            <label>{{__('system.Gender')}}</label>
                            <p class="line_below">{{(empty($User->Gender) ? __('system.NotUpdateYet') : ($User->Gender == "F" ? __("system.Female") : __("system.Male")))}}</p>
                        </div>
                        @endif

                        @if ($isCurrentUser || $User->UserSetting->Birthday == 'yes')
                            <div>
                                <label>{{__('system.BirthDay')}}</label>
                                <p class="line_below">{{empty($User->Birthday) ? __('system.NotUpdateYet') : ($User->Birthday)}}</p>
                            </div>
                        @endif
                    </div>

                    <hr />

                    <div style="padding-top: 10px">
                        <h5>{{__('module.Work_Info')}}</h5>

                        <div>
                            <label>{{__('module.Company')}}</label>
                            <p class="line_below">{{!empty($User->CompanyID) ? $User->Company->Name : __('system.NoCompany')}}</p>
                        </div>

                        <div>
                            <label>{{__('module.JobTitle')}}</label>
                            <p class="line_below">{{empty($User->JobTitle) ? __('system.NotUpdateYet') : $User->JobTitle}}</p>
                        </div>

                        @if ($isCurrentUser || $User->UserSetting->WorkAddress == 'yes')
                            <div>
                                <label>{{__('system.WorkAddress')}}</label>
                                <p class="line_below">{{empty($User->WorkAddress) ? __('system.NotUpdateYet') : $User->WorkAddress}}</p>
                            </div>
                        @endif
                    </div>

                    <hr />

                    <div style="padding-top: 10px">
                        <h5>{{__('module.Contact_Info')}}</h5>

                        @if ($isCurrentUser || $User->UserSetting->Email == 'yes')
                            <div>
                                <label>{{__('system.Email')}}</label>
                                <p class="line_below">{{$User->Email}}</p>
                            </div>
                        @endif

                        @if ($isCurrentUser || $User->UserSetting->WorkPhone == 'yes')
                            <div>
                                <label>{{__('system.WorkPhone')}}</label>
                                <p class="line_below">{{empty($User->WorkPhone) ? __('system.NotUpdateYet') : $User->WorkPhone}}</p>
                            </div>
                        @endif

                        @if ($isCurrentUser || $User->UserSetting->HomePhone == 'yes')
                            <div>
                                <label>{{__('system.HomePhone')}}</label>
                                <p class="line_below">{{empty($User->HomePhone) ? __('system.NotUpdateYet') : $User->HomePhone}}</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;">
        <p id="benefitText">{{__('system.Benefits')}}</p>

        <table id="benefitTable" style="width: 100%;">
            <tr>
                <td width="15%" class="text-left">
                    <i class="fa fa-building fa-2x"></i>
                </td>
                <td width="35%" class="text-left">
                    <h5>{{__('system.company_info')}}</h5>
                    <p>{{__('system.company_info_text')}}</p>
                </td>
                <td width="15%" class="text-left">
                    <i class="fa fa-users fa-2x"></i>
                </td>
                <td width="35%" class="text-left">
                    <h5>{{__('system.staff_info')}}</h5>
                    <p>{{__('system.staff_info_text')}}</p>
                </td>
            </tr>
            <tr>
                <td width="15%" class="text-left">
                    <i class="fa fa-calendar-alt fa-2x"></i>
                </td>
                <td width="35%" class="text-left">
                    <h5>{{__('system.booking_info')}}</h5>
                    <p>{{__('system.booking_info_text')}}</p>
                </td>
                <td width="15%" class="text-left">
                    <i class="fa fa-newspaper fa-2x"></i>
                </td>
                <td width="35%" class="text-left">
                    <h5>{{__('system.news_info')}}</h5>
                    <p>{{__('system.news_info_text')}}</p>
                </td>
            </tr>
        </table>
    </div>

    <!-- The Modal -->
    @if ($isCurrentUser && $userObj->CompanyID == null)
    <div class="modal" id="createCompanyModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">{{__('system.create_a_company')}}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tr>
                            <td width="40%">
                                {{__('system.CompanyName')}}
                            </td>
                            <td>
                                <input type="text" class="form-control" id="txtCompanyName" placeholder="{{__('system.CompanyName')}}">
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="UserProfileAction.createCompany()">{{__('system.Create')}}</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal" id="joinCompanyModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">{{__('system.join_a_company')}}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tr>
                            <td width="40%">
                                {{__('system.ChooseYourCompany')}}
                            </td>
                            <td>
                                <select id="companyList" placeholder="{{__('system.ChooseYourCompany')}}" class="form-control" style="width: 100%;">
                                    @foreach ($AllCompanies as $company)
                                        <option value="{{encode_id($company->ID)}}">{{$company->Name}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-primary" onclick="UserProfileAction.joinCompany()">{{__('system.Join')}}</button>
                </div>

            </div>
        </div>
    </div>
    @endif


    <script>
        var langText = {!! json_encode([
            'input_company_failed'  => __('system.input_company_failed'),
            'create_company_failed'  => __('system.create_company_failed'),
            'create_company_success'  => __('system.create_company_success'),
            'choose_company_failed'  => __('system.choose_company_failed'),
            'join_company_failed'  => __('system.join_company_failed'),
            'are_you_sure'  => __('system.are_you_sure'),
            'wont_revert'  => __('system.wont_revert'),
            'YesDelete'  => __('system.Yes'),
        ]) !!};
    </script>
@endsection