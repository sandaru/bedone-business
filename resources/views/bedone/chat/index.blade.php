@extends('layout.bedone_template')

@section('body_content')
    <div id="partial" style="visibility: hidden;">
        <div class="buddy-list">
            <div class="buddy-header">
                <div class="dropdown">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        {{__('system.StartAConversation')}}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="javascript:void(0);" @click="openOneOnOne">{{__('system.WithUser')}}</a>
                        <a class="dropdown-item" href="javascript:void(0);" @click="openGroupChat">{{__('system.WithGroup')}}</a>
                    </div>
                </div>
            </div>

            <div class="form-group" style="padding: 10px;">
                <input type="text" placeholder="{{__('system.Search')}}"
                       class="form-control" id="txtFilterText" @keyup.enter="doFilterRoom">
            </div>

            <div class="list-group">
                <chat-room v-for="(room, index) in roomInfo.chatRooms"
                           :key="room.ID"
                           :room="room"
                           :selected="isSelectedRoom(room)"
                           archive-text="{{__('system.ArchiveMess')}}"
                           delete-text="{{__('system.DeleteMess')}}"
                           leave-group-text="{{__('system.LeaveGroup')}}">
                </chat-room>
                <a class="list-group-item list-group-item-action"
                   href="javascript:void(0)"
                   @click="loadMoreRoom"
                   v-if="roomInfo.isEndRoom === false">
                    <p class="text-center">{{__('system.LoadMore')}}</p>
                </a>
            </div>

            <p class="text-center" v-if="roomInfo.chatRooms.length === 0">{{__('system.no_chat_room')}}</p>
        </div>

        <div class="main-container">

            <div class="chat-header">
                <div v-if="chatInfo.currentRoom !== null">
                    <div class="text-center">
                        <p style="margin-bottom: 0px; margin-top: 5px;"><b>@{{chatInfo.currentRoom.Name}}</b></p>
                        {{--<p>{{__('system.LastMessageFrom')}}@{{chatInfo.currentRoom.LastActivityDate}}</p>--}}
                    </div>
                    <div class="chat-header-right">
                        <i class="fa fa-info-circle fa-lg clickable" @click="chatInfo.isOpenSidebar = !chatInfo.isOpenSidebar"></i>
                    </div>
                </div>
            </div>


            <div class="chat-content">

                <div class="chat-container" v-if="chatInfo.currentRoom !== null">
                    <div class="chat-messages" id="chatMessageContainer">
                        <chat-message v-for="(message, index) in reversedMess"
                                      :key="message.ID"
                                      :message="message"
                                      :index="index">
                        </chat-message>
                    </div>

                    <div class="chat-input">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <a href="javascript:void(0);" @click="chooseIMG">
                                    {{--<i class="fa fa-paperclip"></i>--}}
                                    <i class="fa fa-image"></i>
                                </a>
                            </span>
                            <input type="text"
                                   v-model="chatInfo.textMessage"
                                   class="form-control"
                                   placeholder="{{__('system.your_chat_message_here')}}"
                                   @keyup.Enter="postChat">
                            <span class="input-group-addon">
                                <a href="javascript:void(0);" @click="postChat">
                                    <i class="fa fa-paper-plane"></i>
                                </a>
                            </span>
                        </div>
                        <input type="file" id="fileImgAtt" @change="doUploadChatImage" style="display: none">
                    </div>
                </div>
                <div class="chat-container" v-if="chatInfo.currentRoom === null">
                    <p class="middle-box">{{__('system.choose_a_chat')}}</p>
                </div>

                <div class="side-content" v-if="chatInfo.isOpenSidebar">
                    <sidebar-user :room="chatInfo.currentRoom" v-if="chatInfo.currentRoom.IsGroupChat === false"></sidebar-user>
                    <sidebar-group :room="chatInfo.currentRoom" :mainref="$refs" v-if="chatInfo.currentRoom.IsGroupChat === true"></sidebar-group>
                </div>

            </div>

        </div>

        <one-on-one ref="OneOnOneComponent"></one-on-one>
        <group-chat ref="GroupChatComponent"></group-chat>
        <add-people ref="AddPeopleComponent"></add-people>
    </div>

    <input type="hidden" id="_token" value="{{csrf_token()}}">
    <audio src="{{asset('utils/notify.mp3')}}" preload="auto" controls id="audioNotify" style="display: none;"></audio>

    <script>
        var langText = {!! json_encode([
            'ChatOneOnOne'      => __('system.ChatOneOnOne'),
            'GroupChat'      => __('system.GroupChat'),
            'SelectAUser'      => __('system.SelectAUser'),
            'SelectAGroup'      => __('system.SelectAGroup'),
            'CreateAGroup'      => __('system.CreateAGroup'),
            'please_choose_a_user'      => __('system.please_choose_a_user'),
            'please_choose_at_least_a_user'      => __('system.please_choose_at_least_a_user'),
            'please_choose_a_group'      => __('system.please_choose_a_group'),
            'Name'      => __('system.Name'),
            'Description'      => __('system.Description'),
            'GroupImage'      => __('system.GroupImage'),
            'GroupMembers'      => __('system.GroupMembers'),
            'ClickChooseGroupImage'      => __('system.ClickChooseGroupImage'),
            'Options'      => __('system.Options'),
            'ArchiveMess'      => __('system.ArchiveMess'),
            'DeleteMess'      => __('system.DeleteMess'),
            'LeaveGroup'      => __('system.LeaveGroup'),
            'Admin'      => __('system.Admin'),
            'AddPeople'      => __('system.AddPeople'),
            'Addmorepeople'      => __('system.Addmorepeople'),
            'Addtogroup'      => __('system.Addtogroup'),
            'click_change_group_image'      => __('system.click_change_group_image'),
            'change_group_name_mess'      => __('system.change_group_name_mess'),
            'RemoveUserFromGroup'      => __('system.RemoveUserFromGroup'),
            'ViewProfile'      => __('system.ViewProfile'),
            'failed_retrieve'      => __('system.failed_retrieve'),
        ]) !!};
        var listUsers = {!! json_encode($users) !!};
        var listGroups = {!! json_encode($groups) !!};
        var roomRefreshTime = {{config('bedone.REFRESH_ROOM_TIME') * 1000}};
        window.LoggedUserID = {{$userObj->ID}};
    </script>
@endsection