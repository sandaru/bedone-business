<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{{isset($page_title) ? $page_title . " - " . config('bedone.SITE_NAME') : config('bedone.SITE_NAME')}}</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport">

    <!-- CSS Files -->
    @if(count($CssInclude))
        @foreach ($CssInclude as $file)
            <link href="{{$file}}" rel="stylesheet">
        @endforeach
    @endif

</head>

<body>
<div class="wrapper wrapper-full-page">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-wrapper">
                <a class="navbar-brand" href="/">{{config('bedone.SITE_NAME')}}</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar burger-lines"></span>
                    <span class="navbar-toggler-bar burger-lines"></span>
                    <span class="navbar-toggler-bar burger-lines"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navbar">
                <ul class="navbar-nav">
                    <li class="nav-item @if(route('registerPage') == Request::url()) active @endif">
                        <a href="{{route('registerPage')}}" class="nav-link">
                            <i class="fa fa-edit" style="font-size: 20px;"></i> {{__('system.register')}}
                        </a>
                    </li>
                    <li class="nav-item @if(route('loginPage') == Request::url()) active @endif">
                        <a href="{{route('loginPage')}}" class="nav-link">
                            <i class="fa fa-sign-in-alt" style="font-size: 20px;"></i> {{__('system.login')}}
                        </a>
                    </li>
                    <li class="nav-item @if(route('forgotPage') == Request::url()) active @endif">
                        <a href="{{route('forgotPage')}}" class="nav-link">
                            <i class="fa fa-lock-open" style="font-size: 20px;"></i> {{__('system.forgot')}}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    @yield('body')


    <footer class="footer">
        <div class="container">
            <nav>
                <!--
                <ul class="footer-menu">
                    <li>
                        <a href="">
                            Home
                        </a>
                    </li>
                </ul>
                -->
                <p class="copyright text-center">
                    © {{config('bedone.DEVELOPMENT_YEAR')}} <a href="http://sethphat.com">Seth Phat</a>
                </p>
            </nav>
        </div>
    </footer>

</div>

@if(count($JsInclude))
    @foreach ($JsInclude as $file)
        <script type="text/javascript" src="{{$file}}"></script>
    @endforeach
@endif

</body>
</html>