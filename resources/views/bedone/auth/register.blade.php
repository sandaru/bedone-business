@extends('bedone.auth.template')

@section('body')
    <div class="full-page register-page section-image" data-color="purple" data-image="">
        <div class="content">
            <div class="container">
                <div class="card card-register card-plain text-center">
                    <div class="card-header ">
                        <div class="row  justify-content-center">
                            <div class="col-md-8">
                                <div class="header-text">
                                    <h2 class="card-title">{{strtoupper(config('bedone.SITE_NAME'))}}</h2>
                                    <h4 class="card-subtitle">{{__('system.introduce_title')}}</h4>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="fa fa-user" style="font-size: 25px;"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>{{__('system.free_to_experience')}}</h4>
                                        <p>{{__('system.free_to_experience_intro')}}</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="fa fa-chart-line" style="font-size: 25px;"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>{{__('system.awesome_performance')}}</h4>
                                        <p>{{__('system.awesome_performance_intro')}}</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="fa fa-globe" style="font-size: 25px;"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>{{__('system.globalization')}}</h4>
                                        <p>{{__('system.globalization_intro')}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mr-auto">
                                <form method="POST" action="{{route('registerAction')}}">
                                    {!! csrf_field() !!}
                                    <div class="card card-plain">
                                        <div class="content">
                                            <div class="form-group">
                                                <input type="text" name="FirstName" value="{{old('FirstName')}}" placeholder="{{__('system.FirstName')}}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="LastName" value="{{old('LastName')}}" placeholder="{{__('system.LastName')}}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" name="Email" value="{{old('Email')}}" placeholder="{{__('system.Email')}}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="Password" placeholder="{{__('system.Password')}}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="RetypePassword" placeholder="{{__('system.RetypePassword')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="footer text-center">
                                            <button type="submit" class="btn btn-fill btn-neutral btn-wd">{{__('system.CreateNewAccount')}}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="full-page-background" style="background-image: url({{asset('img/board_img.jpg')}}) "></div>
    </div>

    @if (Session::has('error'))
        <script>
            var error_mess = "{{ Session::get('error')}}";
        </script>
    @endif

    @if (Session::has('success'))
        <script>
            var success_mess = "{{ Session::get('success')}}";
        </script>
    @endif

    @if (count($errors) > 0)
        <script>
            var valid_error_mess = "{{$errors->first()}}";
        </script>
    @endif
@endsection