@extends('bedone.auth.template')

@section('body')
    <div class="full-page  section-image" data-color="azure" data-image="{{asset('img/board_img.jpg')}}">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                    <form class="form" method="POST" action="{{route('loginAction')}}">
                        {!! csrf_field() !!}
                        <div class="card card-login">
                            <div class="card-header ">
                                <h3 class="header text-center">{{__('system.login')}}</h3>
                            </div>
                            <div class="card-body ">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>{{__('system.Email')}}</label>
                                        <input type="email" name="Email" placeholder="{{__('system.Email')}}" value="{{old('Email')}}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('system.Password')}}</label>
                                        <input type="password" name="Password" placeholder="{{__('system.Password')}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-warning btn-wd">{{__('system.LoginNow')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="full-page-background" style="background-image: url({{asset('img/board_img.jpg')}}) "></div>
    </div>
    @if (Session::has('error'))
        <script>
            var error_mess = "{{ Session::get('error')}}";
        </script>
    @endif

    @if (Session::has('success'))
        <script>
            var success_mess = "{{ Session::get('success')}}";
        </script>
    @endif

    @if (count($errors) > 0)
        <script>
            var valid_error_mess = "{{__('system.email_password_failed')}}";
        </script>
    @endif
@endsection