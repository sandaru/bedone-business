@extends('bedone.auth.template')

@section('body')
    <div class="full-page  section-image" data-color="orange" data-image="{{asset('img/board_img.jpg')}}">
        <div class="content">
            <div class="container">
                <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                    <form class="form" method="POST" action="{{route('forgotAction')}}">
                        {!! csrf_field() !!}
                        <div class="card card-login">
                            <div class="card-header ">
                                <h3 class="header text-center">{{__('system.forgot')}}</h3>
                            </div>
                            <div class="card-body ">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>{{__('system.Email')}}</label>
                                        <input type="email" name="Email" placeholder="{{__('system.Email')}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-warning btn-wd">{{__('system.RetrieveAccountNow')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="full-page-background" style="background-image: url({{asset('img/board_img.jpg')}}) "></div>
    </div>

    @if (Session::has('error'))
        <script>
            var error_mess = "{{ Session::get('error')}}";
        </script>
    @endif

    @if (Session::has('success'))
        <script>
            var success_mess = "{{ Session::get('success')}}";
        </script>
    @endif

    @if (count($errors) > 0)
        <script>
            var valid_error_mess = "{{$errors->first()}}";
        </script>
    @endif
@endsection