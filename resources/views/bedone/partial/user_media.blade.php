<div class="media">
    <img class="mr-3" src="{{profile_picture($user['ProfileImage'])}}" style="width: 50px;height: 50px;object-fit: cover; border-radius: 100%;"
         alt="{{$user['FullName']}}">
    <div class="media-body">
        <h5 class="mt-0">{{$user['FullName']}}</h5>
        {{$user['JobTitle']}}
    </div>
</div>