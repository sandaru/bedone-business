<div class="media">
    <img class="mr-3" src="{{group_picture($group['GroupImage'])}}" style="width: 50px;height: 50px;object-fit: cover; border-radius: 100%;"
         alt="{{$group['Name']}}">
    <div class="media-body">
        <h5 class="mt-0">{{$group['Name']}}</h5>
        {{__("system.GroupMembers")}}: {{$group['TotalMembers']}}
    </div>
</div>