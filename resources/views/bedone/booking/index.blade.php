@extends('layout.bedone_template')

@section('body_content')
    <div class="row" id="partial" style="visibility: hidden">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading" style="padding: 20px;">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#monthViewTab">{{__('system.MonthView')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#resourceViewTab">{{__('system.Resources')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#myBookingViewTab">{{__('system.MyBooking')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane container active col-md-12" id="monthViewTab">
                            <div id="monthViewCalendar"></div>
                        </div>
                        <div class="tab-pane fade" id="resourceViewTab">
                                @foreach($resources as $resource)
                                    <div class="media" style="padding: 10px;">
                                        <img class="mr-3" src="{{feature_image(getTagValue($resource['Gallery'])[0])}}"
                                             alt="{{$resource['Name']}}" style="width: 150px;">
                                        <div class="media-body">
                                            <h5 class="mt-0">
                                                <a href="{{route('bookingViewResourcePage', ['id' => $resource['ID']])}}" target="_blank">
                                                    {{$resource['Name']}}
                                                </a>
                                            </h5>
                                            {{$resource['Description']}}
                                        </div>
                                    </div>
                                @endforeach

                                @if(count($resources) <= 0)
                                    <p class="text-center">{{__('system.no_booking_resource')}}</p>
                                @endif
                        </div>
                        <div class="tab-pane fade" id="myBookingViewTab">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="20%">{{__('system.BookDate')}}</th>
                                        <th>{{__('system.Title')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($myBooking as $booking)
                                    <tr>
                                        <td>{{toDate($booking->BookDate, config('bedone.DATE_FORMAT_SHOW'))}}</td>
                                        <td>
                                            <a href="javascript:void(0);" onclick="BookingModule.openMyBooking('{{encode_id($booking->ID)}}')">
                                                {{$booking->Title}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                @if(count($myBooking) <= 0)
                                    <tr>
                                        <td colspan="2" class="text-center" style="display: table-cell">{{__('system.no_booking')}}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="bookingModal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">{{__('system.CreateNewBooking')}}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td width="20%">{{__("system.Resource")}}</td>
                            <td>
                                <select id="slResource" style="width: 100%;">
                                    @foreach($resources as $resource)
                                        <option value="{{decode_id($resource['ID'])}}">{{$resource['Name']}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">{{__("system.Title")}}</td>
                            <td>
                                <input id="txtTitle" type="text" placeholder="{{__("system.Title")}}" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>{{__("system.Description")}}</td>
                            <td>
                                <textarea id="txtDescription" placeholder="{{__("system.Description")}}"
                                          class="form-control" style="height: 80px;"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>{{__('system.BookDate')}}</td>
                            <td>
                                <div class="input-group" style="padding: 0px;">
                                    <input type="text" id="txtDate" class="form-control" readonly>
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>{{__('system.StartTime')}}</td>
                            <td>
                                <div class="input-group date" style="padding: 0px;">
                                    <input type="text" id="txtStartTime" class="form-control">
                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>{{__('system.EndTime')}}</td>
                            <td>
                                <div class="input-group" style="padding: 0px;">
                                    <input type="text" id="txtEndTime" class="form-control">
                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="ckbIsAllDay">
                                    {{__('system.IsAllDay')}}
                                </label>
                            </td>
                            <td>
                                <input type="checkbox" data-toggle="toggle" id="ckbIsAllDay"
                                       data-on="{{__('system.Yes')}}" data-off="{{__('system.No')}}" data-style="ios">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="ckbIsPrivate">
                                    {{__('system.IsPrivate')}}
                                </label>
                            </td>
                            <td>
                                <input type="checkbox" data-toggle="toggle" id="ckbIsPrivate"
                                       data-on="{{__('system.Yes')}}" data-off="{{__('system.No')}}" data-style="ios">
                            </td>
                        </tr>
                        <tr>
                            <td>{{__('system.AttendUsers')}}</td>
                            <td>
                                <select id="slUsers" style="width: 100%" multiple></select>
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('system.Close')}}</button>
                    <button type="button" class="btn btn-primary" id="btnCreate">{{__('system.Create')}}</button>
                    <button type="button" class="btn btn-primary" id="btnCancel" style="display: none;">{{__('system.CancelBooking')}}</button>
                    <button type="button" class="btn btn-primary" id="btnSave" style="display: none;">{{__('system.Save')}}</button>
                </div>

            </div>
        </div>
    </div>

    <script>
        var listUsers = {!! json_encode($users) !!};
        var functionAccess = {!! json_encode($functionAccess) !!};
        var userID = {{$userObj->ID}};
        var langText = {!! json_encode([
            'CreateNewBooking'          => __('system.CreateNewBooking'),
            'EditBooking'               => __('system.EditBooking'),
            'ViewBooking'               => __('system.ViewBooking'),
        ]) !!};
        var today_date = "{{date(config('bedone.DATE_FORMAT'))}}";
        var DATE_FORMAT = "{{config('bedone.DATE_FORMAT')}}";
    </script>
@endsection