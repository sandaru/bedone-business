@extends('layout.bedone_template')

@section('body_content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                    {{__('system.Gallery')}}
                </h5>
                <div class="card-body">
                    @if ($resource->GalleryItems != [""])
                        <div id="carousel" class="carousel slide" data-ride="carousel">

                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                                @for ($i = 0; $i < count($resource->GalleryItems); $i++)
                                    <li data-target="#carousel" data-slide-to="{{$i}}" @if($i === 0) class="active" @endif></li>
                                @endfor
                            </ul>

                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                @foreach ($resource->GalleryItems as $index => $item)
                                    <div class="carousel-item @if ($index === 0) active @endif text-center">
                                        <img src="{{asset($item)}}" alt="{{$resource->Name}}" class="img-fluid">
                                    </div>
                                @endforeach
                            </div>

                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#carousel" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>

                        </div>
                    @else
                        <p>{{__('system.no_gallery')}}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                    {{__('system.ResourceInformation')}}
                </h5>
                <div class="card-body">
                    <h4 style="margin-top: 5px;">{{$resource->Name}}</h4>
                    <p>{{$resource->Description}}</p>
                    <p><b>{{__('system.Address')}}:</b> {{$resource->Address}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection