@extends('layout.bedone_template')

@section('body_content')
    <div class="row" id="partial" style="visibility: hidden;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-4 float-right">
                        <div class="input-group">
                            <input type="text" class="form-control" v-model="keyword"
                                   placeholder="{{__('system.Search_by_group')}}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">{{__('system.Search')}}</button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 mb-3"
                             v-for="group in list_groups"
                             v-if="group.Name.toLowerCase().indexOf(keyword.toLowerCase()) >= 0">
                            <div class="media border p-3">
                                <a :href="group.GroupInfoURL" target="_blank">
                                    <img :src="group.GroupImage" alt="group.Name"
                                         class="mr-3 mt-3 rounded-circle img-avatar-default">
                                </a>
                                <div class="media-body">
                                    <a :href="group.GroupInfoURL" target="_blank">
                                        <h4>@{{group.Name}}</h4>
                                    </a>
                                    <p>{{__('system.GroupMembers')}}: @{{group.TotalMembers}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="text-center" v-if="list_groups.length === 0">{{__('system.no_groups')}}</p>

                </div>
            </div>
        </div>
    </div>
@endsection