@extends('layout.bedone_template')

@section('body_content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                    {{__('system.GroupInfo')}}
                </h5>
                <div class="card-body">
                    <table style="width: 100%;">
                        <tr>
                            <td width="10%">
                                <img src="{{group_picture($group->GroupImage)}}"
                                     class="rounded-circle img-avatar-default"
                                     alt="{{$group->Name}}">
                            </td>
                            <td>
                                <h5 style="margin-bottom: 5px;">{{$group->Name}}</h5>
                                <p style="font-size: 14px;">
                                    {{$group->Description}}
                                </p>
                            </td>
                        </tr>
                    </table>

                    <h4>{{__("system.GroupAdmin")}}</h4>
                    @php
                        $admin_profile_url = route('userProfilePage', [encode_id($adminUser->ID)]);
                    @endphp
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div class="media border p-3">
                                <a href="{{$admin_profile_url}}" target="_blank">
                                    <img src="{{profile_picture($adminUser->ProfileImage)}}" alt="{{$adminUser->full_name}}"
                                         class="mr-3 mt-3 rounded-circle img-avatar-default">
                                </a>
                                <div class="media-body">
                                    <a href="{{$admin_profile_url}}" target="_blank">
                                        <h4>{{$adminUser->full_name}}</h4>
                                    </a>
                                    <p>{{$adminUser->JobTitle}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4>{{__("system.GroupMembers")}}</h4>
                    <div class="row">
                        @foreach($groupMember as $mem)
                            @php
                                $profile_url = route('userProfilePage', [encode_id($mem->UserID)]);
                                $remove_mem_url = route('removeGroupMemberAction', [encode_id($group->ID), encode_id($mem->UserID)]);
                            @endphp
                            <div class="col-md-6 mb-3">
                                <div class="media border p-3 media-group-member">
                                    <a href="{{$profile_url}}" target="_blank">
                                        <img src="{{profile_picture($mem->User->ProfileImage)}}" alt="{{$mem->User->full_name}}"
                                             class="mr-3 mt-3 rounded-circle img-avatar-default">
                                    </a>
                                    <div class="media-body">
                                        <a href="{{$profile_url}}" target="_blank">
                                            <h4>{{$mem->User->full_name}}</h4>
                                        </a>
                                        <p>{{$mem->User->JobTitle}}</p>
                                    </div>

                                    @if($IsAdmin || $IsMasterAdmin)
                                    <div class="toolbar">
                                        <a href="javascript:void(0);"
                                           data-href="{{$remove_mem_url}}"
                                           title="{{__('system.remove_group_member')}}"
                                           class="removeMember">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @if (count($groupMember) <= 0)
                        <p class="text-center">{{__('system.group_no_member')}}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @if($IsAdmin || $IsMasterAdmin)
        <div class="modal" id="editGroupModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="{{route('editGroupAction', [encode_id($group->ID)])}}" method="post"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">{{__('system.EditGroup')}}</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td width="20%">{{__('system.GroupName')}}</td>
                                    <td>
                                        <input type="text" class="form-control" name="Name">
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{__('system.Description')}}</td>
                                    <td>
                                        <textarea name="Description" class="form-control"
                                                  style="height: 80px"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{__('system.GroupImage')}}</td>
                                    <td>
                                        <img class="img-avatar-default rounded-circle change-img clickable"
                                             data-toggle="tooltip" title="{{__('system.click_change_group_image')}}">
                                        <input type="file" id="fileGroupImage" style="display: none;">
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger"
                                    data-dismiss="modal">{{__('system.Close')}}</button>
                            <button type="submit" class="btn btn-primary">{{__('system.Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" id="addPeopleModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="{{route('addMemberGroupAction', [encode_id($group->ID)])}}" method="post">
                        {!! csrf_field() !!}

                    <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">{{__('system.Addmorepeople')}}</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-group">
                                <label>{{__('system.Addtogroup')}}</label>
                                <select id="slAddPeople" name="user_ids[]" style="width: 100%" multiple></select>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('system.Close')}}</button>
                            <button type="submit" class="btn btn-primary">{{__('system.Addmorepeople')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script>
            var default_data = {
                Name: "{{$group->Name}}",
                Description: "{{$group->Description}}",
                GroupImage: "{{group_picture($group->GroupImage)}}",
            };

            var option_list = {!! json_encode($user_option) !!};
        </script>
    @endif

    <script>
        var group_id = "{{encode_id($group->ID)}}";
    </script>
@endsection