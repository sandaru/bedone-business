@extends('layout.bedone_template')

@section('body_content')

    <form action="{{route('editCompanyAction')}}" method="post" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <input type="file" id="fileFeatureImage" style="display: none;">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body text-right">
                        <button class="btn btn-primary">{{__('system.Save')}}</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <h5 class="card-header bg-primary" style="padding: 20px; color:#fff;">
                        {{__('system.CompanyInfo')}}
                    </h5>
                    <div class="card-body">
                        <div style="padding-bottom: 20px; position: relative;">
                            <img src="{{feature_image($Company->FeatureImage)}}"
                                 id="imgFeature"
                                 class="img-fluid company-cover"
                                 alt="{{$Company->Name}}">

                            <div style="position: absolute; bottom: 25px; right: 10px;">
                                <button class="btn btn-info" type="button" id="btnCover">{{__('system.CoverPhoto')}}</button>
                            </div>
                        </div>

                        <div class="form-group" style="position: relative;">
                            <label>{{__('system.CompanyName')}}</label>
                            <input type="text" name="Name" class="form-control input-char-count"
                                   value="{{$Company->Name}}" maxlength="50">
                        </div>

                        <div class="form-group">
                            <label>{{__('system.Description')}}</label>
                            <textarea name="Description" class="form-control"
                                      style="height: 200px;">{{$Company->Description}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <h5 class="card-header bg-info" style="padding: 20px; color:#fff;">
                        {{__('system.CompanyAdmin')}}
                    </h5>
                    <div class="card-body">
                        <table style="width: 100%;">
                            @foreach ($AllAdmins as $user)
                                <tr>
                                    <td width="30%" class="text-center" style="vertical-align: middle;">
                                        <img class="avatar-size rounded-circle border-gray"
                                             src="{{profile_picture($user->ProfileImage)}}" alt="{{$user->full_name}}">
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <a href="{{route('userProfilePage', ['id' => encode_id($user->ID)])}}"
                                           target="_blank">
                                            <h4 style="line-height: 5px">{{$user->full_name}}</h4>
                                        </a>
                                        <p>{{$user->JobTitle}}</p>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        @if (count($AllAdmins) <= 0)
                            <p>{{__('system.no_admin')}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </form>

    <script>
        var langText = {
            file_not_allowed: "{{__('system.file_not_allowed')}}",
        };
    </script>
@endsection