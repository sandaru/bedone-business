@extends('layout.bedone_template')

@section('body_content')

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header bg-primary" style="padding: 20px; color:#fff;">
                    {{__('system.CompanyInfo')}}
                </h5>
                <div class="card-body">
                    <h3>{{$Company->Name}}</h3>

                    <div style="padding-bottom: 20px;" class="text-center">
                        <img src="{{feature_image($Company->FeatureImage)}}" class="img-fluid"
                             alt="{{$Company->Name}}">
                    </div>

                    <h5>
                        <a href="javascript:void(0);" data-toggle="collapse" style="color:#000;font-weight: bold;"
                           data-target="#descriptionCollapse" onclick="$('#collapsed-chevron').toggleClass('fa-rotate-180')">
                            <i class="fa fa-chevron-circle-up" id="collapsed-chevron"></i>
                            {{__('system.Description')}}
                        </a>
                    </h5>

                    <div id="descriptionCollapse" class="collapse show">
                        {!! empty($Company->Description) ? __('system.no_description') : nl2br($Company->Description)  !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <h5 class="card-header bg-info" style="padding: 20px; color:#fff;">
                    {{__('system.CompanyAdmin')}}
                </h5>
                <div class="card-body">
                    <table style="width: 100%;">
                    @foreach ($AllAdmins as $user)
                        <tr>
                            <td width="30%" class="text-center" style="vertical-align: middle;">
                                <img class="avatar-size rounded-circle border-gray" src="{{profile_picture($user->ProfileImage)}}" alt="{{$user->full_name}}">
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="{{route('userProfilePage', ['id' => encode_id($user->ID)])}}" target="_blank">
                                    <h4 style="line-height: 5px">{{$user->full_name}}</h4>
                                </a>
                                <p>{{$user->JobTitle}}</p>
                            </td>
                        </tr>
                    @endforeach
                    </table>
                    @if (count($AllAdmins) <= 0)
                        <p>{{__('system.no_admin')}}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script>
        var leave_url = "{{route('leaveCompanyAction')}}";
    </script>

@endsection