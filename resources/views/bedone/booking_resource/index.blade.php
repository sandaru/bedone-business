@extends('layout.bedone_template')

@section('body_content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                {{$page_title}}
            </h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <form action="{{route('bookingResourcePage')}}" method="get">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="keyword" placeholder="{{__('system.keyword_here')}}">
                                <button type="submit" class="btn btn-outline-secondary">{{__('system.Search')}}</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="text-right">
                    @if ($functionAccess['Add'])
                        <a href="{{route('addBookingResourcePage')}}" class="btn btn-primary">{{__('system.Add')}}</a>
                    @endif
                </div>

                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>{{__('system.Name')}}</th>
                        <th>{{__('system.Description')}}</th>
                        <th>{{__('system.Address')}}</th>
                        <th>{{__('system.LastUpdateDate')}}</th>
                        <th>{{__('system.LastUpdateBy')}}</th>
                        <th style="display: table-cell;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($listResource) <= 0)
                        <tr>
                            <td colspan="6" style="display: table-cell;">{{__('system.no_data_available')}}</td>
                        </tr>
                    @endif

                    @foreach($listResource as $item)
                        <tr>
                            <td>{{$item->Name}}</td>
                            <td>{{$item->Description}}</td>
                            <td>{{$item->Address}}</td>
                            <td>{{$item->ModifiedDate}}</td>
                            <td>{{$item->ModifiedByName}}</td>
                            <td style="display: table-cell;">
                                @if ($functionAccess['Edit'])
                                    <a href="{{route('editBookingResourcePage', ['id' => encode_id($item->ID)])}}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @endif

                                @if ($functionAccess['Delete'])
                                    <a data-href="{{route('deleteBookingResourceAction', ['id' => encode_id($item->ID)])}}"
                                       class="deleteAction"
                                       href="javascript:void(0);">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="text-center">
                    {!! $listResource->appends(request()->query())->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection