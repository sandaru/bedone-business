@extends('layout.bedone_template')

@section('body_content')
    <div class="row" id="partial" style="visibility: hidden;">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary" style="padding: 20px; color: #fff;">
                    {{$page_title}}
                </h5>

                <div class="card-body">
                    <form action="#" method="post">
                        {!! csrf_field() !!}
                        <div class="col-md-6">
                            <label>{{__('system.FeatureImage')}}</label>
                            <div class="img-list row">
                                <div class="img-add" v-for="(img, index) in selectedFiles">
                                    <div class="close clickable" @click="removeImage(index)">&times;</div>
                                    <img :src="showImage(img)">
                                </div>
                                <div class="img-add clickable" v-if="selectedFiles.length <= 4" @click="selectImage">
                                    <i class="fa fa-plus fa-2x"></i>
                                </div>

                                <input type="file" id="fileUpload" @change="selectedImage" style="display: none;">
                            </div>

                            <div class="form-group" style="position: relative;">
                                <label>{{__('system.Name')}}</label>
                                <input type="text" name="Name" placeholder="{{__('system.Name')}}" v-model="Name"
                                       class="form-control input-char-count" maxlength="50">
                            </div>
                            <div class="form-group" style="position: relative;">
                                <label>{{__('system.Description')}}</label>
                                <input type="text" name="Description" placeholder="{{__('system.Description')}}"  v-model="Description"
                                       class="form-control input-char-count" maxlength="100">
                            </div>
                            <div class="form-group" style="position: relative;">
                                <label>{{__('system.Address')}}</label>
                                <input type="text" name="Address" placeholder="{{__('system.Address')}}"  v-model="Address"
                                       class="form-control input-char-count" maxlength="100">
                            </div>


                            <button type="button" class="btn btn-primary" @click="save">{{__('system.Add')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        var back_url = "{!! route('bookingResourcePage')!!}";
    </script>
@endsection