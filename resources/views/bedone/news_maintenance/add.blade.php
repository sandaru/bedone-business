@extends('layout.bedone_template')

@section('body_content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary" style="padding: 20px; color: #fff;">
                    {{$page_title}}
                </h5>

                <div class="card-body">
                    <form action="{{route('addNewsMaintenanceAction')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="col-md-6">
                            <div class="form-group" style="position: relative;">
                                <label>{{__('system.CategoryName')}}</label>
                                <input type="text" name="Name" class="form-control input-char-count" maxlength="30">
                            </div>
                            <div class="form-group" style="position: relative;">
                                <label>{{__('system.Description')}}</label>
                                <input type="text" name="Description" class="form-control input-char-count" maxlength="50">
                            </div>
                            <div class="form-group">
                                <label>{{__('system.Icon')}}</label> <br>
                                <select name="Icon" id="slIcon" class="form-control" style="width: 90%; display: inline-block;">
                                    <option></option>
                                    @foreach(config('bedone.ICON_LIST') as $icon)
                                        <option value="{{$icon}}">
                                            {{$icon}}
                                        </option>
                                    @endforeach
                                </select>
                                <i id="iconCategory"></i>
                            </div>

                            <button class="btn btn-primary">{{__('system.Add')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection