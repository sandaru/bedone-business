@extends('layout.bedone_template')

@section('body_content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary" style="padding: 20px; color: #fff;">
                    {{$page_title}}
                </h5>

                <div class="card-body">
                    <div class="text-right">
                        @if($PagePermission['Add'])
                            <a href="{{route('addNewsMaintenancePage')}}" class="btn btn-primary">{{__('system.Add')}}</a>
                        @endif
                    </div>

                    <table class="table table-bordered table-striped" id="tbNews">
                        <thead>
                            <tr>
                                <th class="text-center">{{__('system.CategoryName')}}</th>
                                <th class="text-center">{{__('system.Description')}}</th>
                                <th class="text-center">{{__('system.Icon')}}</th>
                                <th class="text-center">{{__('system.LastUpdateDate')}}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($AllCategories as $item)
                                <tr>
                                    <td>{{$item->Name}}</td>
                                    <td>{{$item->Description}}</td>
                                    <td class="text-center">
                                        <i class="fa fa-2x {{$item->Icon}}"></i>
                                    </td>
                                    <td class="text-center">
                                        {{$item->ModifiedDate}}
                                    </td>
                                    <td class="text-center" style="display: table-cell">
                                        @if($PagePermission['Edit'])
                                            <a href="{{route('editNewsMaintenancePage', ['id' => encode_id($item->ID)])}}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endif

                                        @if($PagePermission['Delete'])
                                            <a href="javascript:void(0);"
                                               onclick="NewsCategoryMaintenanceAction.confirmDelete('{{encode_id($item->ID)}}')">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        var langText = {!! json_encode([
            'are_you_sure'  => __('system.are_you_sure'),
            'wont_revert'  => __('system.wont_revert'),
            'YesDelete'  => __('system.YesDelete'),
        ]) !!};
    </script>
@endsection