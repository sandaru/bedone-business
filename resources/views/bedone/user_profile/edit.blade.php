@extends('layout.bedone_template')

@section('body_content')
    <form action="#" method="post" id="partial" style="visibility: hidden">
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body text-right">
                        <button class="btn btn-primary" type="button" @click="save">{{__('system.Save')}}</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                        {{$page_title}}
                    </h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{__('system.Name')}}</label>
                                    <input type="text" name="Name" class="form-control" v-model="name" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{__('system.Description')}}</label>
                                    <input type="text" name="Description" class="form-control" v-model="description" />
                                </div>
                            </div>
                        </div>

                        <hr />

                        <div class="row">
                            <div class="col-md-8">
                                <h5>{{__('system.ProfileModule')}}</h5>
                                <div id="treeNodeBody"></div>
                            </div>
                            <div class="col-md-4" v-if="selectedNode != null">
                                <div class="text-right">
                                    <button class="btn btn-default"
                                            type="button"
                                            @click="applyFunction">
                                        {{__('system.Apply')}}
                                    </button>
                                    <button class="btn btn-default"
                                            type="button"
                                            @click="closeFunction">
                                        {{__('system.Close')}}
                                    </button>
                                </div>

                                <ul class="list-group">
                                    <li class="list-group-item active">@{{ selectedNode.text }} {{__('system.Functions')}}</li>
                                    <li class="list-group-item" v-for="item in selectedNode.functions">
                                        <label style="color:black;">
                                            <input type="checkbox" v-model="item.Checked">
                                            @{{ item.Name }}
                                        </label>
                                    </li>
                                    <li v-if="selectedNode.functions.length == 0" class="list-group-item">
                                        {{__('system.NoFunctions')}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script>
        var companyModules = {!! json_encode($CompanyModules) !!};
        var returnURL = "{!! route('profileMaintenancePage') !!}";
        var langText = {!! json_encode([
            'action_failed' => __('system.action_failed'),
        ]) !!};
        var objUserProfile = {
            ID: "{{encode_id($UserProfile->ID)}}",
            Name: "{{$UserProfile->Name}}",
            Description: "{{$UserProfile->Description}}",
        };
    </script>
@endsection