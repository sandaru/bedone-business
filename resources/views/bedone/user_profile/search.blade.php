@extends('layout.bedone_template')

@section('body_content')
    <div class="row" id="partial">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                    {{$page_title}}
                </h5>
                <div class="card-body" id="joinBody">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="{{route('profileMaintenancePage')}}" method="get">
                                <div class="input-group mb-3">
                                    <select class="custom-select" name="filter_by">
                                        <option value="name">{{__('system.Name')}}</option>
                                        <option value="description">{{__('system.Description')}}</option>
                                        <option value="creator">{{__('system.CreatorName')}}</option>
                                    </select>
                                    <input type="text" class="form-control" name="keyword" placeholder="{{__('system.keyword_here')}}">
                                    <button type="submit" class="btn btn-outline-secondary">{{__('system.Search')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    @if($functionAccess['View'])
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-right">
                                <a href="{{route('addProfileMaintenancePage')}}" class="btn btn-primary">{{__('system.Add')}}</a>
                            </div>
                        </div>
                    </div>
                    @endif

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>{{__('system.Name')}}</th>
                            <th>{{__('system.Description')}}</th>
                            <th>{{__('system.IsMasterAdmin')}}</th>
                            <th>{{__('system.LastUpdateDate')}}</th>
                            <th>{{__('system.LastUpdateBy')}}</th>
                            <th style="display: table-cell"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($listUser) <= 0)
                            <tr>
                                <td colspan="6" style="display: table-cell">
                                    {{__('system.no_data_available')}}
                                </td>
                            </tr>
                        @else
                            @foreach ($listUser as $user)
                                <tr>
                                    <td>{{$user->Name}}</td>
                                    <td>{{$user->Description}}</td>
                                    <td>{{__('system.'.ucfirst($user->IsMasterAdmin))}}</td>
                                    <td>{{$user->ModifiedDate}}</td>
                                    <td>{{$user->CreatedByName}}</td>
                                    <td style="display: table-cell">
                                        @if($functionAccess['Edit'] == true && $user->IsMasterAdmin === "no")
                                                <a href="{{route('editProfileMaintenancePage', ['id' => encode_id($user->ID)])}}"
                                                   title="{{__('system.Edit')}}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                        @endif

                                        @if($functionAccess['Delete'] == true  && $user->IsMasterAdmin === "no")
                                                <a data-href="{{route('deleteProfileMaintenanceAction', ['id' => encode_id($user->ID)])}}"
                                                   href="javascript:void(0);"
                                                   @click="deleteProfile"
                                                   title="{{__('system.Delete')}}">
                                                    <i class="fa fa-times-circle"></i>
                                                </a>
                                        @endif

                                        @if($functionAccess['View'])
                                            <a href="{{route('viewProfileMaintenancePage', ['id' => encode_id($user->ID)])}}" title="{{__('system.ViewUserProfileConfig')}}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        @endif

                                            <a href="javascript:void(0);"
                                               title="{{__('system.UserProfileUsers')}}"
                                               @click="viewUser('{{encode_id($user->ID)}}')">
                                                <i class="fa fa-users"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>

                    <div class="text-center">
                        {!! $listUser->appends(request()->query())->links() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="userModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">{{__('system.UserInUserProfile')}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <table id="tableUser" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        var functionAccess = {!! json_encode($functionAccess) !!};
        var langText = {
            failed_retrieve: "{{__('system.failed_retrieve')}}",
            are_you_sure: "{{__('system.are_you_sure')}}",
            wont_revert: "{!! __('system.wont_revert') !!}",
            Yes: "{{__('system.Yes')}}",
            delete_failed: "{{__('system.delete_failed')}}",
            file_not_allowed: "{{__('system.file_not_allowed')}}",
            action_failed: "{{__('system.action_failed')}}",
        };
    </script>
@endsection