@extends('layout.bedone_template')

@section('body_content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                    {{$page_title}}
                </h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="{{route('userMaintenancePage')}}" method="get">
                                <div class="input-group mb-3">
                                    <select class="custom-select" name="filter_by">
                                        <option value="full_name">Full Name</option>
                                        <option value="email">Email</option>
                                        <option value="jobtitle">Job Title</option>
                                    </select>
                                    <input type="text" class="form-control" name="keyword" placeholder="{{__('system.keyword_here')}}">
                                    <button type="submit" class="btn btn-outline-secondary">{{__('system.Search')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{__('system.FirstName')}}</th>
                                <th>{{__('system.LastName')}}</th>
                                <th>{{__('system.JobTitle')}}</th>
                                <th>{{__('system.Email')}}</th>
                                <th>{{__('system.UserProfile')}}</th>
                                <th style="display: table-cell"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($listUser) <= 0)
                                <tr>
                                    <td colspan="6" style="display: table-cell">
                                        {{__('system.no_data_available')}}
                                    </td>
                                </tr>
                            @else
                                @foreach ($listUser as $user)
                                    <tr>
                                        <td>{{$user->FirstName}}</td>
                                        <td>{{$user->LastName}}</td>
                                        <td>{{$user->JobTitle}}</td>
                                        <td>{{$user->Email}}</td>
                                        <td>{{$user->UserProfileAccess->UserProfile->Name}}</td>
                                        <td style="display: table-cell">
                                            @if($functionAccess['Edit'] == true)
                                                @if($userObj->ID != $user->ID && $user->Company->UserAdminID != $user->ID)
                                                    <a href="{{route('editUserMaintenancePage', ['id' => encode_id($user->ID)])}}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endif
                                            @endif

                                            @if($functionAccess['RemoveUser'] == true)
                                                @if($userObj->ID != $user->ID && $user->Company->UserAdminID != $user->ID)
                                                    <a href="javascript:void(0);"
                                                       data-href="{{route('removeUserMaintenanceAction', ['id' => encode_id($user->ID)])}}"
                                                       class="removeUser"
                                                       title="{{__('system.RemoveUserCompany')}}">
                                                        <i class="fa fa-times-circle"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>

                    <div class="text-center">
                        {!! $listUser->appends(request()->query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var langText = {
            are_you_sure: "{{__('system.are_you_sure')}}",
            wont_revert: "{!!__('system.wont_revert')!!}",
            Yes: "{{__('system.Yes')}}",
        };
    </script>
@endsection