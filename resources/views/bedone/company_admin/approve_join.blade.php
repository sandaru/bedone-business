@extends('layout.bedone_template')

@section('body_content')
    <div class="row">
        <div class="col-md-6">
            <form action="{{route('joinApproveAction', ['id' => encode_id($JoinRequest->ID)])}}" method="post">
                {!! csrf_field() !!}
                <div class="card">
                    <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                        {{__('system.ProfileInformation')}}
                    </h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>{{__('system.UserProfile')}}</label>
                            </div>
                            <div class="col-md-9">
                                <select name="UserProfileID" id="slcProfile" style="width: 100%;">
                                    @foreach($ListUserProfile as $value => $text)
                                        <option value="{{encode_id($value)}}">{{$text}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div style="padding-top: 15px;">
                            <button class="btn btn-primary">{{__('system.Approve')}}</button>
                            <a href="{{route('companyJoinRequestPage')}}" class="btn btn-warning">{{__('system.Cancel')}}</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6">
            <div class="card">
                <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                    {{__('system.UserInformation')}}
                </h5>
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td width="25%">
                                {{__('system.FirstName')}}
                            </td>
                            <td>
                                {{$JoinRequest->User->FirstName}}
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                {{__('system.LastName')}}
                            </td>
                            <td>
                                {{$JoinRequest->User->LastName}}
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                {{__('system.Email')}}
                            </td>
                            <td>
                                {{$JoinRequest->User->Email}}
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                {{__('system.JobTitle')}}
                            </td>
                            <td>
                                {{empty($JoinRequest->User->JobTitle) ? __('system.No') : $JoinRequest->User->JobTitle}}
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                {{__('system.Gender')}}
                            </td>
                            <td>
                                {{$JoinRequest->User->gender_text}}
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                {{__('system.ProfileImage')}}
                            </td>
                            <td>
                                <img src="{{profile_picture($JoinRequest->User->ProfileImage)}}"
                                     class="rounded-circle" style="width: 120px"
                                     alt="{{$JoinRequest->User->full_name}}">                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection