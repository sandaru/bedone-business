@extends('layout.bedone_template')

@section('body_content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                    {{$page_title}}
                </h5>
                <div class="card-body" id="joinBody">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="{{route('userMaintenancePage')}}" method="get">
                                <div class="input-group mb-3">
                                    <select class="custom-select" name="filter_by">
                                        <option value="full_name">Full Name</option>
                                        <option value="email">Email</option>
                                    </select>
                                    <input type="text" class="form-control" name="keyword" placeholder="{{__('system.keyword_here')}}">
                                    <button type="submit" class="btn btn-outline-secondary">{{__('system.Search')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{__('system.FirstName')}}</th>
                            <th>{{__('system.LastName')}}</th>
                            <th>{{__('system.JobTitle')}}</th>
                            <th>{{__('system.Email')}}</th>
                            <th>{{__('system.RequestDate')}}</th>
                            <th style="display: table-cell"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($listUser) <= 0)
                            <tr>
                                <td colspan="6" style="display: table-cell">
                                    {{__('system.no_data_available')}}
                                </td>
                            </tr>
                        @else
                            @foreach ($listUser as $user)
                                <tr>
                                    <td>{{$user->User->FirstName}}</td>
                                    <td>{{$user->User->LastName}}</td>
                                    <td>{{$user->User->JobTitle}}</td>
                                    <td>{{$user->User->Email}}</td>
                                    <td>{{$user->PushedDate}}</td>
                                    <td style="display: table-cell">
                                        @if($functionAccess['Approve'] == true)
                                                <a href="{{route('joinApprovePage', ['id' => encode_id($user->ID)])}}" title="{{__('system.Approve')}}">
                                                    <i class="fa fa-check-circle"></i>
                                                </a>
                                        @endif

                                        @if($functionAccess['Decline'] == true)
                                                <a data-href="{{route('joinRejectAction', ['id' => encode_id($user->ID)])}}"
                                                   href="javascript:void(0);"
                                                   class="decline-user"
                                                   title="{{__('system.Decline')}}">
                                                    <i class="fa fa-times-circle"></i>
                                                </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>

                    <div class="text-center">
                        {!! $listUser->appends(request()->query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var functionAccess = {!! json_encode($functionAccess) !!};
        var langText = {
            failed_retrieve: "{{__('system.failed_retrieve')}}",
            sure_to_reject: "{{__('system.sure_to_reject')}}",
            wont_revert: "{!! __('system.wont_revert') !!}",
            Yes: "{{__('system.Yes')}}",
            delete_failed: "{{__('system.delete_failed')}}",
            file_not_allowed: "{{__('system.file_not_allowed')}}",
            action_failed: "{{__('system.action_failed')}}",
        };
    </script>
@endsection