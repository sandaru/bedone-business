@extends('layout.bedone_template')

@section('body_content')
    <form action="{{route('editUserMaintenanceAction', ['id' => encode_id($user->ID)])}}" method="post" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="file" id="fileProfile" style="display: none">
        <input type="file" id="fileFeature" style="display: none">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="text-right">
                            <button class="btn btn-primary">{{__('system.Save')}}</button>
                            <a href="{{route('userMaintenancePage')}}" class="btn btn-warning">{{__('system.Cancel')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            @if ($functionAccess['EditUserProfile'] === true)
            <div class="col-md-6">
                <div class="card">
                    <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                        {{__('system.ProfileInformation')}}
                    </h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>{{__('system.UserProfile')}}</label>
                            </div>
                            <div class="col-md-9">
                                <select name="UserProfileID" id="slcProfile" style="width: 100%;">
                                    @foreach($ListUserProfile as $value => $text)
                                        <option value="{{encode_id($value)}}"
                                                @if ($value == $user->UserProfileAccess->UserProfileID) selected @endif>
                                            {{$text}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="col-md-6">
                <div class="card">
                    <h5 class="card-header bg-primary" style="color:#fff; padding: 20px;">
                        {{__('system.UserInformation')}}
                    </h5>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td width="25%">
                                    {{__('system.FirstName')}}
                                </td>
                                <td>
                                    <input type="text" name="FirstName" value="{{$user->FirstName}}" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    {{__('system.LastName')}}
                                </td>
                                <td>
                                    <input type="text" name="LastName" value="{{$user->LastName}}" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    {{__('system.JobTitle')}}
                                </td>
                                <td>
                                    <input type="text" name="JobTitle" value="{{$user->JobTitle}}" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    {{__('system.WorkPhone')}}
                                </td>
                                <td>
                                    <input type="text" name="WorkPhone" value="{{$user->WorkPhone}}" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    {{__('system.WorkAddress')}}
                                </td>
                                <td>
                                    <input type="text" name="WorkAddress" value="{{$user->WorkAddress}}" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    {{__('system.ProfileImage')}}
                                </td>
                                <td>
                                    <img src="{{profile_picture($user->ProfileImage)}}"
                                         class="clickable img-avatar-default rounded-circle" id="imgProfile"
                                         data-toggle="tooltip" title="{{__('system.EditUserProfileImage')}}">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    {{__('system.FeatureImage')}}
                                </td>
                                <td>
                                    <img src="{{feature_image($user->FeatureImage)}}"
                                         class="img-fluid clickable" id="imgFeature"
                                         data-toggle="tooltip" title="{{__('system.EditUserFeatureImage')}}">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection