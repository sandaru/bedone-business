<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" id="btnNoti">
    <i class="fa fa-globe fa-normal"></i>
    @if ($Notification['Unread'] > 0)
        <span class="notification" id="numberNotification">{{$Notification['Unread']}}</span>
    @endif
    <span class="d-lg-none">{{__('system.Notification')}}</span>
</a>
<ul class="dropdown-menu dropdown-menu-right" id="notificationBody">
    @include('bedone.notification.render_item', $Notification)

    @if (count($Notification['Data']) == config('bedone.MAX_RESULT'))
        <a class="dropdown-item text-center"
           id="btnLoadMore"
           href="javascript:void(0);"
           style="color:#3498db">
            {{__('system.LoadMore')}}
        </a>
    @endif

    @if(count($Notification['Data']) === 0)
        <li class="dropdown-item">
            {{__('system.no_notification')}}
        </li>
    @endif
</ul>