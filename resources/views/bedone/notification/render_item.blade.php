@foreach($Notification['Data'] as $noti)
    <li class="dropdown-item">
        @switch($noti->Type)
            @case("AcceptJoinOrg")
            {!! __('system.accept_join_company', ['company' => $noti->Company->Name]) !!}
            @break

            @case("RejectJoinOrg")
            {!! __('system.reject_join_company', ['company' => $noti->Company->Name]) !!}
            @break

            @case("News")
            {!! __('system.news_posted', ['user' => $noti->NewsPoster->full_name]) !!}
            @break

            @case("BookingAdded")
            {!! __('system.booking_added', ['booking_title' => $noti->Booking->Title]) !!}
            @break

            @case("BookingNotification")
            {!! __('system.booking_notification', ['booking_title' => $noti->Booking->Title]) !!}
            @break

            @case("BookingCancel")
            {!! __('system.booking_cancel', ['booking_title' => $noti->Booking->Title]) !!}
            @break
        @endswitch
    </li>
@endforeach