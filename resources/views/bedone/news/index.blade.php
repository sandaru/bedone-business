@extends('layout.bedone_template')

@section('body_content')

    <div class="row" id="partial" style="visibility: hidden;">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="card-columns">
                        <div v-for="(item, index) in list_news">
                            <div class="card card-news">
                            <span class="card-category">
                                @{{ item.CategoryName }}
                            </span>
                                <a :href="getNewsPage(item.ID)" target="_blank" v-if="item.FeatureImage.length > 0">
                                    <img class="card-img-top img-fluid" :src="getFeatureImage(item.FeatureImage[0])" :alt="item.Title">
                                </a>
                                <div class="card-body">
                                    <div>
                                        <label>
                                            <i class="fa fa-calendar-alt"></i> @{{item.CreatedDate}}
                                            -
                                            @{{item.CreatedByName}}
                                        </label>
                                    </div>
                                    <h4 class="card-title">
                                        <a :href="getNewsPage(item.ID)" target="_blank">
                                            @{{ item.Title }}
                                        </a>
                                    </h4>
                                    <p class="card-text">@{{ item.Description }}</p>
                                </div>

                                <div class="card-util">
                                    <i class="fa fa-edit clickable" v-if="functionAccess.Edit === true" @click="editItem(index)"></i>
                                    <i class="fa fa-trash clickable" v-if="functionAccess.Delete === true" @click="confirmDelete(item.ID, index)"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center" v-if="list_news.length >= limit && isOver === false">
                        <button class="btn btn-primary" @click="load_more_news">{{__('system.LoadMore')}}</button>
                    </div>
                    {{--<div class="text-center" v-else>--}}
                        {{--{{__('system.End')}}--}}
                    {{--</div>--}}

                    <h4 class="text-center" v-if="list_news.length === 0">
                        {{__('system.no_news_yet')}}
                    </h4>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="list-group">
                <h5 class="list-group-item list-group-item-primary">
                    {{__('module.NewsCategory')}}
                </h5>
                <a href="javascript:void(0);"
                   class="list-group-item list-group-item-action"
                   v-for="cate in list_category"
                   :class="{ active: cate === selectedCate}"
                   :data-title="cate.Description"
                   data-toggle="tooltip"
                   @click="load_category(cate)">
                    <i class="fa" :class="cate.Icon"></i> @{{ cate.Name }}
                </a>
            </div>
        </div>

        <!-- Modal News (Add/Edit) -->
        <div class="modal" id="newsModal">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">{{__('system.NewsInformation')}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">

                        <label>Feature Images</label>
                        <div class="img-list row">
                            <div class="img-add" v-for="(img, index) in selectedFiles">
                                <div class="close clickable" @click="removeImage(index)">&times;</div>
                                <img :src="showImage(img)">
                            </div>
                            <div class="img-add clickable" v-if="selectedFiles.length <= 4" @click="selectImage">
                                <i class="fa fa-plus fa-2x"></i>
                            </div>

                            <input type="file" id="fileUpload" @change="selectedImage" style="display: none;">
                        </div>

                        <div class="form-group">
                            <label>{{__('system.Category')}}</label>
                            <select id="slCategory" v-model="news.CategoryID" class="form-control">
                                <option></option>
                                <option v-for="cate in list_category" :value="cate.ID">@{{ cate.Name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('system.Title')}}</label>
                            <input type="text" placeholder="{{__('system.Title')}}" v-model="news.Title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>{{__('system.Description')}}</label>
                            <input type="text" placeholder="{{__('system.Description')}}" v-model="news.Description" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>{{__('system.Content')}}</label>
                            <textarea placeholder="{{__('system.Content')}}"
                                      v-model="news.Content"
                                      class="form-control"
                                      style="height: 250px;"></textarea>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" @click="addOrUpdate()">{{__('system.Save')}}</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('system.Close')}}</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        var functionAccess = {!! json_encode($functionAccess) !!};
        var category_list = {!! json_encode($Categories) !!};
        var langText = {
            failed_retrieve: "{{__('system.failed_retrieve')}}",
            are_you_sure: "{{__('system.are_you_sure')}}",
            wont_revert: "{!! __('system.wont_revert') !!}",
            YesDelete: "{{__('system.YesDelete')}}",
            delete_failed: "{{__('system.delete_failed')}}",
            file_not_allowed: "{{__('system.file_not_allowed')}}",
            action_failed: "{{__('system.action_failed')}}",
        };
    </script>
@endsection