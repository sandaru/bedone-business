@extends('layout.bedone_template')

@section('body_content')
    <div class="row" id="partial" style="visibility: hidden;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if ($News->NewsFeatureImage != [""])
                    <div id="carousel" class="carousel slide" data-ride="carousel">

                        <!-- Indicators -->
                        <ul class="carousel-indicators">
                            @for ($i = 0; $i < count($News->NewsFeatureImage); $i++)
                            <li data-target="#carousel" data-slide-to="{{$i}}" @if($i === 0) class="active" @endif></li>
                            @endfor
                        </ul>

                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            @foreach ($News->NewsFeatureImage as $index => $item)
                            <div class="carousel-item @if ($index === 0) active @endif text-center">
                                <img src="{{asset($item)}}" alt="{{$News->Title}}" class="img-fluid">
                            </div>
                            @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#carousel" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>

                    </div>
                    @endif

                    <div class="text-left" style="margin-top: 15px;">
                        <label>
                            <i class="fa fa-calendar-alt"></i> {{$News->CreatedDate}}
                             -
                            {{$News->CreatedByName}}
                             -
                            <i class="fa {{$News->Category->Icon}}"></i> {{$News->Category->Name}}
                        </label>

                        <h3>{{$News->Title}}</h3>

                        <div>
                            @markdown($News->Content)
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <h5 class="card-header bg-primary" style="padding: 20px; color: white;">
                    {{__('system.Comment')}}
                </h5>

                <div class="card-body">
                    @if ($functionAccess['ViewComment'])
                    <div class="col-md-12" v-if="list_comment.length > 0" v-for="(item, index) in list_comment">
                        <div class="card flex-row flex-wrap">
                            <div class="card-header border-0">
                                <a :href="get_profile_url(item.UserID)" target="_blank">
                                    <img :src="get_profile_image(item.ProfileImage)" :alt="item.CreatedByName"
                                         class="avatar-size rounded-circle">
                                </a>
                            </div>
                            <div class="card-block px-2" style="padding-top: 10px; width: 70%">
                                <h4 class="card-title">
                                    <a :href="get_profile_url(item.UserID)" target="_blank">
                                        @{{ item.CreatedByName }}
                                    </a>
                                </h4>
                                <p class="card-text" v-html="item.Comment.replace(/(?:\r\n|\r|\n)/g, '<br>')" v-if="nowEditing !== index"></p>
                                <div class="form-group"  v-if="nowEditing === index">
                                    <textarea type="text" class="form-control" v-model="nowEditComment"
                                              placeholder="{{__('system.cmt_here')}}"
                                              style="height: 70px;"></textarea>

                                    <div class="text-right" style="padding-top: 10px;">
                                        <button class="btn btn-primary" @click="doEdit(item.ID)">{{__('system.Save')}}</button>
                                        <button class="btn btn-danger" @click="clearEditState">{{__('system.Close')}}</button>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="card-footer w-100 text-muted" style="position: relative;">
                                @{{ item.CreatedDate }}

                                <div class="card-util" v-if="nowEditing === -1">
                                    @if ($functionAccess['EditComment'])
                                        <i class="fa fa-edit clickable" @click="setEditState(index)"
                                           title="{{__('system.Edit')}}"></i>
                                    @endif

                                    @if ($functionAccess['DeleteComment'])
                                        <i class="fa fa-trash clickable" @click="removeComment(index)"
                                           title="{{__('system.Remove')}}"></i>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center" v-if="list_comment.length == 0">
                        <p>{{__('system.no_comment')}}</p>
                    </div>

                    <div class="text-center" v-if="total > limit">
                        <button class="btn btn-primary" v-if="page > 1" @click="page_change(false)">
                            {{__('system.older_cmt')}}
                        </button>

                        <button class="btn btn-primary" v-if="page < totalPage" @click="page_change(true)">
                            {{__('system.newest_cmt')}}
                        </button>
                    </div>
                    @else
                        <div class="text-center">
                            <p>{{__('system.no_comment')}}</p>
                        </div>
                    @endif
                </div>

                @if ($functionAccess['AddComment'])
                <div class="card-footer" style="background-color: #ecf0f1;">
                    <h5>{{__('system.own_comment')}}</h5>
                    <table style="width: 100%;">
                        <tr>
                            <td width="10%">
                                <img src="{{profile_picture($userObj->ProfileImage)}}"
                                     style="width: 40px; height: 40px; border-radius: 100%; object-fit: cover;">
                            </td>
                            <td>
                                <textarea type="text" class="form-control" v-model="comment"
                                          placeholder="{{__('system.cmt_here')}}"
                                          style="height: 70px;"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-right" style="padding-top: 10px;">
                                <button class="btn btn-primary" @click="post_comment">{{__('system.Comment')}}</button>
                            </td>
                        </tr>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        var NEWS_ID = "{{encode_id($News->ID)}}";
        var functionAccess = {!! json_encode($functionAccess) !!};
        var langText = {!! json_encode([
            'failed_retrieve' => __('system.failed_retrieve'),
            'action_failed' => __('system.action_failed'),
            'are_you_sure' => __('system.are_you_sure'),
            'wont_revert' => __('system.wont_revert'),
            'YesDelete' => __('system.YesDelete'),
        ]) !!};

        var userInfo = {
            UserID: "{{encode_id($userObj->ID)}}",
            ProfileImage: "{{$userObj->ProfileImage}}",
            CreatedByName: "{{$userObj->full_name}}",
        };
    </script>
@endsection