<!--
BeDone Business Platform v1.2.1
By Phat Tran Minh - Seth Phat
http://sethphat.com
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{isset($page_title) ? $page_title . " - " . config('bedone.SITE_NAME') : config('bedone.SITE_NAME')}}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <!-- CSS Files -->
    @if(count($CssInclude))
        @foreach ($CssInclude as $file)
            <link href="{{$file}}" rel="stylesheet">
        @endforeach
    @endif

    <script>
        var base = "{{url('/')}}/";
        var base_api = base + "api/";
    </script>
</head>

<body>
<div class="wrapper">
    <div class="loader" style="display: none;"></div>
    <div class="sidebar" data-color="blue" data-image="{{asset('img/sidebar.jpg')}}">
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{url("/")}}" class="simple-text logo-mini">
                    {{config('bedone.LOGO_KEY')}}
                </a>
                <a href="{{url("/")}}" class="simple-text logo-normal">
                    {{config('bedone.SHORT_NAME')}}
                </a>
            </div>
            <div class="user">
                <div class="photo">
                    <img src="{{profile_picture($userObj->ProfileImage)}}" style="object-fit: cover;" />
                </div>
                <div class="info ">
                    <a class="collapsed">
                            <span>
                                {{$userObj->full_name}}
                            </span>
                    </a>
                </div>
            </div>
            <ul class="nav">
                @foreach ($Sidebar as $menu_item)
                <li class="nav-item {{$menu_item['Current'] ? "active" : ""}}">
                    <a class="nav-link" href="{{empty($menu_item['RouteName']) ? "#collapse".$menu_item['Name'] : route($menu_item['RouteName'])}}"
                            @if($menu_item['HasChild']) data-toggle="collapse" @endif
                            @if($menu_item['HasChild'] && $menu_item['HasOpenChild']) aria-expanded="true" @endif>
                        <i class="fa {{$menu_item['Icon']}}"></i>
                        <p>
                            {{__('module.MODULE_' .$menu_item['Name'])}}
                            @if($menu_item['HasChild'])
                                <b class="caret"></b>
                            @endif
                        </p>

                    </a>

                    <div class="collapse {{($menu_item['HasChild'] && $menu_item['HasOpenChild']) ? "show" : ""}}" id="collapse{{$menu_item['Name']}}">
                        <ul class="nav">
                            @foreach ($menu_item['Children'] as $child)
                            <li class="nav-item {{$child['Current'] ? "active" : ""}}">
                                <a class="nav-link" href="{{empty($child['RouteName']) ? "#" : route($child['RouteName'])}}">
                                    <i class="fa {{$child['Icon']}} fa-small"></i>
                                    <p>{{__('module.MODULE_' .$child['Name'])}}</p>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon d-none d-lg-block">
                            <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                            <i class="fa fa-list visible-on-sidebar-mini"></i>
                        </button>
                    </div>
                    <a class="navbar-brand">
                        {{isset($page_title) ? $page_title : ""}}
                    </a>
                </div>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar burger-lines"></span>
                    <span class="navbar-toggler-bar burger-lines"></span>
                    <span class="navbar-toggler-bar burger-lines"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <ul class="navbar-nav">
                        @if (count($ActionMenu) > 0)
                        <li class="dropdown nav-item">
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="fa fa-bolt fa-normal"></i>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach ($ActionMenu as $menu)
                                <a class="dropdown-item"
                                        href="{{empty($menu['URL']) ? "javascript:void(0)" : $menu['URL']}}"
                                        onclick="{{empty($menu['JsAction']) ? "return;" : $menu['JsAction']}}">
                                    @if (!empty($menu['Icon']))
                                        <i class="fa {{$menu['Icon']}}"></i>
                                    @endif
                                    {{$menu['Title']}}
                                </a>
                                @endforeach
                            </ul>
                        </li>
                        @endif
                        <li class="dropdown nav-item">
                            @include('bedone.notification.index', $Notification)
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-cogs fa-normal"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{route('userProfilePage')}}">
                                    <i class="fa fa-user"></i> {{__('module.MyProfile')}}
                                </a>
                                <a class="dropdown-item" href="{{route('settingPage')}}">
                                    <i class="fa fa-cog"></i> {{__('module.Settings')}}
                                </a>
                                <div class="divider"></div>
                                <a href="{{route('logoutAction')}}" class="dropdown-item text-danger">
                                    <i class="fa fa-power-off"></i> {{__('module.LogOut')}}
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="container-fluid">
                    @yield('body_content')
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <nav>
                    <p class="copyright text-center">
                        © 2018 by Seth Phat
                    </p>
                </nav>
            </div>
        </footer>
    </div>
</div>
</body>

@if (Session::has('error'))
    <script>
        var error_mess = "{{ Session::get('error')}}";
    </script>
@endif

@if (Session::has('success'))
    <script>
        var success_mess = "{{ Session::get('success')}}";
    </script>
@endif

@if (count($errors) > 0)
    <script>
        var valid_error_mess = "{{$errors->first()}}";
    </script>
@endif

<!-- Global language string -->
<script>
    var globalLang = {!! $global_lang !!};
</script>

<!--   Core JS Files   -->
@if(count($JsInclude))
    @foreach ($JsInclude as $file)
        <script type="text/javascript" src="{{$file}}"></script>
    @endforeach
@endif

</html>