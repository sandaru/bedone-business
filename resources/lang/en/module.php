<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 5/24/2018
 * Time: 8:34 PM
 */

return [
    // Module name
    'MyProfile' => 'My Profile',
    'EditProfile' => 'Edit Profile',
    'EditCompany' => 'Edit Company',
    'Settings'  => 'Settings',
    'Company'  => 'Company',
    'LogOut'  => 'Log out',
    'LeaveCompany'  => 'Leave Company',
    'NewsCategoryMaintenance'  => 'News Category Maintenance',
    'AddCategory'  => 'Add Category',
    'News'  => 'News',
    'NewsCategory'  => 'News Categories',
    'AllStaff'  => 'All Staffs',
    'UserMaintenance'  => 'User Maintenance',
    'CompanyJoinRequest'  => 'Company Join Request',

    // module name table
    'MODULE_Company' => 'Company',
    'MODULE_Booking' => 'Booking',
    'MODULE_News' => 'News',
    'MODULE_MyNetwork' => 'My Network',
    'MODULE_NewsCategory' => 'News Category',
    'MODULE_NewsFeed' => 'NewsFeed',
    'MODULE_AllStaff' => 'All Staffs',
    'MODULE_Maintainance' => 'Maintenance',
    'MODULE_BookingAdmin' => 'Booking Admin',
    'MODULE_BookingResource' => 'Booking Resource',
    'MODULE_UserProfile' => 'User Profile',
    'MODULE_UserMaintainance' => 'User Maintenance',
    'MODULE_CompanyJoinRequest' => 'Company Join Request',
    'MODULE_Chat' => 'Chat Messenger',
    'MODULE_Group' => 'Groups',


    //Name of fields
    'Personal_Info'  => 'Personal Information',
    'Work_Info'  => 'Work Information',
    'Contact_Info'  => 'Contact Information',
];