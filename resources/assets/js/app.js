require('./bootstrap');

// import vue
window.Vue = require('vue');

// needed filter
require('./filters/index');

// import necessary components
import ChatRoom from './components/ChatRoomComponent.vue';
import OneOnOne from './components/OneOnOneComponent.vue';
import GroupChat from './components/GroupChatComponent.vue';
import SidebarUser from './components/SidebarUserComponent.vue';
import SidebarGroup from './components/SidebarGroupComponent.vue';
import AddPeople from './components/AddPeopleComponent.vue';
import ChatMessage from './components/ChatMessageComponent.vue';
import {MessageHelper} from "./common/helper";

const CHAT_MESS_CONTAINER = "#chatMessageContainer";

const app = new Vue({
    el: '#partial',
    components: {
        'chat-room': ChatRoom,
        'one-on-one': OneOnOne,
        'group-chat': GroupChat,
        'sidebar-user': SidebarUser,
        'sidebar-group': SidebarGroup,
        'add-people': AddPeople,
        'chat-message': ChatMessage,
    },
    data: {
        roomInfo: {
            chatRooms: [],
            filterText: "",
            start: 0,
            limit: 10,
            isEndRoom: false,
        },
        chatInfo: {
            currentRoom: null,
            chatMessages: [],
            isOpenSidebar: false,
            textMessage: "",
            start: 0,
            limit: 20,
            isEndMess: false,
            isLoadMess: false,
            pollingStatus: false,
            pollingTimeOut: null,
            axiosSource: null,
        },
    },
    methods: {
        openOneOnOne() {
            this.$refs.OneOnOneComponent.open();
        },
        openGroupChat() {
            this.$refs.GroupChatComponent.open();
        },
        isSelectedRoom(room) {
            if(this.chatInfo.currentRoom === null) {
                return false;
            }

            if (room.Name === this.chatInfo.currentRoom.Name && room.ProfileImage === this.chatInfo.currentRoom.ProfileImage) {
                return true;
            }
            return false;
        },
        getUrlRoom(isRefresh) {
            var start = _.clone(this.roomInfo.start);
            if (isRefresh) {
                start = 0;
            }
            return `${base_api}chat/chat_room?start=${start}&limit=${this.roomInfo.limit}&keyword=${this.roomInfo.filterText}`;
        },
        getRoom(isRefresh) {
            var self = this;
            axios.get(this.getUrlRoom(isRefresh))
                .then(function (data) {
                    if (data.data) {
                        self.roomInfo.chatRooms = data.data;
                        if (data.data.length < self.roomInfo.limit) {
                            self.roomInfo.isEndRoom = true;
                        }
                    }
                });
        },
        refreshRoom() {
            var self = this;
            setTimeout(function () {
                self.getRoom(true);
                self.refreshRoom();
            }, roomRefreshTime)
        },
        loadMoreRoom() {
            this.roomInfo.start += this.roomInfo.limit;
            var self = this;
            axios.get(this.getUrlRoom())
                .then(function (data) {
                    if (data.data) {
                        _.each(data.data, function (item) {
                            self.roomInfo.chatRooms.push(item);
                        });
                        if (data.data.length < self.roomInfo.limit) {
                            self.roomInfo.isEndRoom = true;
                        }
                    }
                });
        },
        doFilterRoom(e) {
            this.roomInfo.filterText = e.target.value;
            this.roomInfo.start = 0;
            this.getRoom(false);
        },
        getUrlMess(roomID) {
            return `${base_api}chat/chat_room/mess/${roomID}?start=${this.chatInfo.start}&limit=${this.chatInfo.limit}`;
        },
        getUrlNewMess(roomID) {
            return `${base_api}chat/chat_room/new_mess/${roomID}`;
        },
        sendLoadMess(callback, errCallback) {
            BeDoneAjax.get(this.getUrlMess(this.chatInfo.currentRoom.ID))
                .done(result => {
                    callback(result);
                })
                .fail(err => {
                    errCallback(err);
                });
        },
        loadRoomMessage() {
            var self = this;
            this.sendLoadMess((result) => {
                var data = result;
                if (data !== null) {
                    if (data.error) {
                        BeDoneToaster.error(data.error);
                        return;
                    }

                    self.chatInfo.chatMessages = data.data;
                    if (data.data.length !== self.chatInfo.limit) {
                        self.chatInfo.isEndMess = true;
                    }

                    // after set data
                    self.scrollChatBottom();
                    self.clearPoiling();
                    self.retrieveNewMess();
                    $("#chatMessageContainer").off('scroll').scroll(function () {
                        if ($(this).scrollTop() === 0) {
                            self.scrollTopEvent();
                        }
                    });
                } else {
                    BeDoneToaster.error(globalLang.action_failed);
                    setTimeout(self.loadRoomMessage, 2500);
                }
            }, (err) => {
                BeDoneToaster.error(globalLang.action_failed);
                setTimeout(self.loadRoomMessage, 2500);
            });
        },
        retrieveNewMess() {
            var self = this;

            function timeOutRetrieveMess() {
                var CreatedDate = "";
                if (self.chatInfo.chatMessages.length > 0) {
                    CreatedDate = self.chatInfo.chatMessages[0].CreatedDate;
                }

                const CancelToken = axios.CancelToken;
                self.chatInfo.pollingStatus = true;
                self.chatInfo.axiosSource = CancelToken.source();
                axios({
                    method: "get",
                    url: self.getUrlNewMess(self.chatInfo.currentRoom.ID) + "?CreatedDate=" + CreatedDate,
                    cancelToken: self.chatInfo.axiosSource.token
                }).then(result => {
                        if (self.chatInfo.pollingStatus === false) {
                            return;
                        }

                        var data = result.data;
                        if (data !== null) {
                            if (data.error) {
                                BeDoneToaster.error(data.error);
                            } else if (data.data) {
                                _.each(data.data, function (mess) {
                                    self.chatInfo.chatMessages.unshift(mess);
                                });
                            }
                        }
                        self.retrieveNewMess();
                    })
                    .catch(err => {
                        if (axios.isCancel(err) === false) {
                            self.retrieveNewMess();
                        }
                    });
            }

            this.chatInfo.pollingTimeOut = setTimeout(timeOutRetrieveMess, 500);
        },
        clearPoiling() {
            this.chatInfo.pollingStatus = false;
            if (this.chatInfo.axiosSource != null) {
                this.chatInfo.axiosSource.cancel('Stop retrieving old room');
            }
            clearTimeout(this.chatInfo.pollingTimeOut);
        },
        postChat() {
            if (this.chatInfo.textMessage === "") {
                return;
            }

            // ok post chat now :D
            var self = this;
            var roomID = this.chatInfo.currentRoom.ID;
            BeDoneAjax.post(base_api + "chat/chat_room/chat/" + roomID, {Message: this.chatInfo.textMessage})
                .done(data => {
                    if (data !== null) {
                        if (data.error) {
                            BeDoneToaster.error(globalLang.action_failed);
                        } else {
                            self.chatInfo.chatMessages.unshift(data.data);
                            self.scrollChatBottom();
                            self.chatInfo.textMessage = "";
                            self.chatInfo.currentRoom.LastActivityMessage = data.data.LatestMessage;
                        }
                    } else {
                        BeDoneToaster.error(globalLang.action_failed);
                    }
                })
                .fail(err => {
                    BeDoneToaster.error(globalLang.action_failed);
                });
        },
        chooseIMG() {
            $("#fileImgAtt").click();
        },
        doUploadChatImage(e) {
            var file = _.first(e.target.files);
            if (file.type.indexOf("image") < 0)
            {
                $("#fileImgAtt").val('');
                BeDoneToaster.error(globalLang.file_not_allowed);
                return;
            }

            // update file now
            var postData = new FormData();
            postData.append("Image", file);

            var self = this;
            var roomID = this.chatInfo.currentRoom.ID;
            BeDoneAjax.post(base_api + "chat/chat_room/chat_img/" + roomID, postData, ajaxUploadSetting)
                .done(function (data) {
                    if (data != null)
                    {
                        if (data.error) {
                            BeDoneToaster.error(data.error);
                        } else {
                            self.chatInfo.chatMessages.unshift(data.data);
                            self.scrollChatBottom();
                            $("#fileImgAtt").val('');
                        }
                    }
                    else {
                        BeDoneToaster.error(globalLang.action_failed);
                    }
                })
                .fail(err => {
                    BeDoneToaster.error(globalLang.action_failed);
                });
        },
        scrollChatBottom(time) {
            runAfter(function () {
                $(CHAT_MESS_CONTAINER).scrollToBottom();
            }, _.isUndefined(time) ? 500 : time);
        },
        scrollTopEvent() {
            if (this.chatInfo.isEndMess === true) {
                return;
            }
            if (this.chatInfo.isLoadMess === true) {
                return;
            }

            this.chatInfo.isLoadMess = true;
            this.chatInfo.start += this.chatInfo.limit;
            var self = this;
            this.sendLoadMess((result) => {
                var data = result;
                if (data !== null) {
                    if (data.error) {
                        BeDoneToaster.error(data.error);
                        return;
                    }

                    // ok set data
                    _.each(data.data, (mess) => {
                        self.chatInfo.chatMessages.push(mess);
                    });
                    $(CHAT_MESS_CONTAINER).scrollTop(50); // to prevent stay at top

                    // check if end room
                    if (data.data.length !== self.chatInfo.limit) {
                        self.chatInfo.isEndMess = true;
                    }
                } else {
                    BeDoneToaster.error(langText.failed_retrieve);
                    self.chatInfo.start -= self.chatInfo.limit;
                }

                self.chatInfo.isLoadMess = false;
            }, (err) => {
                 BeDoneToaster.error(langText.failed_retrieve);
                self.chatInfo.start -= self.chatInfo.limit;
                self.chatInfo.isLoadMess = false;
            });
        }
    },
    mounted() {
        $("#partial").css('visibility', 'visible');
        var self = this;
        this.getRoom(true);
        this.refreshRoom();
    },
    created() {
        // listen emit when select room
        this.$on('OpenChat', roomData => {
            // select this room too
            this.chatInfo.currentRoom = roomData;

            // open room mess
            this.chatInfo.start = 0;
            this.chatInfo.chatMessages = [];
            this.chatInfo.isEndMess = false;
            this.chatInfo.isLoadMess = false;
            this.loadRoomMessage();
        });
        this.$on('LeaveAndArchive', roomID => {
            var roomIndex = _.findIndex(this.roomInfo.chatRooms, function (item) {
                return item.ID === roomID;
            });

            if (this.chatInfo.currentRoom !== null && this.chatInfo.currentRoom.ID === roomID) {
                this.chatInfo.currentRoom = null;
            }

            if (roomIndex >= 0) {
                this.roomInfo.chatRooms.splice(roomIndex, 1);
            }
        });
    },
    computed: {
        reversedMess() {
            return [...this.chatInfo.chatMessages].reverse();
        }
    }
});