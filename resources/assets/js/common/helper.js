var MessageHelper = {};

MessageHelper.archiveMess = function (roomID, callback) {
    BeDoneConfirm.confirm("", function () {
        BeDoneAjax.get(base_api + "chat/chat_room/archive/" + roomID)
            .done(data => {
                if (data !== null) {
                    if (data.error) {
                        BeDoneToaster.error(data.error);
                    } else {
                        BeDoneToaster.success(data.success);
                        callback();
                    }
                } else {
                    BeDoneToaster.error(globalLang.action_failed);
                }
            })
            .fail(err => {
                BeDoneToaster.error(globalLang.action_failed);
            });
    });
};

MessageHelper.leaveGroup = function (groupID, callback) {
    var postData = {
        GroupID: groupID,
    };

    BeDoneConfirm.confirm("", function () {
        BeDoneAjax.post(base_api + "chat/chat_room/leave_group", postData)
            .done(data => {
                if (data !== null) {
                    if (data.error) {
                        BeDoneToaster.error(data.error);
                    } else {
                        BeDoneToaster.success(data.success);
                        callback();
                    }
                } else {
                    BeDoneToaster.error(globalLang.action_failed);
                }
            })
            .fail(err => {
                BeDoneToaster.error(globalLang.action_failed);
            });
    });
};

MessageHelper.notifySound = function () {
    document.getElementById("audioNotify").play();
};


export {
    MessageHelper
}